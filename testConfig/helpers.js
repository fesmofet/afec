import { mount, shallow, configure } from 'enzyme';
import { assert, expect } from 'chai';
import sinon from 'sinon';
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import mochaSnapshots from 'mocha-snapshots';
import 'styled-components-test-utils/lib/chai';

// React
/*global*/
global.React = React;
global.ReactDOM = ReactDOM;
global.PropTypes = PropTypes;
// Mocha Snapshots
global.mochaSnapshots = mochaSnapshots;
mochaSnapshots.setup({ sanitizeClassNames: false });
// Sinon
/*global*/
global.sinon = sinon;
// Chai
/*global*/
global.expect = expect;
/*global*/
global.assert = assert;
// Enzyme
configure({ adapter: new Adapter() });
global.mount = mount;
global.shallow = shallow;
// Render StyledComponent
global.getRenderedSC = StyledComponent => renderer.create(StyledComponent).toJSON();

global.createSnapshot = Component => {
    const wrapper = shallow(Component);
    expect(wrapper).to.matchSnapshot();
};

global.shalowRender = Component => {
    return shallow(Component);
};

global.mountRender = Component => {
    return mount(Component);
};
