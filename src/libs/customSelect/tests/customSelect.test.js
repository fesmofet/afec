import CustomSelectMemo from '../CustomSelect.jsx';
import { Select, defaultStyles } from '../styledComponents';

const CustomSelect = CustomSelectMemo.type;

describe('CustomButton component', () => {
    let props;
    let sandbox;

    before(() => {
        sandbox = sinon.createSandbox();
        props = {
            fontSize: 12,
            textColor: 'textColor',
            fontWeight: 500,
            borderRadius: 5,
            defaultValue: 'option2',
            dataAttribute: 'dataAttribute',
            textTransform: 'textTransform',
            backgroundColor: 'backgroundColor',
            onChangeCallback: () => true,
            backgroundColorHover: 'backgroundColorHover',
            options: [
                { id: 'optionId1', dataAttribute: 'option1', value: 'option1', innerText: 'one' },
                { id: 'optionId2', dataAttribute: 'option2', value: 'option2', innerText: 'two' },
                { id: 'optionId3', dataAttribute: 'option3', value: 'option3', innerText: 'three' },
            ],
        };
    });

    it('CustomSelect create a snapshot, should render correctly', () => {
        const wrapper = shallow(<CustomSelect {...props}/>);

        expect(wrapper).matchSnapshot();
    });

    it('CustomSelect should call onChangeCallback', () => {
        const localProps = { ...props, onChangeCallback: sandbox.stub() };
        const component = shallow(<CustomSelect {...localProps}/>);
        component.find(Select).simulate('change');
        sinon.assert.calledOnce(localProps.onChangeCallback);
    });

    afterEach(() => {
        sandbox.restore();
    });

    after(() => {
        sandbox.reset();
    });
});

describe('CustomSelect styled components', () => {
    const props = {
        fntSize: 12,
        txtColor: 'textColor',
        fntWeight: 500,
        brRadius: 5,
        bgColor: 'backgroundColor',
        txtTransform: 'textTransform',
        bgColorHover: 'backgroundColorHover',
    };

    it('Select should have correct styles when all props were transferred', () => {
        const component = getRenderedSC(<Select {...props}/>);

        expect(component).toHaveStyleRule('color', 'textColor');
        expect(component).toHaveStyleRule('font-size', '12px');
        expect(component).toHaveStyleRule('background', 'backgroundColor');
        expect(component).toHaveStyleRule('font-weight', '500');
        expect(component).toHaveStyleRule('border-radius', '5px');
        expect(component).toHaveStyleRule('text-transform', 'textTransform');
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('background', 'backgroundColorHover');
    });

    it('Select should have default styles when all props weren\'t transferred', () => {
        const component = getRenderedSC(<Select />);

        expect(component).toHaveStyleRule('color', defaultStyles.txtColorDefault);
        expect(component).toHaveStyleRule('font-size', `${defaultStyles.fntSizeDefault}px`);
        expect(component).toHaveStyleRule('background', defaultStyles.bgColorDefault);
        expect(component).toHaveStyleRule('font-weight', defaultStyles.fontWeightDefault);
        expect(component).toHaveStyleRule('border-radius', `${defaultStyles.brRadiusDefault}px`);
        expect(component).toHaveStyleRule('text-transform', defaultStyles.txtTransformDefault);
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('background', defaultStyles.bgColorHoverDefault);
    });
});
