import styled from 'styled-components';

const bgColorDefault = '#fff';
const fntSizeDefault = 15;
const txtColorDefault = '#000';
const brRadiusDefault = 3;
const fontWeightDefault = 'normal';
const txtTransformDefault = 'none';
const bgColorHoverDefault = '#fff';

export const Select = styled.select`
  width: 100%;
  height: 100%;
  color: ${props => props.txtColor ? props.txtColor : txtColorDefault};
  background: ${props => props.bgColor ? props.bgColor : bgColorDefault};
  font-size: ${props => props.fntSize ? props.fntSize : fntSizeDefault}px;
  font-weight: ${props => props.fntWeight ? props.fntWeight : fontWeightDefault};
  border-radius: ${props => props.brRadius ? props.brRadius : brRadiusDefault}px;
  text-transform: ${props => props.txtTransform ? props.txtTransform : txtTransformDefault};
    
  &:hover {
    cursor: pointer;
    background: ${props => props.bgColorHover ? props.bgColorHover : bgColorHoverDefault};
  }

  &:active {
    box-shadow: none;
  }
`;

export const Option = styled.option``;

export const defaultStyles = {
    bgColorDefault,
    fntSizeDefault,
    txtColorDefault,
    brRadiusDefault,
    fontWeightDefault,
    txtTransformDefault,
    bgColorHoverDefault,
};
