import React from 'react';
import PropTypes from 'prop-types';
import {Select, Option} from './styledComponents';

const CustomSelect = props => {
    const {
        userId,
        referral,
        options,
        fontSize,
        textColor,
        fontWeight,
        borderRadius,
        defaultValue,
        dataAttribute,
        textTransform,
        backgroundColor,
        onChangeCallback,
        backgroundColorHover,
    } = props;

    return (
        <Select
            accessKey={userId}
            fntSize={fontSize}
            txtColor={textColor}
            fntWeight={fontWeight}
            data-at={dataAttribute}
            brRadius={borderRadius}
            bgColor={backgroundColor}
            onChange={onChangeCallback}
            txtTransform={textTransform}
            bgColorHover={backgroundColorHover}
            defaultValue={defaultValue}
            ref={referral}
        >
            {options && options.length ? options.map(option =>
                <Option
                    id={option.id}
                    key={option.id}
                    value={option.value}
                    data-at={option.dataAttribute}>
                    {option.innerText}
                </Option>
            ) : null}
        </Select>
    );
};

CustomSelect.propTypes = {
    userId: PropTypes.number,
    options: PropTypes.array,
    fontSize: PropTypes.number,
    textColor: PropTypes.string,
    defaultValue: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    bgColorHover: PropTypes.string,
    borderRadius: PropTypes.number,
    dataAttribute: PropTypes.string,
    textTransform: PropTypes.string,
    backgroundColor: PropTypes.string,
    backgroundColorHover: PropTypes.string,
    onChangeCallback: PropTypes.func,
    referral: PropTypes.object,
    fontWeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

export default React.memo(CustomSelect);
