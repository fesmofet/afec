import styled from 'styled-components';

const bgColorDisabled = '#79838a';
const bgColorDefault = '#000000';
const fntSizeDefault = 10;
const txtColorDefault = '#ffffff';
const brRadiusDefault = 3;
const bxShadowDefault = 'none';
const fontWeightDefault = 'bold';
const txtTransformDefault = 'uppercase';
const bgColorHoverDefault = '#000000';

export const Button = styled.button`
  width: 100%;
  height: 100%;
  background: ${props => props.bgColor ? props.bgColor : bgColorDefault};
  border-radius: ${props => props.brRadius ? props.brRadius : brRadiusDefault}px;
  color: ${props => props.txtColor ? props.txtColor : txtColorDefault};
  font-size: ${props => props.fntSize ? props.fntSize : fntSizeDefault}px;
  font-weight: ${props => props.fntWeight ? props.fntWeight : fontWeightDefault};
  text-transform: ${props => props.txtTransform ? props.txtTransform : txtTransformDefault};
    
  &:hover {
    cursor: pointer;
    box-shadow: ${props => props.bxShadow ? props.bxShadow : bxShadowDefault};
    background: ${props => props.bgColorHover ? props.bgColorHover : bgColorHoverDefault};
  }

  &:active {
    box-shadow: none;
  }
  
  &:disabled {
    opacity: 0.5;
    cursor: no-drop;
    user-select: none;
    background: ${bgColorDisabled};
    box-shadow: none;
  }
`;

export const defaultStyles = { // For Unit Test
    bgColorDisabled,
    bgColorDefault,
    fntSizeDefault,
    txtColorDefault,
    brRadiusDefault,
    bxShadowDefault,
    fontWeightDefault,
    txtTransformDefault,
    bgColorHoverDefault,
};
