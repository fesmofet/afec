import React from 'react';
import PropTypes from 'prop-types';
import {Button} from './styledComponents';

const CustomButton = props => {
    const {
        fontSize,
        boxShadow,
        textColor,
        fontWeight,
        isDisabled,
        buttonName,
        borderRadius,
        dataAttribute,
        textTransform,
        backgroundColor,
        onClickCallback,
        backgroundColorHover,
    } = props;

    return (
        <Button
            data-at={dataAttribute}
            bgColor={backgroundColor}
            onClick={onClickCallback}
            fntSize={fontSize}
            brRadius={borderRadius}
            disabled={isDisabled}
            children={buttonName}
            txtColor={textColor}
            bxShadow={boxShadow}
            fntWeight={fontWeight}
            txtTransform={textTransform}
            bgColorHover={backgroundColorHover}
        />
    );
};

CustomButton.propTypes = {
    fontSize: PropTypes.number,
    isDisabled: PropTypes.bool,
    boxShadow: PropTypes.string,
    textColor: PropTypes.string,
    fontWeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    buttonName: PropTypes.string.isRequired,
    borderRadius: PropTypes.number,
    dataAttribute: PropTypes.string,
    textTransform: PropTypes.string,
    backgroundColor: PropTypes.string,
    bgColorHover: PropTypes.string,
    onClickCallback: PropTypes.func.isRequired,
    backgroundColorHover: PropTypes.string,
};

CustomButton.defaultProps = {
    isDisabled: false,
};

export default React.memo(CustomButton);
