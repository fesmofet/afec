import InputBlockMemo from '../InputBlock.jsx';
import * as styledComponents from '../styledComponents';

const InputBlock = InputBlockMemo.type;

describe('InputBlock react component', () => {
    const props = {
        id: 'inputId', 
        labelText: 'labelText', 
        labelTextColor: 'labelTextColor', 
        labelBgColor: 'labelBgColor', 
        labelBorderColor: 'labelBorderColor', 
        labelFontSize: 12,
        labelfontWeight: 500, 
        labelBorderRadius: 5,
        inputType: 'text', 
        placeholder: 'placeholder', 
        inputTextColor: 'inputTextColor', 
        inputBgColor: 'inputBgColor',  
        inputFontSize: 12,
        dataAttribute: 'dataAttribute',
        inputBlockBgColor: 'inputBlockBgColor',
    };

    it('snapshot created, should rendered correctly', done => {
        const wrapper = shallow(<InputBlock {...props}/>);

        expect(wrapper).matchSnapshot();

        done();
    });
});

describe('InputBlockWrapper styled components', () => {
    const props = {
        labelBorderRadius: 2,
        inputBlockBgColor: '#000000',
    };

    it('InputBlockWrapper should have correct styles, when all props were ransferred', () => {
        const component = getRenderedSC(<styledComponents.InputBlockWrapper {...props}/>);

        expect(component).toHaveStyleRule('background-color', '#000000');
    });

    it('InputBlockWrapper should have default styles, when all props weren\'t transferred', () => {
        const component = getRenderedSC(<styledComponents.InputBlockWrapper/>);

        expect(component).toHaveStyleRule('background-color', styledComponents.defaultStyles.inputBlockBgColorDefault);
    });
});

describe('LabelBox styled components', () => {
    const props = {
        labelTextColor: '#fff', 
        labelBgColor: '#000', 
        labelBorderColor: 'red', 
        labelFontSize: 10,
        labelfontWeight: 'normal', 
        labelBorderRadius: 2,
    };

    it('LabelBox should have correct styles, when all props were ransferred', () => {
        const component = getRenderedSC(<styledComponents.LabelBox {...props}/>);

        expect(component).toHaveStyleRule('color', '#fff');
        expect(component).toHaveStyleRule('background-color', '#000');
        expect(component).toHaveStyleRule('border', '2px solid red');
        expect(component).toHaveStyleRule('border-radius', '2px');
        expect(component).toHaveStyleRule('font-size', '10px');
        expect(component).toHaveStyleRule('font-weight', 'normal');
    });

    it('LabelBox should have default styles, when all props weren\'t transferred', () => {
        const component = getRenderedSC(<styledComponents.LabelBox/>);

        expect(component).toHaveStyleRule('color', styledComponents.defaultStyles.labelTextColorDefault);
        expect(component).toHaveStyleRule('background-color', styledComponents.defaultStyles.labelBgColorDefault);
        expect(component).toHaveStyleRule('border', `2px solid ${styledComponents.defaultStyles.labelBorderColorDefault}`);
        expect(component).toHaveStyleRule('border-radius', `${styledComponents.defaultStyles.labelBorderRadiusDefault}px`);
        expect(component).toHaveStyleRule('font-size', `${styledComponents.defaultStyles.labelFontSizeDefault}px`);
        expect(component).toHaveStyleRule('font-weight', styledComponents.defaultStyles.labelFontWeightDefault);
    });
});

describe('InputBox styled components', () => {
    const props = {
        inputTextColor: '#fff', 
        inputBgColor: '#000',  
        inputFontSize: 10,
    };

    it('InputBox should have correct styles, when all props were ransferred', () => {
        const component = getRenderedSC(<styledComponents.InputBox {...props}/>);

        expect(component).toHaveStyleRule('color', '#fff');
        expect(component).toHaveStyleRule('background-color', '#000');
        expect(component).toHaveStyleRule('font-size', '10px');
    });

    it('InputBox should have default styles, when all props weren\'t transferred', () => {
        const component = getRenderedSC(<styledComponents.InputBox/>);

        expect(component).toHaveStyleRule('color', styledComponents.defaultStyles.inputTextColorDefault);
        expect(component).toHaveStyleRule('background-color', styledComponents.defaultStyles.inputBgColorDefault);
        expect(component).toHaveStyleRule('font-size', `${styledComponents.defaultStyles.inputFontSizeDefault}px`);
    });
});