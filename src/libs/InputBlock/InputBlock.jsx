import React from 'react';
import PropTypes from 'prop-types';
import { InputBlockWrapper, LabelBox, InputBox } from './styledComponents';

const InputBlock = (props) => {
    const {
        id,
        labelText,
        labelTextColor,
        labelBgColor,
        labelBorderColor,
        labelFontSize,
        labelfontWeight,
        labelBorderRadius,
        inputType,
        placeholder,
        inputTextColor,
        inputBgColor,
        inputFontSize,
        dataAttribute,
        inputBlockBgColor,
        onChangeCallback,
        refValue,
        isDisabled,
        value,
       
    } = props;

    return (
        <InputBlockWrapper
            inputBlockBgColor={inputBlockBgColor}
            inputBlockBrRadius={labelBorderRadius}
        >
            <LabelBox
                htmlFor={id}
                labelTextColor={labelTextColor}
                labelBgColor={labelBgColor}
                labelBorderColor={labelBorderColor}
                labelBorderRadius = {labelBorderRadius}
                labelFontSize={labelFontSize}
                labelfontWeight={labelfontWeight}
                children={labelText} />
            <InputBox
                ref={refValue}
                id={id}
                type={inputType}
                placeholder={placeholder}
                inputTextColor={inputTextColor}
                inputBgColor={inputBgColor}
                inputFontSize={inputFontSize}
                data-at={dataAttribute}
                inputBrRadius={labelBorderRadius}
                disabled={isDisabled}
                defaultValue={value}
                onChange={onChangeCallback}
                />
        </InputBlockWrapper>
    );
};

InputBlock.propTypes = {
    id: PropTypes.string.isRequired,
    labelText: PropTypes.string.isRequired,
    labelTextColor: PropTypes.string,
    labelBgColor: PropTypes.string,
    labelBorderColor: PropTypes.string,
    labelFontSize: PropTypes.number,
    labelfontWeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    labelBorderRadius: PropTypes.number,
    inputType: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    inputTextColor: PropTypes.string,
    inputBgColor: PropTypes.string,
    inputFontSize: PropTypes.number,
    inputBlockBgColor: PropTypes.string,
    dataAttribute: PropTypes.string,
    isDisabled: PropTypes.bool,
    value: PropTypes.string,
    onChangeCallback: PropTypes.func,
    refValue: PropTypes.object,
};

export default React.memo(InputBlock);
