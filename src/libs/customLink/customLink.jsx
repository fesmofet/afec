import React from 'react';
import PropTypes from 'prop-types';
import { Link } from './styledComponent.js';

const CustomLink = (props) => {
    return (
        <Link onClick={props.onClick}
              fontSize={props.textSize}
              fontWeight={props.fontWeight}
              hoverColor={props.hoverColor}
              activeColor={props.activeColor}
              textTransform={props.textTransform}
              textDecoration={props.textDecoration}
        >{props.text}</Link>
    );
};

CustomLink.propTypes = {
    textSize: PropTypes.number,
    size: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    onClick: PropTypes.func,
    fontWeight: PropTypes.string,
    hoverColor: PropTypes.string,
    activeColor: PropTypes.string,
    textTransform: PropTypes.string,
    textDecoration: PropTypes.string,
    text: PropTypes.string.isRequired,
};

export default React.memo(CustomLink);
