import styled from 'styled-components';

export const defaultStyle = {
    fontSize: 15,
    activeColor: 'blue',
    hoverColor: 'darkblue',
    fontWeight: 'normal',
    textTransform: 'none',
    textDecoration: 'none',
};

export const Link = styled.a`
    color: blue;
    cursor: pointer;
    font-size: ${props => props.fontSize ? props.fontSize : defaultStyle.fontSize}px;
    font-weight: ${props => props.fontWeight ? props.fontWeight : defaultStyle.fontWeight};
    text-transform: ${props => props.textTransform ? props.textTransform : defaultStyle.textTransform};
    text-decoration: ${props => props.textDecoration ? props.textDecoration : defaultStyle.textDecoration};
    
    &:active {
      color: ${props => props.activeColor ? props.activeColor : defaultStyle.activeColor};
    }
    
    &:hover {
      color: ${props => props.hoverColor ? props.hoverColor : defaultStyle.hoverColor};
    }
`;
