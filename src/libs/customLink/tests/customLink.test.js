import { Link, defaultStyle } from '../styledComponent.js';
import CustomLinkMemo from '../customLink.jsx';
import React from 'react';

describe('CustomLink react component', () => {
    const props = {
        fontSize: 15,
        fontWeight: 'bold',
        activeColor: 'blue',
        textTransform: 'uppercase',
        textDecoration: 'underline',
    };

    it('snapshot created, should rendered correctly with props', done => {
        const wrapper = shallow(<CustomLinkMemo {...props}/>);
        expect(wrapper).matchSnapshot();
        done();
    });

    it('snapshot created, should rendered correctly without props', done => {
        const wrapper = shallow(<CustomLinkMemo />);
        expect(wrapper).matchSnapshot();
        done();
    });
});

describe('CustomLink styled components:', () => {
    const props = {
        fontSize: 18,
        fontWeight: 'bold',
        activeColor: 'orange',
        hoverColor: 'green',
        textTransform: 'uppercase',
        textDecoration: 'underline',
    };

    it('Link styled component must have styles from props', () => {
        const component = getRenderedSC(<Link {...props}/>);
        expect(component).toHaveStyleRule('font-size', `${props.fontSize}px`);
        expect(component).toHaveStyleRule('font-weight', props.fontWeight);
        expect(component).toHaveStyleRule('text-transform', props.textTransform);
        expect(component).toHaveStyleRule('text-decoration', props.textDecoration);
        expect({
            component,
            modifier: '&:active',
        }).toHaveStyleRule('color', props.activeColor);
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('color', props.hoverColor);
    });

    it('Link styled component must have default styles', () => {
        const component = getRenderedSC(<Link />);
        expect(component).toHaveStyleRule('font-size', `${defaultStyle.fontSize}px`);
        expect(component).toHaveStyleRule('font-weight', defaultStyle.fontWeight);
        expect(component).toHaveStyleRule('text-transform', defaultStyle.textTransform);
        expect(component).toHaveStyleRule('text-decoration', defaultStyle.textDecoration);
        expect({
            component,
            modifier: '&:active',
        }).toHaveStyleRule('color', defaultStyle.activeColor);
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('color', defaultStyle.hoverColor);
    });
});
