import { NavBarWrapper, NavItem, NavItemText, defaultStyle } from '../styledComponent.js';

describe('NavBarWrapper styled components:', () => {
    const props = {
        navBarBorder: '#1b115c',
    };

    it('NavBarWrapper styled component must have styles from props', () => {
        const component = getRenderedSC(<NavBarWrapper {...props}/>);
        expect(component).toHaveStyleRule('border', `1px solid ${props.navBarBorder}`);
    });

    it('NavBarWrapper styled component must have default styles', () => {
        const component = getRenderedSC(<NavBarWrapper/>);
        expect(component).toHaveStyleRule('border', `1px solid ${defaultStyle.navBarBorder}`);
    });
});

describe('NavItem styled components:', () => {
    const props = {
        navBarBorder: '#1b115c',
        btnBgColor: '#236ea1',
    };

    it('NavItem styled component must have styles from props', () => {
        const component = getRenderedSC(<NavItem {...props}/>);
        expect(component).toHaveStyleRule('background', props.btnBgColor);
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('background', props.navBarBorder);
    });

    it('NavItem styled component must have default styles', () => {
        const component = getRenderedSC(<NavItem/>);
        expect(component).toHaveStyleRule('background', defaultStyle.btnBgColor);
        expect({
            component,
            modifier: '&:hover',
        }).toHaveStyleRule('background', defaultStyle.navBarBorder);
    });
});

describe('NavItemText styled components:', () => {
    const props = {
        textColor: '#fdfdfb',
    };

    it('NavItemText styled component must have styles from props', () => {
        const component = getRenderedSC(<NavItemText {...props}/>);
        expect(component).toHaveStyleRule('color', props.textColor);
    });

    it('NavItemText styled component must have default styles', () => {
        const component = getRenderedSC(<NavItemText/>);
        expect(component).toHaveStyleRule('color', defaultStyle.textColor);
    });
});
