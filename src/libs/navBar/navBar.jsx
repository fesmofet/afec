import React from 'react';
import PropTypes from 'prop-types';
import {
    NavBarWrapper,
    NavItem,
    NavItemText,
} from './styledComponent';

const NavBar = (props) => {
    const handleChangeMode = (event) => {
        props.changeModule(event.target.id);
    };

    const { dictionary, navBarItems } = props;
    return (
        <NavBarWrapper>
            {navBarItems && navBarItems.length ? navBarItems.map((item) =>
                <NavItem key = {item}
                         id = {item}
                         onClick = {handleChangeMode}>
                    <NavItemText
                        id = {item}
                        children={dictionary.navBar[item]}
                    />
                </NavItem>
            ) : null}
        </NavBarWrapper>
    );
};

NavBar.propTypes = {
    dictionary: PropTypes.object.isRequired,
    changeModule: PropTypes.func.isRequired,
    navBarItems: PropTypes.array.isRequired,
};

export default React.memo(NavBar);
