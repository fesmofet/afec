import styled from 'styled-components';

export const defaultStyle = {
    navBarBorder: '#1b115c',
    btnBgColor: '#236ea1',
    textColor: '#fdfdfb',
};

export const NavBarWrapper = styled.div`
  width: 100%;
  min-height: 40px;
  box-sizing: border-box;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  border: 1px solid ${props => props.theme.navBarBorder ? props.theme.navBarBorder : defaultStyle.navBarBorder};
`;

export const NavItem = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  min-width: 90px;
  min-height: 30px;
  margin: auto 5px;
  border-radius: 2px;
  width: calc(60%/6);
  background: ${props => props.theme.btnBgColor ? props.theme.btnBgColor : defaultStyle.btnBgColor};
  
  &:hover{
      background: ${props => props.theme.navBarBorder ? props.theme.navBarBorder : defaultStyle.navBarBorder};
  }
`;

export const NavItemText = styled.div`
 font-size: 20px;
 font-weight: normal;
 color: ${props => props.theme.textColor ? props.theme.textColor : defaultStyle.textColor};
`;
