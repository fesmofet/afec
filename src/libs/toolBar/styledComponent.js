import styled from 'styled-components';

export const ToolBarContainer = styled.div`
  width: 100%;
  height: 60px;
  margin-bottom: 20px;
  display: flex;
  align-items: flex-end;
  justify-content: flex-end;
`;

export const Avatar = styled.div`
  width: 50px;
  height: 50px;
  cursor: pointer;
  background-size: 50px;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('../../../img/user.svg');
  margin-right: 10px;
`;

export const ComeBack = styled.div`
  width: 50px;
  height: 50px;
  cursor: pointer;
  background-size: 50px;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('../../../img/back.svg');
  margin-right: 10px;
`;

export const ToolBarSettings = styled.div`
  width: 50px;
  height: 50px;
  cursor: pointer;
  background-size: 50px;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('../../../img/settings.svg');
`;

export const ToolBarFullscreen = styled.div`
  width: 45px;
  height: 50px;
  cursor: pointer;
  margin-left: 20px;
  background-size: 45px;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('../../../img/fullscreen.svg');
`;

export const ToolBarLogout = styled.div`
  width: 50px;
  height: 50px;
  cursor: pointer;
  margin-left: 20px;
  background-size: 50px;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('../../../img/logout.svg');
`;
