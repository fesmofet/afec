import React from 'react';
import PropTypes from 'prop-types';
import {
    ToolBarContainer,
    ToolBarSettings,
    ToolBarFullscreen,
    ToolBarLogout,
    Avatar,
    ComeBack,
} from './styledComponent.js';

const Body = (props) => {
    const {history, logout, fullScreen, toggleAccountStudent, role, isAccount} = props;
    const handleLogout = () => logout(history);
    const showStudent = () => !isAccount ? <Avatar onClick={toggleAccountStudent ? toggleAccountStudent : null} /> : <ComeBack onClick={toggleAccountStudent ? toggleAccountStudent : null}/>;

    return (
        <ToolBarContainer>  
            {role === 'student' ? showStudent() : null}
            <ToolBarSettings onClick={toggleAccountStudent ? toggleAccountStudent : null} />
            <ToolBarFullscreen onClick={fullScreen} />
            <ToolBarLogout onClick={handleLogout} />
        </ToolBarContainer>
    );
};

Body.propTypes = {
    history: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired,
    fullScreen: PropTypes.func.isRequired,
    toggleAccountStudent: PropTypes.func,
    role: PropTypes.string,
    isAccount: PropTypes.bool,
};

export default React.memo(Body);
