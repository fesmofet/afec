import ErrorBlockMemo from '../ErrorBlock.jsx';
import * as styledComponents from '../styledComponents';

const ErrorBlock = ErrorBlockMemo.type;

describe('ErrorBlock react component', () => {
    const props = {
        textColor: '#ffffff',
        errorMessage: 'error message',
    };

    it('snapshot created, should rendered correctly', done => {
        const wrapper = shallow(<ErrorBlock {...props}/>);

        expect(wrapper).matchSnapshot();

        done();
    });
});

describe('ErrorBlock styled components', () => {
    it('ErrorBlock should have correct color, when textColor was transferred', () => {
        const component = getRenderedSC(<styledComponents.MessageText textColor={'#000000'}/>);

        expect(component).toHaveStyleRule('color', '#000000');
    });

    it('ErrorBlock should have correct color, when textColor wasn\'t transferred', () => {
        const component = getRenderedSC(<styledComponents.MessageText/>);

        expect(component).toHaveStyleRule('color', '#d1193e');
    });
});
