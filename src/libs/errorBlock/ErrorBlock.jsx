import React from 'react';
import PropTypes from 'prop-types';
import { ErrorBlockWrapper, MessageText } from './styledComponents';

const ErrorBlock = ({ errorMessage, textColor }) => 
    <ErrorBlockWrapper>
        <MessageText textColor={textColor} children={errorMessage}/>
    </ErrorBlockWrapper>
;

ErrorBlock.propTypes = {
    textColor: PropTypes.string,
    errorMessage: PropTypes.string.isRequired,
};

export default React.memo(ErrorBlock);