import styled from 'styled-components';

const defaultColor = '#d1193e';

export const ErrorBlockWrapper = styled.div`
   width: 100%;
   height: 100%;
   display: flex;
   align-items: center;
   justify-content: center;
`;

export const MessageText = styled.span`
   color: ${props => props.textColor ? props.textColor : defaultColor};
   text-align: center;
`;