import React from 'react';
import { render } from 'react-dom';
import { compose, createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { rootReducer } from './redux/rootReducer.js';
import rootSaga from './sagas/rootSaga';
import Routing from './modules/routing/Routing.jsx';

const saga = createSagaMiddleware();
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancer(applyMiddleware(saga)));
saga.run(rootSaga);

const app = 
    <Provider store={store}>
        <Routing/>
    </Provider>
;
render(app, document.getElementById('root'));
