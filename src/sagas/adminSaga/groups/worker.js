import rout from '../../../constants/rout';
import { put, call } from 'redux-saga/effects';
import * as actions from '../../../constants/actions';
import * as request from '../../../utils/request/request';
import { mergeArraysOfObject } from '../../../utils/helpers/helper';

export function* getGroupsPageData() {
    const token = localStorage.getItem('token');
    const groupsData = yield call(request.getRequestSender, { path: rout.getGroupsPage, headers: { Authorization: token } });
    const store = groupsData.data.groups.map(group => ({
        id: group.group_id,
        group: group.name,
        teacherLN: group.last_name,
        teacherFN: group.first_name,
        city: group.city,
        level: group.level,
        attendance: '-',
    }));
    const countStudents = yield call(request.getRequestSender, { path: rout.getGroupsCountStudents, headers: { Authorization: token } });
    const count = countStudents.data.groups.map(group => ({
        id: group.group_id,
        count: group.count,
    }));
    const join = mergeArraysOfObject(store, count);
    yield put(actions.setAllGroups(join));
}

export function* getAllTeachers() {
    const token = localStorage.getItem('token');
    const payload = yield call(request.getRequestSender, { path: rout.getTeachers, headers: { Authorization: token } });
    const store = payload.data.teachersInfo.map(user => ({
        id: user.id,
        lastName: user.last_name,
        firstName: user.first_name,
        email: user.email,
        phone: user.phone,
        role: user.role,
    }));
    yield put(actions.setAllTeachers(store));
}

export function* deleteGroupAdmin(action) {
    const token = localStorage.getItem('token');
    yield call(request.postRequestSender, {
        path: rout.deleteGroupAdmin,
        data: { id: action.payload },
        headers: { Authorization: token },
    });
    yield put(actions.setDeleteConfirm(false));
    yield call(getGroupsPageData);
    yield put(actions.toggleModalWindow({ modal: 'deletingModal', isOpen: false }));
}
