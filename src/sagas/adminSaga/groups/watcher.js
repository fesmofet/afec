import { takeEvery } from 'redux-saga/effects';
import actionTypes from '../../../constants/actionTypes.js';
import { getGroupsPageData, getAllTeachers, deleteGroupAdmin } from './worker';

export function* watchGroupsPage() {
    yield takeEvery(actionTypes.GET_ALL_GROUPS, getGroupsPageData);
    yield takeEvery(actionTypes.GET_ALL_TEACHERS, getAllTeachers);
    yield takeEvery(actionTypes.DELETE_GROUP_ADMIN, deleteGroupAdmin);
}