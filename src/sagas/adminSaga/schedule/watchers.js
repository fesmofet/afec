import actionTypes from '../../../constants/actionTypes';
import { takeLatest } from 'redux-saga/effects';
import * as workers from './workers';

export function* watchGetSchedule() {
    yield takeLatest(actionTypes.GET_SCHEDULE, workers.getSchedule);
}

