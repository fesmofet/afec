import actionTypes from '../../../constants/actionTypes';
import { call, put } from 'redux-saga/effects';
import * as request from '../../../utils/request/request';
import * as rout from '../../../constants/rout';
import moment from 'moment';

export function* getSchedule(action) {
    const token = localStorage.getItem('token');
    if (!action.payload) {
        return;
    }
    const scheduleData = yield call(request.postRequestSender, { path: rout.getSchedule, data: action.payload, headers: { Authorization: token } });
    const data = scheduleData.data.lessons;
    let weekDates = [];
    for (let i = 0; i < 7; i++) {
         weekDates.push(moment(action.payload.from).add(i, 'days').format('YYYY-MM-DD'));
     }
     const lessons = [];
     for (let i = 0; i < weekDates.length; i++) {
         let tempLesson = {};
         tempLesson.date = `${weekDates[i]}`;
         tempLesson.groups = [];
         tempLesson.teachers = [];
         tempLesson.time = [];
         tempLesson.type = [];
         if (data) {
             data.map(item => {
                 if (moment(Number(item.start)).format('YYYY-MM-DD') === weekDates[i]) {
                     tempLesson.groups.push(item.name);
                     tempLesson.teachers.push(`${item.first_name} ${item.last_name}`);
                     tempLesson.time.push(`${moment(Number(item.start)).format('HH:mm')}  -  ${moment(Number(item.finished)).format('HH:mm')}`);
                     tempLesson.type.push(item.type);
                 }
             });
             lessons.push(tempLesson);
         }
     }
     const payload = lessons;
     yield put({ type: actionTypes.SET_SCHEDULE_DATA, payload });
     yield put({ type: actionTypes.SET_SCHEDULE_CURRENT_DATE, payload: action.payload.from });
}
