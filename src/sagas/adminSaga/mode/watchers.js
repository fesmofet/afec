import actionTypes from '../../../constants/actionTypes';
import { takeLatest } from 'redux-saga/effects';
import * as workers from './workers';

export function* watchChangeMode() {
    yield takeLatest(actionTypes.CHANGE_ADMIN_MODULE, workers.changeMode);
}

