import { takeLatest, call, put, select } from 'redux-saga/effects';
import { watchChangeMode } from './watchers';
import { changeMode } from './workers';
import actionTypes from '../../../constants/actionTypes';
import * as workers from './workers';
import { saveDataLS } from '../../../utils/helpers/helper';
import { getCurrentModule } from '../../../constants/selectors';

describe('Testing saga watcher ChangeMode: ', () => {
    const generator = watchChangeMode();
    it('test takeLatest CHANGE_ADMIN_MODULE', () => {
        const actual = generator.next();
        assert.deepEqual(
            actual.value,
            takeLatest(actionTypes.CHANGE_ADMIN_MODULE, workers.changeMode)
        );
    });
    it('ChangeMode done', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing worker ChangeMode with no payload: ', () => {
    const action = {
        payload: null,
    };
    const generator = changeMode(action);
    it('test changeMode with empty payload', () => {
        const actual = generator.next();
        assert.deepEqual(
            actual.value,
            undefined
        );
    });
    it('test changeMode done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing worker ChangeMode with  {payload: "students"}', () => {
    const action = {
        payload: 'students',
    };

    const generator = changeMode(action);
    it('test saga call saveDataLS', () => {
        const actual = generator.next();
        assert.deepEqual(
            actual.value,
            call(saveDataLS, 'pageMode', action.payload)
        );
    });
    it('test saga select', () => {
        const actual = generator.next('users');
        assert.deepEqual(
            actual.value,
            select(getCurrentModule),
        );
    });

    it('test saga put  { type: actionTypes.CHANGE_ADMIN_MODE, payload }', () => {
        const actual = generator.next();
        const payload = action.payload;
        assert.deepEqual(
            actual.value,
            put({ type: actionTypes.CHANGE_ADMIN_MODE, payload })
        );
    });

    it('test saga done', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

