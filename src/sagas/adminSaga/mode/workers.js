import actionTypes from '../../../constants/actionTypes';
import { call, select, put } from 'redux-saga/effects';
import { saveDataLS } from '../../../utils/helpers/helper';
import { getCurrentModule } from '../../../constants/selectors';

export function* changeMode(action) {
    if (!action.payload) {
        return;
    }
    const pageMode = action.payload;
    yield call(saveDataLS, 'pageMode', pageMode);
    const currentPageMode = yield select(getCurrentModule);
    if (currentPageMode === pageMode) {
        return;
    }
    const payload = pageMode;
    yield put({ type: actionTypes.CHANGE_ADMIN_MODE, payload });
}
