import { getAllUsers, setUserRole } from './worker';
import { watchAdmin } from './watcher';
import { takeEvery } from 'redux-saga/effects';
import actionTypes from '../../../constants/actionTypes';

describe('Testing watchAdmin: ', () => {
    const generator = watchAdmin();

    it('watchAdmin takeLatest GET_ALL_USERS', () => {
        const expected = takeEvery(actionTypes.GET_ALL_USERS, getAllUsers);
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('watchAdmin takeLatest GHANGE_USER_ROLE', () => {
        const expected = takeEvery(actionTypes.GHANGE_USER_ROLE, setUserRole);
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('watchAdmin done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});