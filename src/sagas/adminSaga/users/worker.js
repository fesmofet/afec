import { put, call, select } from 'redux-saga/effects';
import * as actions from '../../../constants/actions';
import * as request from '../../../utils/request/request';
import rout from '../../../constants/rout';

export function* getGroups() {
    const token = localStorage.getItem('token');
    const payload = yield call(request.getRequestSender, { path: rout.getGroups, headers: { Authorization: token } });
    const store = yield payload.data.groups.map(group => ({
        id: group.id,
        name: group.name,
        level: group.level,
        teacherId: group.teacher_id,
    }));
    yield put(actions.setGroups(store));
}

const getResultSearch = (value, itemValue) => {
    if (!value) {
        return true;
    } else if (value && itemValue) {
        return itemValue.toLowerCase().includes(value.toLowerCase());
    } else if (!itemValue && value) {
        return false;
    }
};

export function* searchUser(action) {
    yield put(actions.setUserSearchCriteria(action.payload));
    const { lastName, firstName, email, phone, role, groupForUser, teacherLevel } = action.payload;
    const users = yield select(state => state.admin.allUsers);
    let updateUsers = users.filter(item => getResultSearch(lastName, item.lastName)
        && getResultSearch(firstName, item.firstName)
        && getResultSearch(email, item.email)
        && getResultSearch(phone, item.phone)
        && getResultSearch(role, item.role)
        && getResultSearch(groupForUser, item.currentGroup)
        && getResultSearch(teacherLevel, item.teacherLevel)
    );
    yield put(actions.setSearchedUsers(updateUsers));
}

export function* getAllUsers(action) {
    const token = localStorage.getItem('token');
    const payload = yield call(request.postRequestSender, {
        path: rout.users,
        data: { userId: action.payload },
        headers: { Authorization: token },
    });
    const store = yield payload.data.users.map(user => ({
        id: user.id,
        email: user.email,
        firstName: user.first_name,
        lastName: user.last_name,
        phone: user.phone,
        birthday: user.date_of_birth,
        password: user.password,
        salaryPerHour: user.salaryperhour,
        teacherLevel: user.teacherlevel,
        keyword: user.keyword,
        role: user.role,
        currentGroup: user.name,
    }));
    yield put(actions.setAllUsers(store));
}

export function* deleteUser(action) {
    yield action.payload.role === 'teacher' ? call(deleteTeacher, action) : call(deleteUserAndAdmin, action);
}

function* deleteUserAndAdmin(action) {
    const token = localStorage.getItem('token');
    let response = yield call(request.postRequestSender, {
        path: rout.deleteUser,
        data: { userId: action.payload.userId },
        headers: { Authorization: token },
    });
    yield put(actions.setDeleteConfirm(false));
    yield call(setUsersAfterDelete, response, action);
    yield put(actions.toggleModalWindow({ modal: 'deletingModal', isOpen: false }));
}

function* deleteTeacher(action) {
    const token = localStorage.getItem('token');
    let response = yield call(request.postRequestSender, {
        path: rout.deleteTeacher,
        data: { userId: action.payload.userId },
        headers: { Authorization: token },
    });
    yield put(actions.setDeleteConfirm(false));
    yield call(setUsersAfterDelete, response, action);
    yield put(actions.toggleModalWindow({ modal: 'deletingModal', isOpen: false }));
}

function* setUsersAfterDelete(response, action) {
    if (response.data.message === 'DELETE COMPLETE') {
        const users = yield select(state => state.admin.allUsers);
        const updatedUsers = yield users.filter((item, index) => index !== action.payload.id);
        yield put(actions.setAllUsers(updatedUsers));
    } else {
        console.log(response.data.message); //FIXME  ADD NOTIFICATION
    }
}

export function* updateUser(action) {
    switch (action.payload.userNewData.role) {
        case 'student': yield updateStudent(action); break;
        case 'teacher': yield updateTeacher(action); break;
        case 'admin': yield updateTeacher(action); break;
        default: return;
    }
}

function* updateStudent(action) {
    const token = localStorage.getItem('token');
    let response = yield call(request.postRequestSender, {
        path: rout.updateUser,
        data: action.payload.userNewData,
        headers: { Authorization: token },
    });
    yield call(setUsersInStore, response, action);
}

function* updateTeacher(action) {
    const token = localStorage.getItem('token');
    const { id, firstName, lastName, email, phone, role, teacherLevel, salaryPerHour } = action.payload.userNewData;
    const dataSend = { id, firstName, lastName, email, phone, role, teacherLevel, salaryPerHour };
    let response = yield call(request.postRequestSender, {
        path: rout.updateTeacher,
        data: dataSend,
        headers: { Authorization: token },
    });
    yield call(setUsersInStore, response, action);
}

function* setUsersInStore(response, action) {
    if (response.data.message === 'UPDATE COMPLETE') {
        const { id, first_name: firstName, last_name: lastName, email, phone, date_of_birth: birthday, password, salaryperhour: salaryPerHour, teacherlevel: teacherLevel, keyword, role, group: currentGroup } = response.data.updateUser;
        const users = yield select(state => state.admin.allUsers);
        const updatedUsers = yield users.map(item => item.id === action.payload.userNewData.id ? {
            id,
            email,
            firstName,
            lastName,
            phone,
            birthday,
            password,
            salaryPerHour,
            teacherLevel,
            keyword,
            role,
            currentGroup,
        } : item);
        yield put(actions.setUpdatedUsers(updatedUsers));
        const searchCriteriaUsers = yield select(state => state.admin.userSearchCriteria);
        yield call(searchUser, { payload: searchCriteriaUsers });
    } else {
        console.log(response.data.message); //FIXME  ADD NOTIFICATION
    }
}
