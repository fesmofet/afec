import * as worker from './worker';
import { call, put } from 'redux-saga/effects';
import * as actions from '../../../constants/actions.js';
import * as request from '../../../utils/request/request';

describe('Testing getAllUsers: ', () => {
    const data = {
        'Content-Type': 'aplication/json',
        Authorization: '123456789',
    };
    const generator = worker.getAllUsers();

    it('getAllUsers call func get response from server', () => {
        const expected = call(request.getRequestSender, { path: 'users', data: data });
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('getAllUsers put func set data to store', () => {
        const expected = put(actions.setAllUsers(payload.data));
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('getAllUsers done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});
