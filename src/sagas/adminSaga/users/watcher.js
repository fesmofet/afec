import actionTypes from '../../../constants/actionTypes';
import { takeEvery, takeLatest } from 'redux-saga/effects';
import { getAllUsers, getGroups, deleteUser, updateUser, searchUser } from './worker';

export function* watchAdmin() {
    yield takeEvery(actionTypes.GET_ALL_USERS, getAllUsers);
    yield takeLatest(actionTypes.GET_GROUPS, getGroups);
    yield takeLatest(actionTypes.DELETE_USER, deleteUser);
    yield takeLatest(actionTypes.UPDATE_USER, updateUser);
    yield takeLatest(actionTypes.SEARCH_USER, searchUser);
}
