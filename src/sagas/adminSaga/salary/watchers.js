import actionTypes from '../../../constants/actionTypes';
import { takeLatest } from 'redux-saga/effects';
import * as workers from './workers';

export function* watchSalaryMode() {
    yield takeLatest(actionTypes.GET_TEACHER_SALARY_DATA, workers.getTeachersSalary);
}

