import actionTypes from '../../../constants/actionTypes';
import { call, put } from 'redux-saga/effects';
import * as request from '../../../utils/request/request';
import * as rout from '../../../constants/rout';

export function* getTeachersSalary(action) {
    const token = localStorage.getItem('token');
    if (!action.payload) {
        return;
    }
    const salaryData = yield call(request.postRequestSender, { path: rout.getTeachersSalary, data: action.payload, headers: { Authorization: token } });
    const data = salaryData.data.salaryData;
    const result = [];
        data.map(item => {
        let temp = { ...item };
        temp.money = item.salaryperhour * item.lessons_amount * 1.5;
        temp.hours = item.lessons_amount * 1.5;
        result.push(temp);
    });
    const payload = result;
    yield put({ type: actionTypes.SET_TEACHERS_SALARY_DATA, payload });
}
