import { takeLatest, put, call, apply } from 'redux-saga/effects';
import { watchLogin } from './watchers.js';
import actionTypes from '../../constants/actionTypes.js';
import * as workers from './workers.js';
import * as actions from '../../constants/actions.js';
import config from '../../constants/config';
import rout from '../../constants/rout.js';
import axios from 'axios';

describe('Testing watchLogin: ', () => {
    const generator = watchLogin();
    it('watchLogin takeLatest LOGIN', () => {
        const actual = generator.next();
        assert.deepEqual(
            actual.value,
            takeLatest(actionTypes.LOGIN, workers.login)
        );
    });

    it('watchLogin done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing login: ', () => {
    const errors = [];
    const action = { payload: {}, history: {} };
    const generator = workers.login(action);
    it('Testing login: should put clearAuthInputErrors', () => {
        const expected = put(actions.clearAuthInputErrors());
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('Testing login: should call loginCheckFields', () => {
        const expected = call(workers.loginCheckFields, errors, action);
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('Testing login: done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing loginCheckFields: errors != 0 ', () => {
    const errors = ['email'];
    const action = { payload: {} };
    const generator = workers.loginCheckFields(errors, action);
    it('Testing loginCheckFields: should put setAuthInputData', () => {
        const payload = {
            invalidFields: [...errors],
        };
        assert.deepEqual(
            generator.next().value,
            put(actions.setAuthInputData(payload))
        );
    });

    it('loginCheckFields: errors != 0 done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing loginCheckFields: errors === 0 ', () => {
    const errors = [];
    const action = {
        payload: {
            email: 'admin@gmail.com',
            password: 'password',
        } };
    const generator = workers.loginCheckFields(errors, action);
    it('Testing loginCheckFields: errors === 0: axios.post', () => {
        assert.deepEqual(
            generator.next().value,
            apply(axios, axios.post, [`${config.pathToServer}${rout.auth}`, action.payload])
        );
    });
});

describe('Testing signInSuccess: ', () => {
    const payload = {
        data: {
            user: {
                id: 1,
                first_name: 'Andrey',
                last_name: 'Belichenko',
                phone: '+380660643308',
                email: 'admin@gmail.com',
                keyword: 'andrey',
                role: 'student',
                token: '123',
                date_of_birth: '22.08.2001',
                password: 'qwerty',
            },
        },
    };
    const expectPayload = {
        id: 1,
        firstName: 'Andrey',
        lastName: 'Belichenko',
        phone: '+380660643308',
        email: 'admin@gmail.com',
        keyword: 'andrey',
        role: 'student',
        token: '123',
        birthday: '22.08.2001',
        password: 'qwerty',
    };
    const action = { history: [] };
    const generator = workers.authSuccess(action, payload);

    it('Testing put into setUserData ', () => {
        const actual = generator.next();
        assert.deepEqual(
            actual.value,
            put(actions.setUserData(expectPayload))
        );
    });

    it('Testing put into accountReducer ', () => {
        const actual = generator.next();
        assert.deepEqual(
            actual.value,
            call(workers.redirectFromAuthorization, action, expectPayload.role)
        );
    });

    it('Testing signInSuccess done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing redirectFromAuthorization: ', () => {
    it('Testing put into redirectFromAuthorization case 1', () => {
        const action = { payload: {}, history: [] };
        const data = { role: 'admin' };
        const expectAction = ['/admin'];
        assert.deepEqual(
            workers.redirectFromAuthorization(action, data.role),
            expectAction
        );
    });

    it('Testing put into redirectFromAuthorization case 2', () => {
        const action = { payload: {}, history: [] };
        const data = { role: 'student' };
        const expectAction = ['/student'];
        assert.deepEqual(
            workers.redirectFromAuthorization(action, data.role),
            expectAction
        );
    });

    it('Testing put into redirectFromAuthorization case 3', () => {
        const action = { payload: {}, history: [] };
        const data = { role: 'teacher' };
        const expectAction = ['/teacher'];
        assert.deepEqual(
            workers.redirectFromAuthorization(action, data.role),
            expectAction
        );
    });
});
