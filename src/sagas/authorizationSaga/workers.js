import * as actions from '../../constants/actions';
import { put, call, apply } from 'redux-saga/effects';
import { isAuthorizationFieldsEmpty } from '../../utils/validation/validation';
import config from '../../constants/config';
import rout from '../../constants/rout.js';
import axios from 'axios';
import * as request from '../../utils/request/request';

export function* login(action) {
    yield put(actions.clearAuthInputErrors());
    let errors = [];
    for (let input in action.payload) {
        let error = isAuthorizationFieldsEmpty(input, action.payload[input]);
        error !== 'OK' ? errors.push(error) : null;
    }
    yield action && action.payload && call(loginCheckFields, errors, action);
}

export function* loginCheckFields(errors, action) {
    if (errors.length) {
        yield put(actions.setAuthInputData({ invalidFields: [...errors] }));
    } else {
        try {
            const payload = yield apply(axios, axios.post, [`${config.pathToServer}${rout.auth}`, action.payload]);
            yield payload && call(authSuccess, action, payload);
        } catch (err) {
            yield put(actions.authError());
        }
    }
}

export function* authSuccess(action, payload) {
    localStorage.setItem('token', payload.data.token);
    const {
        id,
        email,
        first_name: firstName,
        last_name: lastName,
        phone,
        date_of_birth: birthday,
        password,
        role,
        salaryperhour: salaryPerHour,
        teacherlevel: teacherLevel,
        keyword,
        token,
    } = payload.data.user;
    const data = {
        id,
        email,
        firstName,
        lastName,
        phone,
        birthday,
        password,
        role,
        salaryPerHour,
        teacherLevel,
        keyword,
        token,
    };
    if (role === 'student') {
        const user_id = { id: payload.data.user.id };
        const response = yield call(request.postRequestSender, {
            path: rout.getStudentGroup,
            data: user_id,
            headers: { Authorization: payload.data.token },
        });
        if (response.data.groups.length > 0) {
            data.groups = [response.data.groups[0].group_id];
        } else {
            data.groups = [];
        }
    }
    if (role === 'teacher') {
        const user_id = { id: payload.data.user.id };
        const response = yield call(request.postRequestSender, {
            path: rout.getTeachersGroupsIds,
            data: user_id,
            headers: { Authorization: payload.data.token },
        });
        if (response.data.groups.length > 0) {
            data.groups = [...response.data.groups.map(item => item.id)];
        } else {
            data.groups = [];
        }
    }
    if (role === 'admin') {
        data.groups = [];
    }
    yield localStorage.setItem('currentUserData', JSON.stringify(data));
    yield put(actions.setUserData(data));
    yield call(redirectFromAuthorization, action, payload.data.user.role);
}

export const redirectFromAuthorization = (action, role) => {
    switch (role) {
        case 'admin':
            action.history.push('/admin');
            return action.history;
        case 'student':
            action.history.push('/student');
            return action.history;
        case 'teacher':
            action.history.push('/teacher');
            return action.history;
        default:
            return;
    }
};
