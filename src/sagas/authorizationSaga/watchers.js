import actionTypes from '../../constants/actionTypes';
import { takeLatest } from 'redux-saga/effects';
import * as workers from './workers';

export function* watchLogin() {
    yield takeLatest(actionTypes.LOGIN, workers.login);
}
