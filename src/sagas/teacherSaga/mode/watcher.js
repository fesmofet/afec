import actionTypes from '../../../constants/actionTypes';
import { takeLatest } from 'redux-saga/effects';
import * as workers from './workers';

export function* watchChangeTeacherMode() {
    yield takeLatest(actionTypes.CHANGE_TEACHER_MODULE, workers.changeTeacherMode);
}
