import { saveDataLS } from '../../../utils/helpers/helper';
import { getCurrentModuleTeacher } from '../../../constants/selectors';
import actionTypes from '../../../constants/actionTypes';
import { call, select, put } from 'redux-saga/effects';

export function* changeTeacherMode(action) {
    if (!action.payload) {
        return;
    }
    const pageMode = action.payload;
    yield call(saveDataLS, 'pageModeTeacher', pageMode);
    const currentPageMode = yield select(getCurrentModuleTeacher);
    if (currentPageMode === pageMode) {
        return;
    }
    const payload = pageMode;
    yield put({ type: actionTypes.SET_TEACHER_MODE, payload });
}
