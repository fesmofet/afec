import { call, select, put } from 'redux-saga/effects';
import * as request from '../../../utils/request/request';
import rout from '../../../constants/rout';
import * as actions from '../../../constants/actions';

export function* getAttendance(action) {
    const token = localStorage.getItem('token');
    const response = yield call(request.postRequestSender, {
        path: rout.getAttendance,
        data: action.payload,
        headers: { Authorization: token },
    });
    const store = yield response.data.attendances.map(attendance => ({
        groupName: attendance.name,
        groupLevel: attendance.level,
        groupId: attendance.group_id,
        lessonId: attendance.lesson_id,
        studentId: attendance.student_id,
        attendance: attendance.attendance,
        lastName: attendance.last_name,
        firstName: attendance.first_name,
    }));
    yield put(actions.setAttendances(store));
    const attendancesSearch = yield select(state => state.teacher.attendanceSearchCriteria);
    let sendCriterias = { payload: attendancesSearch };
    yield call(searchAttendance, sendCriterias);
}

export function* updateAttendance(action) {
    const token = localStorage.getItem('token');
    yield call(request.postRequestSender, {
        path: rout.updateAttendance,
        data: action.payload,
        headers: { Authorization: token },
    });
}

const getResultSearch = (value, itemValue) => {
    if (!value) {
        return true;
    } else if (value && itemValue) {
        return itemValue.toLowerCase().includes(value.toLowerCase());
    } else if (!itemValue && value) {
        return false;
    }
};

export function* searchAttendance(action) {
    const { lastName, firstName } = action.payload;
    yield put(actions.setSearchCriteriasAttendances(action.payload));
    const attendances = yield select(state => state.teacher.attendance);
    let updateAttendances = attendances.filter(item => getResultSearch(lastName, item.lastName) && getResultSearch(firstName, item.firstName));
    yield put(actions.setSearchAttendances(updateAttendances));
}
