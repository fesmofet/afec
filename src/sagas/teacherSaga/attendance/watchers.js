import actionTypes from '../../../constants/actionTypes';
import { takeLatest } from 'redux-saga/effects';
import * as workers from './workers';

export function* watchAttendance() {
    yield takeLatest(actionTypes.GET_ATTENDANCE, workers.getAttendance);
    yield takeLatest(actionTypes.UPDATE_ATTENDANCE, workers.updateAttendance);
    yield takeLatest(actionTypes.SEARCH_ATTENDANCES, workers.searchAttendance);
}
