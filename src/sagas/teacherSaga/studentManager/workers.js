import { call, select, put } from 'redux-saga/effects';
import * as request from '../../../utils/request/request';
import rout from '../../../constants/rout';
import * as actions from '../../../constants/actions';

export function* getStudents(action) {
    const token = localStorage.getItem('token');
    const response = yield call(request.postRequestSender, {
        path: rout.getStudents,
        data: { id: action.payload },
        headers: { Authorization: token },
    });
    const store = yield response.data.students.map(student => ({
        groupId: student.group_id,
        groupName: student.name,
        studentId: student.student_id,
        lastName: student.last_name,
        firstName: student.first_name,
    }));
    yield put(actions.setStudents(store));
    yield put(actions.setSearchStudents(store));
}

export function* getTeachersGroups(action) {
    const token = localStorage.getItem('token');
    const response = yield call(request.postRequestSender, {
        path: rout.getTeachersGroups,
        data: { id: action.payload },
        headers: { Authorization: token },
    });
    const store = yield response.data.groups.map(group => ({
        groupId: group.id,
        groupName: group.name,
        level: group.level,
        city: group.city,
    }));
    yield put(actions.setTeachersGroups(store));
}

export function* updateStudent(action) {
    const token = localStorage.getItem('token');
    const response = yield call(request.postRequestSender, {
        path: rout.updateStudent,
        data: action.payload,
        headers: { Authorization: token },
    });
    const students = yield select(state => state.teacher.students);
    const updatedStudents = yield students.map(item => item.studentId === response.data.updateStudent.id ? {
        groupId: response.data.updateStudent.groupId,
        groupName: response.data.updateStudent.group,
        studentId: response.data.updateStudent.id,
        lastName: response.data.updateStudent.last_name,
        firstName: response.data.updateStudent.first_name,
    } : item);
    yield put(actions.setStudents(updatedStudents));
    const searchCriteriaStudents = yield select(state => state.teacher.studentSearchCriteria);
    yield call(searchStudents, { payload: searchCriteriaStudents });
}

const getResultSearch = (value, itemValue) => {
    if (!value) {
        return true;
    } else if (value && itemValue) {
        return itemValue.toLowerCase().includes(value.toLowerCase());
    } else if (!itemValue && value) {
        return false;
    }
};

export function* searchStudents(action) {
    yield put(actions.setStudentsSearchCriteria(action.payload));
    const { lastName, firstName, group } = action.payload;
    const students = yield select(state => state.teacher.students);
    let updateStudents = students.filter(item => getResultSearch(lastName, item.lastName)
        && getResultSearch(firstName, item.firstName)
        && getResultSearch(group, item.groupName)
    );
    yield put(actions.setSearchStudents(updateStudents));
}
