import actionTypes from '../../../constants/actionTypes';
import { takeLatest } from 'redux-saga/effects';
import * as workers from './workers';

export function* watchStudentManager() {
    yield takeLatest(actionTypes.GET_STUDENTS, workers.getStudents);
    yield takeLatest(actionTypes.GET_TEACHERS_GROUPS, workers.getTeachersGroups);
    yield takeLatest(actionTypes.UPDATE_STUDENT, workers.updateStudent);
    yield takeLatest(actionTypes.SEARCH_STUDENTS, workers.searchStudents);
}
