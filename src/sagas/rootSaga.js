import { all, fork } from 'redux-saga/effects';
import { watchAdmin } from './adminSaga/users/watcher';
import { watchChangeLocale } from './localeSaga/watchers';
import { watchLogin } from './authorizationSaga/watchers';
import { watchChangeMode } from './adminSaga/mode/watchers';
import { watchGroupsPage } from './adminSaga/groups/watcher';
import { watchRegistration } from './registrationSaga/watcher';
import { watchChangeTheme } from '../managers/themeManager/saga';
import { watchModalWindow } from '../managers/modalManager/saga/watchers';
import { watchLogout, watchToggleFullScreen } from './commonSaga/watchers';
import { watchChangeTeacherMode } from './teacherSaga/mode/watcher.js';
import { watchStudentManager } from './teacherSaga/studentManager/watchers.js';
import { watchSalaryMode } from './adminSaga/salary/watchers';
import { watchGetSchedule } from './adminSaga/schedule/watchers';
import { watchStudent } from './studentSaga/watcher';
import { watchAttendance } from './teacherSaga/attendance/watchers.js';

const sagas = [
  watchAdmin,
  watchLogin,
  watchLogout,
  watchGetSchedule,
  watchChangeMode,
  watchGroupsPage,
  watchChangeTeacherMode,
  watchModalWindow,
  watchChangeTheme,
  watchRegistration,
  watchChangeLocale,
  watchToggleFullScreen,
  watchStudentManager,
  watchGroupsPage,
  watchSalaryMode,
  watchStudent,
  watchAttendance,
];

export default function* rootSaga() {
  yield all(sagas.map(fork));
}
