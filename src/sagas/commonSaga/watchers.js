import actionTypes from '../../constants/actionTypes';
import { takeLatest } from 'redux-saga/effects';
import * as workers from './workers';

export function* watchLogout() {
    yield takeLatest(actionTypes.LOGOUT, workers.logout);
}

export function* watchToggleFullScreen() {
    yield takeLatest(actionTypes.TOGGLE_FULL_SCREEN, workers.toggleFullScreen);
}
