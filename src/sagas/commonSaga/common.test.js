import { takeLatest, put } from 'redux-saga/effects';
import { watchLogout, watchToggleFullScreen } from './watchers.js';
import actionTypes from '../../constants/actionTypes.js';
import * as workers from './workers.js';
import * as actions from '../../constants/actions';

describe('Testing watcher logout: ', () => {
    const generator = watchLogout();
    it('logout takeLatest LOGOUT', () => {
        const actual = generator.next();
        assert.deepEqual(
            actual.value,
            takeLatest(actionTypes.LOGOUT, workers.logout)
        );
    });

    it('logout done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing watcher toggleFullScreen: ', () => {
    const generator = watchToggleFullScreen();
    it('logout takeLatest TOGGLE_FULL_SCREEN', () => {
        const actual = generator.next();
        assert.deepEqual(
            actual.value,
            takeLatest(actionTypes.TOGGLE_FULL_SCREEN, workers.toggleFullScreen)
        );
    });

    it('logout done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing worker logout: ', () => {
    const action = { history: [] };
    const generator = workers.logout(action);
    it('logout: put in store clearAuthUserData', () => {
        generator.next();
        const actual = generator.next();
        assert.deepEqual(
            actual.value,
            put(actions.clearAuthUserData())
        );
    });
    it('logout done equals to false', () => {
        const actual = generator.next();
        assert.isFalse(actual.done);
    });
    it('logout done equals to true', () => {
        generator.next();
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

