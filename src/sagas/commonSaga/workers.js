import * as actions from '../../constants/actions';
import { put } from 'redux-saga/effects';

export function* logout(action) {
    yield localStorage.clear();
    yield put(actions.clearAuthUserData());
    yield action.history.push('/');
}

export const toggleFullScreen = () => {
    document.fullscreenElement ? document.exitFullscreen() : document.documentElement.requestFullscreen();
};
