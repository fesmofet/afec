import { takeEvery } from 'redux-saga/effects';
import actionTypes from '../../constants/actionTypes';
import * as worker from './worker.js';

export function* watchStudent() {
    yield takeEvery(actionTypes.UPDATE_ACCOUNT_INFO, worker.workerUpdateAccountInfo);
    yield takeEvery(actionTypes.GET_ACCOUNT_INFO, worker.workerGetTableData);
    yield takeEvery(actionTypes.TOGGLE_ACCOUNT_STUDENT, worker.toggleAccountStudent);
}
