import { call, put, select } from 'redux-saga/effects';
import * as rout from '../../constants/rout';
import * as request from '../../utils/request/request';
import * as actions from '../../constants/actions.js';

export function* workerUpdateAccountInfo(action) {
    const token = localStorage.getItem('token');
    if (action.payload.password !== action.payload.passwordAgain) {
        return yield call(alert, 'Passwords must be the same');
    }
    yield call(request.postRequestSender, {
        path: rout.updateAccountInfo,
        data: action.payload,
        headers: { Authorization: token },
    });
    let currentUser = yield select(state => state.authorization.userData);
    currentUser.firstName = action.payload.firstName;
    currentUser.lastName = action.payload.lastName;
    currentUser.birthday = action.payload.dateOfBirth;
    currentUser.password = action.payload.password;
    localStorage.setItem('currentUserData', JSON.stringify(currentUser));
    yield put(actions.setUpdatedStudent(currentUser));
}

export function* workerGetTableData(action) {
    const token = localStorage.getItem('token');
    const response = yield call(request.postRequestSender, {
        path: rout.getAccountInfo,
        headers: { Authorization: token },
        data: { id: action.payload },
    });
    yield put(actions.setAccountTableData(response.data.info));
}

export function* toggleAccountStudent() {
    let isAccount = yield select(state => state.student.isAccount);
    yield put(actions.setIsAccount(!isAccount));
}
