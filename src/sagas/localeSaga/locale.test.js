import { takeEvery, put } from 'redux-saga/effects';
import { watchChangeLocale } from './watchers.js';
import actionTypes from '../../constants/actionTypes';
import * as workers from './workers.js';
import * as actions from '../../constants/actions.js';

describe('Testing watchChangeLocale: ', () => {
    const generator = watchChangeLocale();

    it('watchChangeLocale takeLatest CHANGE_LOCALE', () => {
        const actual = generator.next();
        assert.deepEqual(
            actual.value,
            takeEvery(actionTypes.CHANGE_LOCALE, workers.changeLocale)
        );
    });

    it('watchChangeLocale done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });

    it('changeLocale put SET_LOCALE', () => {
        const generator = workers.changeLocale(actions.setLocale('ru'));
        let actual = generator.next();
        assert.deepEqual(
            actual.value,
            put(actions.setLocale('ru'))
        );
    });

    it('changeLocale done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});
