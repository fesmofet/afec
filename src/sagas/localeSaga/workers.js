import { put } from 'redux-saga/effects';
import * as actions from '../../constants/actions.js';

export function* changeLocale(action) {
    yield put(actions.setLocale(action.payload));
    localStorage.setItem('language', action.payload);
}
