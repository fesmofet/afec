import { takeEvery } from 'redux-saga/effects';
import actionTypes from '../../constants/actionTypes.js';
import * as workers from './workers.js';

export function* watchChangeLocale() {
    yield takeEvery(actionTypes.CHANGE_LOCALE, workers.changeLocale);
}
