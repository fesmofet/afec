import * as worker from './worker';
import { call, put } from 'redux-saga/effects';
import * as actions from '../../constants/actions.js';
import * as request from '../../utils/request/request';

describe('Testing registration: ', () => {
    const errors = [];
    const action = { payload: {}, history: {} };
    const generator = worker.registration(action);

    it('registration put CLEAR_LOGIN_ERROR', () => {
        const expected = put(actions.clearInputErrors());
        let actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('should call validateOnSaga', () => {
        const expected = call(worker.validateOnSaga, errors, action);
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('should call registrationSendData', () => {
        const expected = call(worker.registrationSendData, errors, action);
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('registration done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing registrationSendData if we have errors in inputs: ', () => {
    const errors = ['email'];
    const action = { payload: {}, history: {} };
    const generator = worker.registrationSendData(errors, action);

    it('registrationSendData put SET_REGISTRATION_INPUTS_DATA', () => {
        const payload = {
            inputs: action.payload,
            invalidFields: [...errors],
        };
        const expected = put(actions.setRegInputsData(payload));
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('registrationSendData done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing registrationSendData if we do not have errors in inputs: ', () => {
    const data = {
        role: 'student',
        birthday: 1323232345654,
    };
    const errors = [];
    const action = { payload: { birthday: 1323232345654 }, history: { push: function () {} } };
    const generator = worker.registrationSendData(errors, action);

    it('registrationSendData call func send request to server', () => {
        const expected = call(request.postRequestSender, { path: 'reg', data: data });
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    // it('registrationSendData call switchResponse', () => { //FIXME
    //     const expected = call(worker.switchResponse, action, data);
    //     const actual = generator.next();
    //     assert.deepEqual(actual.value, expected)
    // });

    // it('registrationSendData done equals to true', () => {
    //     const actual = generator.next();
    //     assert.isTrue(actual.done);
    // });
});

describe('Testing switchResponse case "LOGIN ALREADY USED"', () => {
    const data = 'LOGIN ALREADY USED';
    const action = {};
    const generator = worker.switchResponse(action, data);

    it('switchResponse put SET_LOGIN_EXIST', () => {
        const expected = put(actions.setLoginExist());
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('registrationSendData done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing switchResponse case "REGISTRATION COMPLETE"', () => {
    const data = 'REGISTRATION COMPLETE';
    const action = { payload: {}, history: { push: function () {} } };
    const generator = worker.switchResponse(action, data);

    it('switchResponse put SET_LOGIN_EXIST', () => {
        const expected = put(actions.completeRegistration());
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('registrationSendData done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});

describe('Testing preparePayload', () => {
    it('switchResponse put SET_LOGIN_EXIST', () => {
        const payload = { birthday: 1323232345654, passwordAgain: '111', firstName: 'Boris' };
        const expected = { birthday: 1323232345654, role: 'student', firstName: 'Boris' };
        const actual = worker.preparePayload(payload);
        assert.deepEqual(actual, expected);
    });
});
