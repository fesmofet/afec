import { put, call } from 'redux-saga/effects';
import * as rout from '../../constants/rout';
import * as actions from '../../constants/actions.js';
import * as request from '../../utils/request/request';
import { validateAll } from '../../utils/validation/validation';

export function* registration(action) {
    let errors = [];
    yield put(actions.clearInputErrors());
    yield call(validateOnSaga, errors, action);
    yield call(registrationSendData, errors, action);
}

export function* registrationSendData(errors, action) {
    if (errors.length) {
        let payload = {
            inputs: action.payload,
            invalidFields: [...errors],
        };
        yield put(actions.setRegInputsData(payload));
    } else {
        let data = preparePayload(action.payload);
        const payload = yield call(request.postRequestSender, { path: rout.reg, data: data });
        yield call(switchResponse, action, payload.data);
    }
}

export function* switchResponse(action, data) {
    switch (data.message) {
        case 'USER ALREADY EXIST': {
            yield put(actions.setLoginExist());
            return;
        }
        case 'REGISTRATION COMPLETE': {
            yield put(actions.completeRegistration());
            action.history.push('/');
            return;
        }
        default: {
            return null;
        }
    }
}

export const preparePayload = payload => {
    delete payload.passwordAgain;
    payload.role = 'student';
    payload.birthday = new Date(payload.birthday).getTime();
    return payload;
};

export const validateOnSaga = (errors, action) => {
    for (let input in action.payload) {
        let error = validateAll(input, action.payload[input], action.payload.password);
        error === 'OK' || errors.push(error);
    }
};