import { registration } from './worker';
import { takeEvery } from 'redux-saga/effects';
import actionTypes from '../../constants/actionTypes.js';

export function* watchRegistration() {
    yield takeEvery(actionTypes.REGISTRATION, registration);
}