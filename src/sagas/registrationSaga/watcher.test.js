import { registration } from './worker';
import { watchRegistration } from './watcher';
import { takeEvery } from 'redux-saga/effects';
import actionTypes from '../../constants/actionTypes';

describe('Testing watchRegistration: ', () => {
    const generator = watchRegistration();

    it('watchRegistration takeLatest REGISTRATION', () => {
        const expected = takeEvery(actionTypes.REGISTRATION, registration);
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('watchRegistration done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});