import * as validate from './validation.js';

describe('Testing validateAll', () => {
    describe('test validateAll(), where inputType = email', () => {
        it('test must return email', () => {
            const actual = validate.validateAll('email', '!@#$%');
            const expect = 'email';
            assert.strictEqual(actual, expect);
        });

        it('test must return OK', () => {
            const actual = validate.validateAll('email', 'belichenko.andreyka22@gmail.com');
            const expect = 'OK';
            assert.strictEqual(actual, expect);
        });
    });

    describe('test validateAll(), where inputType = password', () => {
        it('test must return password', () => {
            const actual = validate.validateAll('password', 'qw');
            const expect = 'password';
            assert.strictEqual(actual, expect);
        });

        it('test must return OK', () => {
            const actual = validate.validateAll('password', 'qwerty');
            const expect = 'OK';
            assert.strictEqual(actual, expect);
        });
    });

    describe('test validateAll(), where inputType = passwordAgain', () => {
        it('test must return passwordAgain', () => {
            const actual = validate.validateAll('passwordAgain', 'qwerty', 'qwer');
            const expect = 'passwordAgain';
            assert.strictEqual(actual, expect);
        });

        it('test must return OK', () => {
            const actual = validate.validateAll('passwordAgain', 'qwerty', 'qwerty');
            const expect = 'OK';
            assert.strictEqual(actual, expect);
        });
    });

    describe('test validateAll(), where inputType = firstName', () => {
        it('test must return firstName', () => {
            const actual = validate.validateAll('firstName', '123');
            const expect = 'firstName';
            assert.strictEqual(actual, expect);
        });

        it('test must return OK', () => {
            const actual = validate.validateAll('firstName', 'qwerty');
            const expect = 'OK';
            assert.strictEqual(actual, expect);
        });
    });

    describe('test validateAll(), where inputType = lastName', () => {
        it('test must return lastName', () => {
            const actual = validate.validateAll('lastName', '123',);
            const expect = 'lastName';
            assert.strictEqual(actual, expect);
        });

        it('test must return OK', () => {
            const actual = validate.validateAll('lastName', 'qwerty',);
            const expect = 'OK';
            assert.strictEqual(actual, expect);
        });
    });

    describe('test validateAll(), where inputType = keyword', () => {
        it('test must return keyword', () => {
            const actual = validate.validateAll('keyword', '123');
            const expect = 'keyword';
            assert.strictEqual(actual, expect);
        });

        it('test must return OK', () => {
            const actual = validate.validateAll('keyword', 'qwerty');
            const expect = 'OK';
            assert.strictEqual(actual, expect);
        });
    });

    describe('test validateAll(), where inputType = birthday', () => {
        it('test must return birthday', () => {
            const actual = validate.validateAll('birthday', '');
            const expect = 'birthday';
            assert.strictEqual(actual, expect);
        });

        it('test must return OK', () => {
            const actual = validate.validateAll('birthday', '22.08.2001');
            const expect = 'OK';
            assert.strictEqual(actual, expect);
        });
    });

    describe('test validateAll(), where inputType = phone', () => {
        it('test must return phone', () => {
            const actual = validate.validateAll('phone', '123456789');
            const expect = 'phone';
            assert.strictEqual(actual, expect);
        });

        it('test must return OK', () => {
            const actual = validate.validateAll('phone', '+380660666666');
            const expect = 'OK';
            assert.strictEqual(actual, expect);
        });
    });
});

describe('test validate functions', () => {
    describe('test isEmailSend', () => {
        it('test must return true', () => {
            const actual = validate.isEmailSend('belichenko.andreyka22@gmail.com');
            assert.isTrue(actual);
        });

        it('test must return false', () => {
            const actual = validate.isEmailSend('number',);
            assert.isFalse(actual);
        });
    });

    describe('test isPasswordSend', () => {
        it('test must return true', () => {
            const actual = validate.isPasswordSend('123456789');
            assert.isTrue(actual);
        });

        it('test must return false', () => {
            const actual = validate.isPasswordSend('123',);
            assert.isFalse(actual);
        });
    });

    describe('test isPasswordConfirmSend', () => {
        it('test must return true', () => {
            const actual = validate.isPasswordConfirmSend('123456789', '123456789');
            assert.isTrue(actual);
        });

        it('test must return false', () => {
            const actual = validate.isPasswordConfirmSend('123', '5');
            assert.isFalse(actual);
        });
    });

    describe('test isCheckLengthSend', () => {
        it('test must return true', () => {
            const actual = validate.isCheckLengthSend('Andrey');
            assert.isTrue(actual);
        });

        it('test must return false', () => {
            const actual = validate.isCheckLengthSend('1');
            assert.isFalse(actual);
        });
    });

    describe('test isPhoneSend', () => {
        it('test must return true', () => {
            const actual = validate.isPhoneSend('+380666666666');
            assert.isTrue(actual);
        });

        it('test must return false', () => {
            const actual = validate.isPhoneSend('1');
            assert.isFalse(actual);
        });
    });

    describe('test isBirthdaySend', () => {
        it('test must return true', () => {
            const actual = validate.isBirthdaySend('22.08.2001');
            assert.isTrue(actual);
        });

        it('test must return false', () => {
            const actual = validate.isBirthdaySend('');
            assert.isFalse(actual);
        });
    });
});

describe('Testing isAuthorizationFieldsEmpty', () => {
    describe('test isAuthorizationFieldsEmpty(), where inputType = email', () => {
        it('test must return email', () => {
            const actual = validate.validateAll('email', '!@#$%');
            const expect = 'email';
            assert.strictEqual(actual, expect);
        });

        it('test must return OK', () => {
            const actual = validate.validateAll('email', 'belichenko.andreyka22@gmail.com');
            const expect = 'OK';
            assert.strictEqual(actual, expect);
        });
    });

    describe('test isAuthorizationFieldsEmpty(), where inputType = password', () => {
        it('test must return password', () => {
            const actual = validate.validateAll('password', 'qw');
            const expect = 'password';
            assert.strictEqual(actual, expect);
        });

        it('test must return OK', () => {
            const actual = validate.validateAll('password', 'qwerty');
            const expect = 'OK';
            assert.strictEqual(actual, expect);
        });
    });
});

describe('test validate functions', () => {
    describe('test isEmailEmpty', () => {
        it('test must return true', () => {
            const actual = validate.isEmailEmpty('belichenko.andreyka22@gmail.com');
            assert.isTrue(actual);
        });

        it('test must return false', () => {
            const actual = validate.isEmailEmpty('',);
            assert.isFalse(actual);
        });
    });

    describe('test isPasswordSend', () => {
        it('test must return true', () => {
            const actual = validate.isPasswordEmpty('123456789');
            assert.isTrue(actual);
        });

        it('test must return false', () => {
            const actual = validate.isPasswordEmpty('',);
            assert.isFalse(actual);
        });
    });
});