export const validateAll = (inputType, value, confPass) => {
    // eslint-disable-next-line default-case
    switch (inputType) {
        case 'email': return isEmailSend(value) ? 'OK' : inputType;
        case 'password': return isPasswordSend(value) ? 'OK' : inputType;
        case 'passwordAgain': return isPasswordConfirmSend(value, confPass) ? 'OK' : inputType;
        case 'firstName': return isFirstNameSend(value) ? 'OK' : inputType;
        case 'lastName': return isLastNameSend(value) ? 'OK' : inputType;
        case 'keyword': return isKeywordSend(value) ? 'OK' : inputType;
        case 'birthday': return isBirthdaySend(value) ? 'OK' : inputType;
        case 'phone': return isPhoneSend(value) ? 'OK' : inputType;
    }
};

export const isEmailSend = value => !!(value.length <= 50 && value.length > 0 && value.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/) && !value.includes('..'));
export const isFirstNameSend = value => !!(value.length <= 50 && value.length >= 1 && value.match(/[a-zA-Z]/) && value.match(/^[a-zA-Z0-9]+$/) && !value.includes('  '));
export const isLastNameSend = value => !!(value.length <= 50 && value.length >= 1 && !value.includes('  ') && value.match(/^[a-zA-Z]+$/));
export const isPasswordSend = value => !!(value.length <= 30 && value.length >= 5 && value.match(/^[!@#$%^&*a-zA-Z0-9]+$/));
export const isPasswordConfirmSend = (value, confPass) => value === confPass;
export const isCheckLengthSend = value => !!(value.length <= 30 && value.length >= 2 && value.match(/^[a-zA-Z]+$/));
export const isPhoneSend = value => !!(value.length === 13 && value.match(/^[\+][0-9]+$/));
export const isBirthdaySend = value => !!value;
export const isKeywordSend = value => !!(value.length <= 30 && value.length > 1 && value.match(/^[a-zA-Z]+$/));

export const isAuthorizationFieldsEmpty = (inputType, value) => {
    // eslint-disable-next-line default-case
    switch (inputType) {
        case 'email': return isEmailEmpty(value) ? 'OK' : inputType;
        case 'password': return isPasswordEmpty(value) ? 'OK' : inputType;
    }
};

export const isEmailEmpty = value => !!value.length;
export const isPasswordEmpty = value => !!value.length;
