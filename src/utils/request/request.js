import axios from 'axios';
import config from '../../constants/config';

export const postRequestSender = options => {
  return axios.post(`${config.pathToServer}${options.path}`, options.data, { headers: options.headers });
};

export const getRequestSender = options => {
  return axios.get(`${config.pathToServer}${options.path}`, { headers: options.headers });
};
