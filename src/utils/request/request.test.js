import axios from 'axios';

describe('axios requests', () => {
    it('POST', () => {
        const call = sinon.stub(axios, 'post');
        call();    
    });

    it('GET', () => {
        const call = sinon.stub(axios, 'get');
        call();      
    });
});