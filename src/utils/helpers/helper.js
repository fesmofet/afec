export const saveDataLS = (key, value) => {
    if (window && window.localStorage) {
        window.localStorage.setItem(key, value);
    }
};

export function mergeArraysOfObject() {
    let result = [];
    
    Array.prototype.forEach.call(arguments, function (arr) {
        if (Array.isArray(arr)) {
            for (let e in arr) {
                e = arr[e]; 
                let tmp = {}; let isAdd = true;
                result.some(function (i) {
                    if (i.id === e.id) {
                      tmp = i;
                      isAdd = false;
                      return true;
                    }
                });
                for (let prop in e) {
                    tmp[prop] = e[prop];
                }
                if (isAdd) {
                    result.push(tmp);
                }
            }
        }
    });
    return result;
}
