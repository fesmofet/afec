import * as actions from '../actions.js';

describe('testing manager actions', () => {
    it('changeTheme should return type: CHANGE_THEME, payload: "ocean"', () => {
        const expectedActionCreator = { type: 'CHANGE_THEME', payload: 'ocean' };
        const actualActionCreator = actions.changeTheme('ocean');
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('changeLocale should return type: CHANGE_LOCALE, payload: "en"', () => {
        const expectedActionCreator = { type: 'CHANGE_LOCALE', payload: 'en' };
        const actualActionCreator = actions.changeLocale('en');
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('changeLocale should return type: SET_LOCALE, payload: "en"', () => {
        const expectedActionCreator = { type: 'SET_LOCALE', payload: 'en' };
        const actualActionCreator = actions.setLocale('en');
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });
});

describe('testing registration actions', () => {
    it('setRegInputsData should return type: actionTypes.SET_REGISTRATION_INPUTS_DATA, payload: {\'keyword\': \'aaa\'}', () => {
        const expectedActionCreator = { type: 'SET_REGISTRATION_INPUTS_DATA', payload: { keyword: 'aaa' } };
        const actualActionCreator = actions.setRegInputsData({ keyword: 'aaa' });
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('setLoginExist should return type: actionTypes.SET_LOGIN_EXIST', () => {
        const expectedActionCreator = { type: 'SET_LOGIN_EXIST' };
        const actualActionCreator = actions.setLoginExist();
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('clearLoginError should return type: actionTypes.CLEAR_LOGIN_ERROR', () => {
        const expectedActionCreator = { type: 'CLEAR_LOGIN_ERROR' };
        const actualActionCreator = actions.clearLoginError();
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('completeRegistration should return type: actionTypes.SET_USER_REGISTRATION_COMPLETE', () => {
        const expectedActionCreator = { type: 'SET_USER_REGISTRATION_COMPLETE' };
        const actualActionCreator = actions.completeRegistration();
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('clearInputErrors should return type: actionTypes.CLEAR_INPUT_ERRORS', () => {
        const expectedActionCreator = { type: 'CLEAR_INPUT_ERRORS' };
        const actualActionCreator = actions.clearInputErrors();
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('registration should return type: actionTypes.REGISTRATION, payload: {\'keyword\': \'aaa\'}, history: {}', () => {
        const expectedActionCreator = { type: 'REGISTRATION', payload: { keyword: 'aaa' }, history: {} };
        const actualActionCreator = actions.registration({ keyword: 'aaa' }, {});
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });
});

describe('testing authorization actions', () => {
    it('login should return {type: actionTypes.LOGIN, payload, history}', () => {
        const expectedActionCreator = { type: 'LOGIN', payload: {}, history: {} };
        const actualActionCreator = actions.login({}, {});
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('clearAuthInputErrors should return type: actionTypes.CLEAR_ERRORS', () => {
        const expectedActionCreator = { type: 'CLEAR_ERRORS' };
        const actualActionCreator = actions.clearAuthInputErrors();
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('setAuthInputData should return type: actionTypes.SET_INPUT_ERROR_AUTH', () => {
        const expectedActionCreator = { type: 'SET_INPUT_ERROR_AUTH', payload: {} };
        const actualActionCreator = actions.setAuthInputData({});
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('authError should return type: actionTypes.AUTH_ERROR', () => {
        const expectedActionCreator = { type: 'AUTH_ERROR' };
        const actualActionCreator = actions.authError();
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('clearAuthUserData should return type: actionTypes.CLEAR_USER_DATA', () => {
        const expectedActionCreator = { type: 'CLEAR_USER_DATA' };
        const actualActionCreator = actions.clearAuthUserData();
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('setUserData should return type: actionTypes.AUTH_SET_USER_DATA', () => {
        const expectedActionCreator = { type: 'AUTH_SET_USER_DATA', payload: {} };
        const actualActionCreator = actions.setUserData({});
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });
});

describe('testing toolBar actions', () => {
    it('logout should return {type: actionTypes.LOGOUT, history}', () => {
        const expectedActionCreator = { type: 'LOGOUT', history: [] };
        const actualActionCreator = actions.logout([]);
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });

    it('toggleFullScreen should return {type: actionTypes.TOGGLE_FULL_SCREEN }', () => {
        const expectedActionCreator = { type: 'TOGGLE_FULL_SCREEN' };
        const actualActionCreator = actions.toggleFullScreen();
        assert.deepEqual(actualActionCreator, expectedActionCreator);
    });
});

describe('testing adminPage actions', () => {
    const expectedActionCreator = { type: 'CHANGE_ROLE', payload: {} };
    const actualActionCreator = actions.changeRole({});
    assert.deepEqual(actualActionCreator, expectedActionCreator);
});
