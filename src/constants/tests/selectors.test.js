import * as selectors from '../selectors.js';
import mockStore from '../../mockData/mockStore';

describe('testing selectors', () => {
    it('getTheme should return { regPage, authPage}', () => {
        const expectedSelector = mockStore.theme.currentTheme;
        const actualSelector = selectors.getTheme(mockStore);
        assert.deepEqual(actualSelector, expectedSelector);
    });

    it('getRegPageTheme should return {}', () => {
        const expectedSelector = mockStore.theme.currentTheme.regPage;
        const actualSelector = selectors.getRegPageTheme(mockStore);
        assert.deepEqual(actualSelector, expectedSelector);
    });

    it('getAuthPageTheme should return {}', () => {
        const expectedSelector = mockStore.theme.currentTheme.authPage;
        const actualSelector = selectors.getAuthPageTheme(mockStore);
        assert.deepEqual(actualSelector, expectedSelector);
    });

    it('getDictionary should return { regPage, authPage}', () => {
    const expectedSelector = mockStore.locale.dictionary;
        const actualSelector = selectors.getDictionary(mockStore);
        assert.deepEqual(actualSelector, expectedSelector);
    });

    it('getRegistrationData should return {}', () => {
        const expectedSelector = mockStore.registration;
        const actualSelector = selectors.getRegistrationData(mockStore);
        assert.deepEqual(actualSelector, expectedSelector);
    });

    it('getAuthorizationData should return {}', () => {
        const expectedSelector = mockStore.authorization;
        const actualSelector = selectors.getAuthorizationData(mockStore);
        assert.deepEqual(actualSelector, expectedSelector);
    });

    it('getAllUsers should return allUsers', () => {
        const expectedSelector = mockStore.admin.allUsers;
        const actualSelector = selectors.getAllUsers(mockStore);
        assert.deepEqual(actualSelector, expectedSelector);
    });
});
