export default [
    {
        id: 1,
        value: '9:00:00',
        innerText: '9:00 - 10:30',
    },
    {
        id: 2,
        value: '17:30',
        innerText: '17:30 - 19:00',
    },
];
