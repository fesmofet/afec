export default [
    {
        id: 'number',
        inputType: 'text',
        labelText: 'number',
        isDisabled: true,
        placeholder: 'number',
    },
    {
        id: 'lastName',
        inputType: 'text',
        labelText: 'lastName',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'firstName',
        inputType: 'text',
        labelText: 'firstName',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'email',
        inputType: 'text',
        labelText: 'email',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'phone',
        inputType: 'text',
        labelText: 'phone',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'role',
        inputType: 'text',
        labelText: 'role',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'groupForUser',
        inputType: 'text',
        labelText: 'groupForUser',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'teacherLevel',
        inputType: 'text',
        labelText: 'teacherLevel',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'salaryPerHour',
        inputType: 'text',
        labelText: 'salaryPerHour',
        isDisabled: false,
        placeholder: 'search',
    },
];
