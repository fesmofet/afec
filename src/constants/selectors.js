//Theme
export const getTheme = state => state.theme.currentTheme;
export const getRegPageTheme = state => state.theme.currentTheme.regPage;
export const getAdminPageTheme = state => state.theme.currentTheme.adminPage;
export const getAuthPageTheme = state => state.theme.currentTheme.authPage;
//Dictionary
export const getDictionary = state => state.locale.dictionary;
//ModulesData
export const getRegistrationData = state => state.registration;
export const getAuthorizationData = state => state.authorization;
// Modals
export const getModalsData = state => state.modals;
export const deleteConfirm = state => state.modals.deleteConfirm;
export const restoredPassword = state => state.authorization.restorePassword;
export const getClosePermissions = state => ({
    isCloseOnEsc: state.modals.params.isCloseOnEsc,
    isCloseOnOverlayClick: state.modals.params.isCloseOnOverlayClick,
});
export const getTeachers = state => state.admin.allTeachers;

export const getCurrentModule = state => state.config.adminMode;
export const getAdminGroupsData = state => state.admin.adminGroupsSearch;
export const getCurrentModuleTeacher = state => state.config.teacherMode;
export const getAllUsers = state => state.admin.usersSearch;
export const getGroups = state => state.admin.groups;
export const getTeachersSalary = state => state.admin.teachersDataSearch;
//TeacherPage
export const getStudents = state => state.teacher.searchStudents;
export const getTeachersGroups = state => state.teacher.teachersGroups;
export const getLessonsData = state => state.admin.lessonsFilteredData;
export const getCurrentDate = state => state.admin.currentDate;
//StudentPage
export const getStudentData = state => state.student.studentData;
export const getIsAccount = state => state.student.isAccount;
export const getAttendances = state => state.teacher.searchAttendance;
export const checkIsAccount = state => state.student.isAccount;

