import actionTypes from './actionTypes';
//Managers
export const changeTheme = payload => ({ type: actionTypes.CHANGE_THEME, payload });
export const changeLocale = payload => ({ type: actionTypes.CHANGE_LOCALE, payload });
export const setLocale = payload => ({ type: actionTypes.SET_LOCALE, payload });
/////actionType for testing default case in reducers switch
export const testDefaultCaseInReducers = () => ({ type: actionTypes.NO_ACTION });
///// actionTypes for registration
export const registration = (payload, history) => ({ type: actionTypes.REGISTRATION, payload, history });
export const login = (payload, history) => ({ type: actionTypes.LOGIN, payload, history });
export const setRegInputsData = payload => ({ type: actionTypes.SET_REGISTRATION_INPUTS_DATA, payload });
export const setLoginExist = () => ({ type: actionTypes.SET_LOGIN_EXIST });
export const clearLoginError = () => ({ type: actionTypes.CLEAR_LOGIN_ERROR });
export const completeRegistration = () => ({ type: actionTypes.SET_USER_REGISTRATION_COMPLETE });
export const clearInputErrors = () => ({ type: actionTypes.CLEAR_INPUT_ERRORS });
/////actionTypes for admin page
export const getAllUsers = payload => ({ type: actionTypes.GET_ALL_USERS, payload });
export const getAllGroups = () => ({ type: actionTypes.GET_ALL_GROUPS });
export const setAllGroups = payload => ({ type: actionTypes.SET_ALL_GROUPS, payload });
export const deleteGroupAdmin = payload => ({ type: actionTypes.DELETE_GROUP_ADMIN, payload });
export const getGroups = () => ({ type: actionTypes.GET_GROUPS });
export const getGroupsMembers = () => ({ type: actionTypes.GET_GROUPS_MEMBERS });
export const setAllUsers = payload => ({ type: actionTypes.SET_ALL_USERS, payload });
export const changeUserRole = payload => ({ type: actionTypes.GHANGE_USER_ROLE, payload });
export const setUserRole = payload => ({ type: actionTypes.SET_USER_ROLE, payload });
export const setUpdatedUsers = payload => ({ type: actionTypes.SET_UPDATED_USERS, payload });
export const setUserSearchCriteria = payload => ({ type: actionTypes.SET_USER_SEARCH_CRITERIA, payload });
export const searchUser = payload => ({ type: actionTypes.SEARCH_USER, payload });
export const setSearchedUsers = payload => ({ type: actionTypes.SET_SEARCHED_USER, payload });
export const setGroups = payload => ({ type: actionTypes.SET_GROUPS, payload });
export const deleteUser = payload => ({ type: actionTypes.DELETE_USER, payload });
export const updateUser = payload => ({ type: actionTypes.UPDATE_USER, payload });
//authorization
export const clearAuthInputErrors = () => ({ type: actionTypes.CLEAR_ERRORS });
export const setAuthInputData = payload => ({ type: actionTypes.SET_INPUT_ERROR_AUTH, payload });
export const authError = () => ({ type: actionTypes.AUTH_ERROR });
export const clearAuthUserData = () => ({ type: actionTypes.CLEAR_USER_DATA });
export const setUserData = payload => ({ type: actionTypes.AUTH_SET_USER_DATA, payload });
export const logout = history => ({ type: actionTypes.LOGOUT, history });
export const toggleFullScreen = () => ({ type: actionTypes.TOGGLE_FULL_SCREEN });
export const changeRole = payload => ({ type: actionTypes.CHANGE_ROLE, payload });
//Modals
export const toggleModalWindow = payload => ({ type: actionTypes.TOGGLE_MODAL_WINDOW, payload });
export const restorePassword = payload => ({ type: actionTypes.RESTORE_PASSWORD, payload });
export const setRestorePassword = payload => ({ type: actionTypes.SET_RESTORE_PASSWORD, payload });
export const requestAddGroup = payload => ({ type: actionTypes.MODAL_ADD_GROUP, payload });
export const requestEditGroup = payload => ({ type: actionTypes.MODAL_EDIT_GROUP, payload });
export const setDeleteConfirm = payload => ({ type: actionTypes.SET_DELETE_CONFIRM, payload });
export const addLessonAdmin = payload => ({ type: actionTypes.ADD_LESSON_ADMIN, payload });
export const addLessonTeacher = payload => ({ type: actionTypes.ADD_LESSON_TEACHER, payload });
// admin
export const changeModule = payload => ({ type: actionTypes.CHANGE_ADMIN_MODULE, payload });
export const getSchedule = payload => ({ type: actionTypes.GET_SCHEDULE, payload });
export const filterSchedule = payload => ({ type: actionTypes.GET_SCHEDULE_FILTERED_DATA, payload });
export const getTeachersSalary = payload => ({ type: actionTypes.GET_TEACHER_SALARY_DATA, payload });
export const filterTeachers = payload => ({ type: actionTypes.SET_TEACHERS_SEARCH_DATA, payload });
export const getAllTeachers = () => ({ type: actionTypes.GET_ALL_TEACHERS });
export const setAllTeachers = payload => ({ type: actionTypes.SET_ALL_TEACHERS, payload });
//teachersPages
export const changeModuleTeacher = payload => ({ type: actionTypes.CHANGE_TEACHER_MODULE, payload });
export const filterGroupsPage = payload => ({ type: actionTypes.SET_GROUPS_PAGE_SEARCH, payload });
export const getStudents = payload => ({ type: actionTypes.GET_STUDENTS, payload });
export const setStudents = payload => ({ type: actionTypes.SET_STUDENTS, payload });
export const setSearchStudents = payload => ({ type: actionTypes.SET_SEARCH_STUDENTS, payload });
export const getTeachersGroups = payload => ({ type: actionTypes.GET_TEACHERS_GROUPS, payload });
export const setTeachersGroups = payload => ({ type: actionTypes.SET_TEACHERS_GROUPS, payload });
export const updateStudent = payload => ({ type: actionTypes.UPDATE_STUDENT, payload });
export const searchStudent = payload => ({ type: actionTypes.SEARCH_STUDENTS, payload });
export const setStudentsSearchCriteria = payload => ({ type: actionTypes.SET_STUDENT_SEARCH_CRITERIA, payload });
//studentsPages
export const requestEditAccountInfo = payload => ({ type: actionTypes.UPDATE_ACCOUNT_INFO, payload });
export const getAccountTableData = payload => ({ type: actionTypes.GET_ACCOUNT_INFO, payload });
export const setAccountTableData = payload => ({ type: actionTypes.SET_ACCOUNT_INFO, payload });
export const setIsAccount = payload => ({ type: actionTypes.SET_IS_ACCOUNT, payload });
export const toggleAccountStudent = () => ({ type: actionTypes.TOGGLE_ACCOUNT_STUDENT });
export const getAttendance = payload => ({ type: actionTypes.GET_ATTENDANCE, payload });
export const setAttendances = payload => ({ type: actionTypes.SET_ATTENDANCE, payload });
export const updateAttendance = payload => ({ type: actionTypes.UPDATE_ATTENDANCE, payload });
export const searchAttendance = payload => ({ type: actionTypes.SEARCH_ATTENDANCES, payload });
export const setSearchAttendances = payload => ({ type: actionTypes.SET_SEARCHED_ATTENDANCES, payload });
export const setSearchCriteriasAttendances = payload => ({ type: actionTypes.SET_CRITERIAS_SEARCH_ATTENDANCES, payload });
export const switchAccount = () => ({ type: actionTypes.SWITCH_TO_ACCOUNT });
export const setUpdatedStudent = payload => ({ type: actionTypes.SET_UPDATED_STUDENT, payload });
