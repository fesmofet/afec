export default [
    {
        id: 'lastName',
        inputType: 'text',
        labelText: 'lastName',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'firstName',
        inputType: 'text',
        labelText: 'firstName',
        isDisabled: false,
        placeholder: 'search',
    },
];
