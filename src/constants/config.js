import inputs from './inputs';
import authInputs from './authInputs';
import recoveryInputs from '../managers/modalManager/modals/passwordRecovery/config/recoveryInputs';
import salaryInputs from './salaryInputs';
import scheduleInputs from './scheduleInputs';

export default {
    pathToServer: 'http://localhost:3000/',
    defaultTheme: 'ocean',
    inputs,
    authInputs,
    salaryInputs,
    scheduleInputs,
    recoveryInputs,
    defaultModals: {
        defaultForgotPassword: {
            isOpen: false,
        },
        defaultAddGroup: {
            isOpen: false,
        },
        defaultEditGroup: {
            isOpen: false,
            data: {},
        },
        defaultDeletingModal: {
            isOpen: false,
            deleteID: null,
        },
        defaultAddLessonAdmin: {
            isOpen: false,
        },
        defaultAddLessonTeacher: {
            isOpen: false,
        },
        defaultParams: {
            styles: {},
            isCloseOnEsc: true,
            isCloseOnOverlayClick: false,
        },
    },
    defaultAdminMode: 'users',
    defaultTeacherMode: 'studentsManager',
    userData: {
        id: null,
        firstName: null,
        lastName: null,
        phone: null,
        email: null,
        keyword: null,
        role: null,
        token: null,
        birthday: null,
        password: null,
    },
};
