export default [
    {
        id: 'id',
        inputType: 'text',
        labelText: 'number',
        isDisabled: true,
        placeholder: 'number',
    },
    {
        id: 'lastName',
        inputType: 'text',
        labelText: 'lastName',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'firstName',
        inputType: 'text',
        labelText: 'firstName',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'group',
        inputType: 'text',
        labelText: 'groups',
        isDisabled: false,
        placeholder: 'search',
    },
];
