
export default [
    {
        id: 'dateFrom',
        labelsKey: 'dateFrom',
        labelFontSize: 14,
        labelFontWeight: 'normal',
        labelBorderRadius: 10,
        inputType: 'date',
        placeholderKey: 'dateFrom',
        inputFontSize: 12,
        dataAttribute: 'data-input-birthday',
        refValue: 'dateFrom',
        errorKey: 'birthday',
    },
    {
        id: 'dateTo',
        labelsKey: 'dateTo',
        labelFontSize: 14,
        labelFontWeight: 'normal',
        labelBorderRadius: 10,
        inputType: 'date',
        placeholderKey: 'dateTo',
        inputFontSize: 12,
        dataAttribute: 'data-input-birthday',
        refValue: 'dateTo',
        errorKey: 'dateTo',
    },
];
