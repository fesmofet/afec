import { takeEvery, put } from '@redux-saga/core/effects';
import { watchChangeTheme, changeTheme } from '../saga.js';

describe('Testing watchChangeTheme: ', () => {
    let generator = watchChangeTheme();

    it('watchChangeTheme takeEvery CHANGE_THEME', () => {
        const expected = takeEvery('CHANGE_THEME', changeTheme);
        const actual = generator.next();
        assert.deepEqual(actual.value, expected);
    });

    it('watchChangeTheme done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });

     it('changeTheme put SET_THEME', () => {
        generator = changeTheme({ type: 'SET_THEME', payload: 'ocean' });
        const actual = generator.next();
        const expected = put({ type: 'SET_THEME', payload: 'ocean' });
        assert.deepEqual(actual.value, expected);
    });

    it('changeTheme done equals to true', () => {
        const actual = generator.next();
        assert.isTrue(actual.done);
    });
});
