import { themeReducer, initialState } from '../reducer';
import desert from '../theme/desertTheme';
import ocean from '../theme/oceanTheme';

describe('Testing reducer themeReducer: ', () => {
    it('Action type SET_THEME, set into desert theme', () => {
        const action = {
            type: 'SET_THEME',
            payload: 'desert',
        };

        assert.deepEqual(themeReducer(initialState, action), {
            ...initialState,
            currentTheme: desert,
            nameTheme: 'desert',
        });
    });

    it('Action type SET_THEME, set into ocean theme', () => {
        const action = {
            type: 'SET_THEME',
            payload: 'ocean',
        };

        assert.deepEqual(themeReducer(initialState, action), {
            ...initialState,
            currentTheme: ocean,
            nameTheme: 'ocean',
        });
    });
});