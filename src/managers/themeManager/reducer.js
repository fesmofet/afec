import actionTypes from '../../constants/actionTypes';
import config from '../../constants/config';
import ocean from './theme/oceanTheme';
import desert from './theme/desertTheme';

const themes = { ocean, desert };

const initialState = {
    currentTheme: themes[config.defaultTheme],
    nameTheme: config.defaultTheme,
};

export const themeReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_THEME: return {
            ...state,
            currentTheme: themes[action.payload],
            nameTheme: action.payload,
        };
        default: return state;
    }
};
