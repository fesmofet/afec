export default {
    color__01: '#fdfdfb', //main text color ocean
    color__02: '#a0d8ef', //background ocean
    color__03: '#cfebf6', //background form ocean
    color__04: '#236ea1', //background button and label ocean
    color__05: '#eff1f0', //main text color desert
    color__06: '#ec8e4e', //background desert
    color__07: '#946053', //background form desert
    color__08: '#2a0f00', //background button and label desert
    color__09: '#cfebf6', 
    color__10: '#000',    
    color__11: '#1b115c', //hover on button
    color__12: '#fff',
};
