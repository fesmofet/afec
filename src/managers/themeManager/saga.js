import { put, takeEvery } from '@redux-saga/core/effects';
import actionTypes from '../../constants/actionTypes';

export function* watchChangeTheme() {
    yield takeEvery(actionTypes.CHANGE_THEME, changeTheme);
}

export function* changeTheme(action) {
    yield put({ type: actionTypes.SET_THEME, payload: action.payload });
}