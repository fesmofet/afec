import colorMatrix from '../colorMatrix';

export default {
    regPage: {
        mainBgColor: colorMatrix.color__06,
        formBgColor: colorMatrix.color__07,
        textColor: colorMatrix.color__05,
        btnBgColor: colorMatrix.color__08,
        labelBgColor: colorMatrix.color__08,
    },
    authPage: {
        mainBgColor: colorMatrix.color__06,
        formBgColor: colorMatrix.color__07,
        textColor: colorMatrix.color__05,
        btnBgColor: colorMatrix.color__08,
        labelBgColor: colorMatrix.color__08,
    },
    adminPage: {
        mainBgColor: colorMatrix.color__06,
        formBgColor: colorMatrix.color__07,
        textColor: colorMatrix.color__05,
        btnBgColor: colorMatrix.color__08,
        labelBgColor: colorMatrix.color__08,
    },
};
