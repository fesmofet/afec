import React from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';
import {
    Content,
    CloseBtn,
    TitleText,
    ModalWrapper,
    ControlPanel,
    TitleContainer,
    ButtonContainer,
    CloseBtnContainer,
 
} from './styledComponent.js';

import CustomButton from '../../../../libs/customButton/CustomButton.jsx';

const DeletingModal = props => {
    const { style, isOpen, onRequestClose, closePermissions, dictionary, theme, setDeleteConfirm } = props;

    const handleOkClick = () => {
        setDeleteConfirm(true);
    };
    return <Modal
        style={style}
        isOpen={isOpen}
        ariaHideApp={false}
        onRequestClose={onRequestClose}
        shouldCloseOnEsc={closePermissions && closePermissions.isCloseOnEsc}
        shouldCloseOnOverlayClick={closePermissions && closePermissions.isCloseOnOverlayClick}
    >
          <ModalWrapper>
            <CloseBtnContainer>
                <CloseBtn onClick={onRequestClose}>&times;</CloseBtn>
            </CloseBtnContainer>
            <TitleContainer>
                <TitleText>{dictionary.resources.titleDelete}</TitleText>
            </TitleContainer>
            <Content>
                <TitleText>{dictionary.dialogs.deleting}</TitleText>
            </Content>
           
            <ControlPanel>
                <ButtonContainer>
                    <CustomButton
                        fontSize={16}
                        boxShadow={'none'}
                        textColor={theme.textColor}
                        fontWeight={'bold'}
                        isDisabled={false}
                        buttonName={dictionary.button.ok}
                        borderRadius={10}
                        dataAttribute={'data-button-restore'}
                        textTransform={'uppercase'}
                        backgroundColor={theme.btnBgColor}
                        onClickCallback={handleOkClick}
                        backgroundColorHover={theme.backgroundColorHover}
                    />
                    <CustomButton
                        fontSize={16}
                        boxShadow={'none'}
                        textColor={theme.textColor}
                        fontWeight={'bold'}
                        isDisabled={false}
                        buttonName={dictionary.button.cancel}
                        borderRadius={10}
                        dataAttribute={'data-button-restore'}
                        textTransform={'uppercase'}
                        backgroundColor={theme.btnBgColor}
                        onClickCallback={onRequestClose}
                        backgroundColorHover={theme.backgroundColorHover}
                    />
                </ButtonContainer>
            </ControlPanel>
        </ModalWrapper>
   
    </Modal>;
};

DeletingModal.propTypes = {
    data: PropTypes.object,
    isOpen: PropTypes.bool.isRequired,
    theme: PropTypes.object.isRequired,
    style: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,
    closePermissions: PropTypes.object.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    setDeleteConfirm: PropTypes.func.isRequired,
    restoredPassword: PropTypes.string,
};

export default React.memo(DeletingModal);
