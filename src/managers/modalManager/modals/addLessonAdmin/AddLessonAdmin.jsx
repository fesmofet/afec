import React from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';
import {
    Input,
    Content,
    CloseBtn,
    TitleText,
    LabelText,
    ModalWrapper,
    ControlPanel,
    ItemContainer,
    TitleContainer,
    ButtonContainer,
    CloseBtnContainer,
} from './styledComponent.js';
import CustomButton from '../../../../libs/customButton/CustomButton.jsx';
import CustomSelect from '../../../../libs/customSelect/CustomSelect.jsx';
import { lessonTypes, lessonsTime, selectStyle } from './config';

const dateRef = React.createRef();
const timeRef = React.createRef();
const groupRef = React.createRef();
const lessonTypeRef = React.createRef();

const AddLessonAdmin = props => {
    const { style, isOpen, onRequestClose, closePermissions, dictionary, theme, getAdminGroupsData, addLessonAdmin } = props;

    const handleOkClick = () => {
        if (!dateRef.current.value) {
            // eslint-disable-next-line no-alert
            return alert('Choose date'); //FIXME
        }
        const payload = {
            groupId: groupRef.current.value,
            lessonType: lessonTypeRef.current.value,
            timeId: timeRef.current.value,
            date: dateRef.current.value,
        };
        addLessonAdmin(payload);
    };
    return <Modal
        style={style}
        isOpen={isOpen}
        ariaHideApp={false}
        onRequestClose={onRequestClose}
        shouldCloseOnEsc={closePermissions && closePermissions.isCloseOnEsc}
        shouldCloseOnOverlayClick={closePermissions && closePermissions.isCloseOnOverlayClick}
    >
          <ModalWrapper>
            <CloseBtnContainer>
                <CloseBtn onClick={onRequestClose}>&times;</CloseBtn>
            </CloseBtnContainer>
            <TitleContainer>
                <TitleText>
                    {dictionary.resources.addLesson}
                </TitleText>
            </TitleContainer>
            <Content>
                <LabelText>
                    {dictionary.labels.group}
                </LabelText>
                <ItemContainer>
                    <CustomSelect
                        options={getAdminGroupsData.map(item => ({
                            id: item.id,
                            value: item.id,
                            innerText: item.group,
                        }))}
                        fontSize={selectStyle.fontSize}
                        textColor={selectStyle.textColor}
                        backgroundColor={selectStyle.bgColor}
                        backgroundColorHover={selectStyle.bgColor}
                        referral={groupRef}
                     />
                </ItemContainer>
                <LabelText>
                    {dictionary.labels.lessonsType}
                </LabelText>
                <ItemContainer>
                    <CustomSelect 
                        options={lessonTypes.map(item => ({
                            id: item.id,
                            value: item.value,
                            innerText: item.value,
                        }))}
                        fontSize={selectStyle.fontSize}
                        textColor={selectStyle.textColor}
                        backgroundColor={selectStyle.bgColor}
                        backgroundColorHover={selectStyle.bgColor}
                        referral={lessonTypeRef}
                    />
                </ItemContainer>
                <LabelText>
                    {dictionary.labels.time}
                </LabelText>
                <ItemContainer>
                    <CustomSelect
                        options={lessonsTime.map(item => ({
                            id: item.id,
                            value: item.id,
                            innerText: item.value,
                        }))}
                        fontSize={selectStyle.fontSize}
                        textColor={selectStyle.textColor}
                        backgroundColor={selectStyle.bgColor}
                        backgroundColorHover={selectStyle.bgColor}
                        referral={timeRef}
                     />
                </ItemContainer>
                <LabelText>
                    {dictionary.labels.date}
                </LabelText>
                <ItemContainer>
                    <Input 
                        type='date'
                        ref={dateRef}
                    />
                </ItemContainer>
            </Content>
           
            <ControlPanel>
                <ButtonContainer>
                    <CustomButton
                        fontSize={16}
                        boxShadow={'none'}
                        textColor={theme.textColor}
                        fontWeight={'bold'}
                        isDisabled={false}
                        buttonName={dictionary.button.saveGroup}
                        borderRadius={10}
                        dataAttribute={'data-button-restore'}
                        textTransform={'uppercase'}
                        backgroundColor={theme.btnBgColor}
                        onClickCallback={handleOkClick}
                        backgroundColorHover={theme.backgroundColorHover}
                    />
                </ButtonContainer>
            </ControlPanel>
        </ModalWrapper>
   
    </Modal>;
};

AddLessonAdmin.propTypes = {
    data: PropTypes.object,
    isOpen: PropTypes.bool.isRequired,
    theme: PropTypes.object.isRequired,
    style: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,
    getAdminGroupsData: PropTypes.array,
    closePermissions: PropTypes.object.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    setDeleteConfirm: PropTypes.func.isRequired,
    addLessonAdmin: PropTypes.func.isRequired,
    restoredPassword: PropTypes.string,
};

export default React.memo(AddLessonAdmin);
