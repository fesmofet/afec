import styled from 'styled-components';

export const defaultStyle = {
   formBgColor: '#76a8cb',
};

export const ModalWrapper = styled.div`
   display: flex;
   flex-direction: column;
   align-items: center;
   width: 100%;
   background-color: ${props => props.theme.formBgColor ? props.theme.formBgColor : defaultStyle.formBgColor};
   border: 5px groove #76a8cb;
   border-radius: 10px;
   box-sizing: border-box;
`;

export const Content = styled.div `
   width: 90%;
   height: 50%;
   display: flex;
   flex-direction: column;
   align-items: center;
`;

export const FieldWrapper = styled.div `
   width: 100%;
   height: 60px;
   display: flex;
   flex-direction: column;
   justify-content: center;
   margin: 5px 0px 5px;
`;

export const InputBlockContainer = styled.div `
   width: 100%;
   height: 50px;
`;

export const ErrorBlockContainer = styled.div `
   width: 100%;
   height: 22px;
   font-size: 11px;
`;

export const ControlPanel = styled.div `
   width: 100%;   
   height: 50%;
   display: flex;
   flex-direction: column;
   justify-content: center;
   align-items: center;
   padding-top: 10px;
   margin-bottom: 10px;
`;

export const ButtonContainer = styled.div `
   width: 200px;
   height: 50px;
`;

export const TitleContainer = styled.div`
   margin: 10px 0;
`;
export const CloseBtnContainer = styled.div`
   width: 100%;
   display: flex;
   justify-content: flex-end;
`;

export const TitleText = styled.span`
   font-size: 30px;
   font-weight: bold;
   color: #236ea1;
`;
export const Output = styled.output`
`;
export const CloseBtn = styled.output`
   font-family:Georgia;
   font-size: 20px; 
   font-weight:normal; 
   padding: 5px; 
   cursor: pointer;
`;
