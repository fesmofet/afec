import React from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';
import {
    Output,
    Content,
    CloseBtn,
    TitleText,
    ModalWrapper,
    ControlPanel,
    FieldWrapper,
    TitleContainer,
    ButtonContainer,
    CloseBtnContainer,
    InputBlockContainer,
    ErrorBlockContainer,
} from './styledComponent.js';
import config from '../../../../constants/config.js';
import InputBlock from '../../../../libs/InputBlock/InputBlock.jsx';
import ErrorBlock from '../../../../libs/errorBlock/ErrorBlock.jsx';
import CustomButton from '../../../../libs/customButton/CustomButton.jsx';

const PasswordRecovery = props => {
    const { style, isOpen, onRequestClose, closePermissions, dictionary, theme, data, restorePassword, restoredPassword } = props;
    const email = React.createRef();
    const keyword = React.createRef();

    const recoveryRefs = {
        email, keyword,
    };
    const handleRequest = () => {
        const payload = {
            email: recoveryRefs.email.current.value,
            keyword: recoveryRefs.keyword.current.value,
        };
        restorePassword(payload);
    };
    return <Modal
        style={style}
        isOpen={isOpen}
        ariaHideApp={false}
        onRequestClose={onRequestClose}
        shouldCloseOnEsc={closePermissions && closePermissions.isCloseOnEsc}
        shouldCloseOnOverlayClick={closePermissions && closePermissions.isCloseOnOverlayClick}
    >
          <ModalWrapper>
            <CloseBtnContainer>
                <CloseBtn onClick={onRequestClose}>&times;</CloseBtn>
            </CloseBtnContainer>
            <TitleContainer>
                <TitleText>{dictionary.button.restore}</TitleText>
            </TitleContainer>
            <Content>
                {config.recoveryInputs.map(input =>
                    <FieldWrapper key={input.id}>
                        <InputBlockContainer>
                            <InputBlock
                                id={input.id}
                                labelText={dictionary.labels[input.labelsKey]}
                                labelTextColor={theme.labelTextColor}
                                labelBgColor={theme.labelBgColor}
                                labelBorderColor={theme.labelBorderColor}
                                labelFontSize={input.labelFontSize}
                                labelfontWeight={input.labelFontWeight}
                                labelBorderRadius={input.labelBorderRadius}
                                inputType={input.inputType}
                                placeholder={dictionary.placeholders[input.placeholderKey]}
                                inputTextColor={theme.inputTextColor}
                                inputBgColor={theme.inputBgColor}
                                inputFontSize={input.inputFontSize}
                                dataAttribute={input.dataAttribute}
                                inputBlockBgColor={theme.inputBlockBgColor}
                                refValue={recoveryRefs[input.refValue]}
                            />
                        </InputBlockContainer>
                        {data.invalidFields.indexOf(input.id) !== -1 ?
                            <ErrorBlockContainer>
                                <ErrorBlock
                                    errorMessage={dictionary.errorMessageAuth[input.errorKey]}
                                    textColor={'red'}
                                />
                            </ErrorBlockContainer>
                            : ''}
                    </FieldWrapper>
                )}
                {data.authError ? 
                    <ErrorBlockContainer>
                    <ErrorBlock 
                        errorMessage={dictionary.errorMessageAuth.wrongEmail}
                        textColor={'red'}
                    />
                    </ErrorBlockContainer>
                    : null
                } 
            </Content>
            <Output>{restoredPassword}</Output>
            <ControlPanel>
                <ButtonContainer>
                    <CustomButton
                        fontSize={16}
                        boxShadow={'none'}
                        textColor={theme.textColor}
                        fontWeight={'bold'}
                        isDisabled={false}
                        buttonName={dictionary.button.restore}
                        borderRadius={10}
                        dataAttribute={'data-button-restore'}
                        textTransform={'uppercase'}
                        backgroundColor={theme.btnBgColor}
                        onClickCallback={handleRequest}
                        backgroundColorHover={theme.backgroundColorHover}
                    />
                </ButtonContainer>
            </ControlPanel>
        </ModalWrapper>
   
    </Modal>;
};

PasswordRecovery.propTypes = {
    data: PropTypes.object,
    isOpen: PropTypes.bool.isRequired,
    theme: PropTypes.object.isRequired,
    style: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    restorePassword: PropTypes.func.isRequired,
    closePermissions: PropTypes.object.isRequired,
    restoredPassword: PropTypes.string,
};

export default React.memo(PasswordRecovery);
