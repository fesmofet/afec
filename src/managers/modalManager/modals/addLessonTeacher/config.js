export const lessonTypes = [{ id: 1, value: 'Lecture' }, { id: 2, value: 'Practice' }, { id: 3, value: 'Test' }];
export const lessonsTime = [{ id: 1, value: '09:00 - 10:30' }, { id: 2, value: '17:30 - 19:00' }];
export const selectStyle = { bgColor: '#fff', fontSize: 15, textColor: '#000' };