import React from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';
import {
    Content,
    CloseBtn,
    TitleText,
    ModalWrapper,
    ControlPanel,
    FieldWrapper,
    TitleContainer,
    ButtonContainer,
    CloseBtnContainer,
    InputBlockContainer,
    ErrorBlockContainer,
    SelectWrapper,
    Span,
} from './styledComponent.js';
import addGroup from './config/addGroup.js';
import InputBlock from '../../../../libs/InputBlock/InputBlock.jsx';
import ErrorBlock from '../../../../libs/errorBlock/ErrorBlock.jsx';
import CustomButton from '../../../../libs/customButton/CustomButton.jsx';
import CustomSelect from '../../../../libs/customSelect/CustomSelect.jsx';

const groupName = React.createRef();
const groupCity = React.createRef();
const groupLevel = React.createRef();
const selectTeacher = React.createRef();

const refsAddGroup = {
    groupName, groupCity, groupLevel, selectTeacher,
};

const AddGroup = props => {
    const { style, onRequestClose, closePermissions, dictionary, data, isOpen, theme, requestAddGroup, allTeachers } = props;

    const handleRequest = () => {
        const payload = {
            groupName: refsAddGroup.groupName.current.value,
            groupCity: refsAddGroup.groupCity.current.value,
            groupLevel: refsAddGroup.groupLevel.current.value,
            selectTeacher: parseInt(refsAddGroup.selectTeacher.current.value, 10),
        };
        requestAddGroup(payload);
    };

    return <Modal
        style={style}
        isOpen={isOpen}
        ariaHideApp={false}
        onRequestClose={onRequestClose}
        shouldCloseOnEsc={closePermissions && closePermissions.isCloseOnEsc}
        shouldCloseOnOverlayClick={closePermissions && closePermissions.isCloseOnOverlayClick}>
        <ModalWrapper>
            <CloseBtnContainer>
                <CloseBtn onClick={onRequestClose}>&times;</CloseBtn>
            </CloseBtnContainer>
            <TitleContainer>
                <TitleText>{dictionary.resources.titleAddGroup}</TitleText>
            </TitleContainer>
            <Content>
                {addGroup.map(input =>
                    <FieldWrapper key={input.id}>
                        <InputBlockContainer>
                            <InputBlock
                                id={input.id}
                                labelText={dictionary.labels[input.labelsKey]}
                                labelTextColor={theme.labelTextColor}
                                labelBgColor={theme.labelBgColor}
                                labelBorderColor={theme.labelBorderColor}
                                labelFontSize={input.labelFontSize}
                                labelfontWeight={input.labelFontWeight}
                                labelBorderRadius={input.labelBorderRadius}
                                inputType={input.inputType}
                                placeholder={dictionary.placeholders[input.placeholderKey]}
                                inputTextColor={theme.inputTextColor}
                                inputBgColor={theme.inputBgColor}
                                inputFontSize={input.inputFontSize}
                                dataAttribute={input.dataAttribute}
                                inputBlockBgColor={theme.inputBlockBgColor}
                                refValue={refsAddGroup[input.refValue]}
                            />
                        </InputBlockContainer>
                        {data.invalidFields.indexOf(input.id) !== -1 ?
                            <ErrorBlockContainer>
                                <ErrorBlock
                                    errorMessage={dictionary.errorMessage[input.errorKey]}
                                    textColor={'red'}
                                />
                            </ErrorBlockContainer>
                            : null}
                    </FieldWrapper>
                )}
                <Span>{dictionary.labels.teacherLastName}</Span>
                <SelectWrapper>
                    <CustomSelect
                        options={allTeachers.map(teacher => ({
                            id: teacher.id,
                            value: teacher.id,
                            innerText: teacher.lastName,
                        }))}
                        referral={selectTeacher}
                        fontSize={15}
                        textColor='black'
                    />
                </SelectWrapper>
            </Content>
            <ControlPanel>
                <ButtonContainer>
                    <CustomButton
                        fontSize={16}
                        boxShadow={'none'}
                        textColor={theme.textColor}
                        fontWeight={'bold'}
                        isDisabled={false}
                        buttonName={dictionary.button.create}
                        borderRadius={10}
                        dataAttribute={'data-button-addGroup'}
                        textTransform={'uppercase'}
                        backgroundColor={theme.btnBgColor}
                        onClickCallback={handleRequest}
                        backgroundColorHover={theme.backgroundColorHover}
                    />
                </ButtonContainer>
            </ControlPanel>
        </ModalWrapper>
    </Modal>;
};

AddGroup.propTypes = {
    data: PropTypes.object,
    isOpen: PropTypes.bool.isRequired,
    theme: PropTypes.object.isRequired,
    style: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    closePermissions: PropTypes.object.isRequired,
    requestAddGroup: PropTypes.func.isRequired,
    allTeachers: PropTypes.array.isRequired,
};

export default React.memo(AddGroup);
