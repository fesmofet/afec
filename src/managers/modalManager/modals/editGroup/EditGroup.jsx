import React from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';
import {
    Content,
    CloseBtn,
    TitleText,
    ModalWrapper,
    ControlPanel,
    FieldWrapper,
    TitleContainer,
    ButtonContainer,
    CloseBtnContainer,
    InputBlockContainer,
    ErrorBlockContainer,
    SelectWrapper,
    Span,
} from '../addGroup/styledComponent.js';
import addGroup from '../addGroup/config/addGroup.js';
import InputBlock from '../../../../libs/InputBlock/InputBlock.jsx';
import ErrorBlock from '../../../../libs/errorBlock/ErrorBlock.jsx';
import CustomButton from '../../../../libs/customButton/CustomButton.jsx';
import CustomSelect from '../../../../libs/customSelect/CustomSelect.jsx';

const groupName = React.createRef();
const groupCity = React.createRef();
const groupLevel = React.createRef();
const selectTeacher = React.createRef();

const refsEditGroup = {
    groupName, groupCity, groupLevel, selectTeacher,
};

const EditGroup = props => {
    const { style, onRequestClose, closePermissions, dictionary, data, isOpen, theme, allTeachers, requestEditGroup, groupNewData } = props;
    const handleRequest = () => {
        const payload = {
            groupId: groupNewData.editGroup.data.groupId,
            groupName: refsEditGroup.groupName.current.value,
            groupCity: refsEditGroup.groupCity.current.value,
            groupLevel: refsEditGroup.groupLevel.current.value,
            selectTeacher: parseInt(refsEditGroup.selectTeacher.current.value, 10),
        };
        requestEditGroup(payload);
    };

    let defaultValueInputs = {
        groupName: groupNewData.editGroup.data.groupName,
        groupCity: groupNewData.editGroup.data.groupCity,
        groupLevel: groupNewData.editGroup.data.groupLevel,
    };

    return <Modal
        style={style}
        isOpen={isOpen}
        ariaHideApp={false}
        onRequestClose={onRequestClose}
        shouldCloseOnEsc={closePermissions && closePermissions.isCloseOnEsc}
        shouldCloseOnOverlayClick={closePermissions && closePermissions.isCloseOnOverlayClick}>
        <ModalWrapper>
            <CloseBtnContainer>
                <CloseBtn onClick={onRequestClose}>&times;</CloseBtn>
            </CloseBtnContainer>
            <TitleContainer>
                <TitleText>{dictionary.resources.titleEditGroup}</TitleText>
            </TitleContainer>
            <Content>
                {addGroup.map(input =>
                    <FieldWrapper key={input.id}>
                        <InputBlockContainer>
                            <InputBlock
                                id={input.id}
                                labelText={dictionary.labels[input.labelsKey]}
                                labelTextColor={theme.labelTextColor}
                                labelBgColor={theme.labelBgColor}
                                labelBorderColor={theme.labelBorderColor}
                                labelFontSize={input.labelFontSize}
                                labelfontWeight={input.labelFontWeight}
                                labelBorderRadius={input.labelBorderRadius}
                                inputType={input.inputType}
                                placeholder={dictionary.placeholders[input.placeholderKey]}
                                inputTextColor={theme.inputTextColor}
                                inputBgColor={theme.inputBgColor}
                                inputFontSize={input.inputFontSize}
                                dataAttribute={input.dataAttribute}
                                inputBlockBgColor={theme.inputBlockBgColor}
                                refValue={refsEditGroup[input.refValue]}
                                value={defaultValueInputs[input.labelsKey]}
                            />
                        </InputBlockContainer>
                        {data.invalidFields.indexOf(input.id) !== -1 ?
                            <ErrorBlockContainer>
                                <ErrorBlock
                                    errorMessage={dictionary.errorMessage[input.errorKey]}
                                    textColor={'red'}
                                />
                            </ErrorBlockContainer>
                            : null}
                    </FieldWrapper>
                )}
                <Span>{dictionary.labels.teacherLastName}</Span>
                <SelectWrapper>
                    <CustomSelect
                        options={allTeachers.map(teacher => ({
                            id: teacher.id,
                            value: teacher.id,
                            innerText: teacher.lastName,
                        }))}
                        referral={selectTeacher}
                        fontSize={15}
                        textColor='black'
                    />
                </SelectWrapper>
            </Content>
            <ControlPanel>
                <ButtonContainer>
                    <CustomButton
                        fontSize={16}
                        boxShadow={'none'}
                        textColor={theme.textColor}
                        fontWeight={'bold'}
                        isDisabled={false}
                        buttonName={dictionary.button.saveGroup}
                        borderRadius={10}
                        dataAttribute={'data-button-addGroup'}
                        textTransform={'uppercase'}
                        backgroundColor={theme.btnBgColor}
                        onClickCallback={handleRequest}
                        backgroundColorHover={theme.backgroundColorHover}
                    />
                </ButtonContainer>
            </ControlPanel>
        </ModalWrapper>
    </Modal>;
};

EditGroup.propTypes = {
    data: PropTypes.object,
    isOpen: PropTypes.bool.isRequired,
    theme: PropTypes.object.isRequired,
    style: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    closePermissions: PropTypes.object.isRequired,
    allTeachers: PropTypes.array.isRequired,
    requestEditGroup: PropTypes.func.isRequired,
    groupNewData: PropTypes.object,
};

export default React.memo(EditGroup);