import { call } from 'redux-saga/effects';
import * as rout from '../../../constants/rout';
import * as request from '../../../utils/request/request';
import { getGroupsPageData } from '../../../sagas/adminSaga/groups/worker';

export function* workerAddGroup(action) {
    const token = localStorage.getItem('token');
    if (!action.payload.groupName || !action.payload.groupCity || !action.payload.groupLevel || !action.payload.selectTeacher) {
      return yield call(alert, 'All fields are required');
    }
    yield call(request.postRequestSender, {
        path: rout.addGroup,
        data: action.payload,
        headers: { Authorization: token },
    });
    yield call(getGroupsPageData);
}