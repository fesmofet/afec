import * as workers from './workers';
import { takeEvery } from 'redux-saga/effects';
import { workerAddGroup } from './workerAddGroup';
import { workerEditGroup } from './workerEditGroup';
import { addLessonAdmin } from './workerAddLessonAdmin';
import { addLessonTeacher } from './workerAddLessonTeacher';
import actionTypes from '../../../constants/actionTypes';

export function* watchModalWindow() {
    yield takeEvery(actionTypes.TOGGLE_MODAL_WINDOW, workers.toggleModal);
    yield takeEvery(actionTypes.RESTORE_PASSWORD, workers.restorePassword);
    yield takeEvery(actionTypes.MODAL_ADD_GROUP, workerAddGroup);
    yield takeEvery(actionTypes.MODAL_EDIT_GROUP, workerEditGroup);
    yield takeEvery(actionTypes.ADD_LESSON_ADMIN, addLessonAdmin);
    yield takeEvery(actionTypes.ADD_LESSON_TEACHER, addLessonTeacher);
}