import { put, call } from 'redux-saga/effects';
import * as rout from '../../../constants/rout';
import actionTypes from '../../../constants/actionTypes';
import * as actions from '../../../constants/actions.js';
import * as request from '../../../utils/request/request';

export function* toggleModal(action) {
    yield put({ type: actionTypes.SET_TOGGLE_MODAL_WINDOW, payload: action.payload });
}

export function* restorePassword(action) {
    if (!action.payload.email || !action.payload.keyword) {
       return yield call(alert, 'All fields are required');
    }
    const password = yield call(request.postRequestSender, { path: rout.restore, data: action.payload });
    switch (password.data.message) {
        case 'RESTORE FAILED': return yield call(alert, 'Wrong keyword or email');
        case 'DATABASE ERROR': return yield call(alert, 'DATABASE ERROR');
        case 'RESTORE SUCCESS': return yield put(actions.setRestorePassword(password.data.password));
        default: return;
    }
}
