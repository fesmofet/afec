
import moment from 'moment';
import { call } from 'redux-saga/effects';
import * as rout from '../../../constants/rout';
import * as request from '../../../utils/request/request';
import { getSchedule } from '../../../sagas/adminSaga/schedule/workers';

export function* addLessonTeacher(action) {
    const token = localStorage.getItem('token');
    let start;
    let end;

    if (action.payload.timeId === '1') {
        start = new Date(`${action.payload.date} 09:00:00`);
        end = new Date(`${action.payload.date} 10:30:00`);
    } else {
        start = new Date(`${action.payload.date} 17:30:00`);
        end = new Date(`${action.payload.date} 19:00:00`);
    }
  
    const data = {
        group_id: action.payload.groupId,
        type: action.payload.lessonType,
        start: moment(start).valueOf(),
        finished: moment(end).valueOf(),
    };

    const response = yield call(request.postRequestSender, {
        path: rout.addLessonAdmin,
        data: data,
        headers: { Authorization: token },
    });
    const user = JSON.parse(localStorage.getItem('currentUserData'));
    const payload = {
        to: moment.utc().endOf('week').valueOf(),
        from: moment.utc().startOf('isoWeek').valueOf(),
        groups: user.groups,
        id: user.id,
    };
    yield call(getSchedule, { payload });
}
