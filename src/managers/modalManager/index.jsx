import { connect } from 'react-redux';
import ModalManager from './ModalManager.jsx';
import * as actions from '../../constants/actions';
import * as selectors from '../../constants/selectors';

export const mapStateToProps = state => ({
    theme: selectors.getTheme(state),
    modals: selectors.getModalsData(state),
    dictionary: selectors.getDictionary(state),
    closePermissions: selectors.getClosePermissions(state),
    authorizationData: selectors.getAuthorizationData(state),
    restoredPassword: selectors.restoredPassword(state),
    allTeachers: selectors.getTeachers(state),
    getAdminGroupsData: selectors.getAdminGroupsData(state),
    getTeachersGroups: selectors.getTeachersGroups(state),
});

export const mapDispatchToProps = dispatch => ({
    restorePassword: payload => dispatch(actions.restorePassword(payload)),
    toggleModalWindow: payload => dispatch(actions.toggleModalWindow(payload)),
    requestAddGroup: payload => dispatch(actions.requestAddGroup(payload)),
    getAllTeachers: () => dispatch(actions.getAllTeachers()),
    requestEditGroup: payload => dispatch(actions.requestEditGroup(payload)),
    setDeleteConfirm: payload => dispatch(actions.setDeleteConfirm(payload)),
    addLessonAdmin: payload => dispatch(actions.addLessonAdmin(payload)),
    addLessonTeacher: payload => dispatch(actions.addLessonTeacher(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalManager);
