import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';
import PasswordRecovery from './modals/passwordRecovery/passwordRecovery.jsx';
import AddGroup from './modals/addGroup/AddGroup.jsx';
import EditGroup from './modals/editGroup/EditGroup.jsx';
import DeletingModal from './modals/deletingModal/DeletingModal.jsx';
import AddLessonAdmin from './modals/addLessonAdmin/AddLessonAdmin.jsx';
import AddLessonTeacher from './modals/addLessonTeacher/AddLessonTeacher.jsx';

export default class ModalManager extends React.Component {

    static propTypes = {
        modals: PropTypes.object.isRequired,
        theme: PropTypes.object.isRequired,
        dictionary: PropTypes.object.isRequired,
        closePermissions: PropTypes.object.isRequired,
        toggleModalWindow: PropTypes.func.isRequired,
        restorePassword: PropTypes.func.isRequired,
        authorizationData: PropTypes.object.isRequired,
        getAdminGroupsData: PropTypes.array,
        restoredPassword: PropTypes.string,
        addGroup: PropTypes.func,
        setDeleteConfirm: PropTypes.func,
        addLessonAdmin: PropTypes.func,
        requestAddGroup: PropTypes.func,
        allTeachers: PropTypes.array.isRequired,
        getTeachersGroups: PropTypes.array.isRequired,
        requestEditGroup: PropTypes.func,
        addLessonTeacher: PropTypes.func,
    };

    handleClosePasswordRecovery = () => {
        this.props.toggleModalWindow({ modal: 'passwordRecovery', isOpen: false });
    }
    handleCloseAddGroup = () => {
        this.props.toggleModalWindow({ modal: 'addGroup', isOpen: false });
    }
    handleCloseDeletingModal = () => {
        this.props.toggleModalWindow({ modal: 'deletingModal', isOpen: false });
    }

    handleCLoseEditGroup = () => {
        this.props.toggleModalWindow({ modal: 'editGroup', isOpen: false, data: {} });
    }
    handleCLoseAddLessonAdmin = () => {
        this.props.toggleModalWindow({ modal: 'addLessonAdmin', isOpen: false });
    }
    handleCLoseAddLessonTeacher = () => {
        this.props.toggleModalWindow({ modal: 'addLessonTeacher', isOpen: false });
    }

    render() {
        return (
            <React.Fragment>
                <ThemeProvider theme={this.props.theme.modals}>
                    <PasswordRecovery
                        style={this.props.modals.params.styles}
                        isOpen={this.props.modals.passwordRecovery.isOpen}
                        dictionary={this.props.dictionary}
                        onRequestClose={this.handleClosePasswordRecovery}
                        closePermissions={this.props.closePermissions}
                        data={this.props.authorizationData}
                        theme={this.props.theme.modals}
                        restorePassword={this.props.restorePassword}
                        restoredPassword={this.props.restoredPassword}
                    />
                    <DeletingModal
                        style={this.props.modals.params.styles}
                        isOpen={this.props.modals.deletingModal.isOpen}
                        dictionary={this.props.dictionary}
                        onRequestClose={this.handleCloseDeletingModal}
                        closePermissions={this.props.closePermissions}
                        theme={this.props.theme.modals}
                        setDeleteConfirm={this.props.setDeleteConfirm}
                    />
                    <AddLessonAdmin
                        style={this.props.modals.params.styles}
                        isOpen={this.props.modals.addLessonAdmin.isOpen}
                        dictionary={this.props.dictionary}
                        onRequestClose={this.handleCLoseAddLessonAdmin}
                        closePermissions={this.props.closePermissions}
                        theme={this.props.theme.modals}
                        setDeleteConfirm={this.props.setDeleteConfirm}
                        getAdminGroupsData={this.props.getAdminGroupsData}
                        addLessonAdmin={this.props.addLessonAdmin}
                    />
                    <AddLessonTeacher
                        style={this.props.modals.params.styles}
                        isOpen={this.props.modals.addLessonTeacher.isOpen}
                        dictionary={this.props.dictionary}
                        onRequestClose={this.handleCLoseAddLessonTeacher}
                        closePermissions={this.props.closePermissions}
                        theme={this.props.theme.modals}
                        setDeleteConfirm={this.props.setDeleteConfirm}
                        getAdminGroupsData={this.props.getAdminGroupsData}
                        getTeachersGroups={this.props.getTeachersGroups}
                        addLessonTeacher={this.props.addLessonTeacher}
                    />
                    <AddGroup
                        style={this.props.modals.params.styles}
                        isOpen={this.props.modals.addGroup.isOpen}
                        dictionary={this.props.dictionary}
                        onRequestClose={this.handleCloseAddGroup}
                        closePermissions={this.props.closePermissions}
                        data={this.props.authorizationData}
                        theme={this.props.theme.modals}
                        requestAddGroup={this.props.requestAddGroup}
                        allTeachers={this.props.allTeachers}
                    />
                    <EditGroup
                        style={this.props.modals.params.styles}
                        isOpen={this.props.modals.editGroup.isOpen}
                        dictionary={this.props.dictionary}
                        onRequestClose={this.handleCLoseEditGroup}
                        closePermissions={this.props.closePermissions}
                        data={this.props.authorizationData}
                        theme={this.props.theme.modals}
                        requestEditGroup={this.props.requestEditGroup}
                        allTeachers={this.props.allTeachers}
                        groupNewData={this.props.modals}
                    />
                </ThemeProvider>
            </React.Fragment>
        );
    }
}
