import actionTypes from '../../constants/actionTypes';
import config from '../../constants/config';
import styles from './defaultStyles';

const initialState = {
    deleteConfirm: false,
    params: config.defaultModals.defaultParams,
    passwordRecovery: config.defaultModals.defaultForgotPassword,
    addGroup: config.defaultModals.defaultAddGroup,
    editGroup: config.defaultModals.defaultEditGroup,
    deletingModal: config.defaultModals.defaultDeletingModal,
    addLessonAdmin: config.defaultModals.defaultAddLessonAdmin,
    addLessonTeacher: config.defaultModals.defaultAddLessonTeacher,
};

initialState.params.styles = styles;

export const modalReducer = (state = initialState, action) => {
    const newState = { ...state };
    switch (action.type) {
        case actionTypes.SET_TOGGLE_MODAL_WINDOW:
            newState[action.payload.modal].isOpen = action.payload.isOpen;
            newState[action.payload.modal].data = action.payload.data;
            newState[action.payload.modal].deleteID = action.payload.deleteID;
            return newState;
        case actionTypes.SET_DELETE_CONFIRM:
            newState.deleteConfirm = action.payload;
            return newState;
        default:
            return state;
    }
};