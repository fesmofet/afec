export default {
    resources: {
        titleUserTable: 'Users Table',
        titleSalaryTable: 'Teachers Salary',
        titleSchedule: 'Schedule',
        title: 'Authorization',
        attendance: 'Attendance',
        titleAddGroup: 'Add group',
        titleGroups: 'Groups',
        titleDelete: 'Delete?',
        none: 'none',
        titleEditGroup: 'Edit group',
        titleStudentsManager: 'Students Manager',
        titleMyAccount: 'My Account',
        addLesson: 'Add lesson',
    },
    placeholders: {
        firstName: 'Enter your first name',
        lastName: 'Enter your last name',
        email: 'Enter your e-mail...',
        phone: '+xxxxxxxxxxxx',
        birthday: 'Enter your birthday',
        password: 'Enter your password...',
        confirmPassword: 'Confirm your password...',
        keyword: 'Enter keyword',
        search: 'Search',
        number: '№',
        groupName: 'Enter group name',
        groupCity: 'Enter group city',
        groupLevel: 'Enter group level',
        selectTeacher: 'Select teacher',
        repeatPassword: 'Enter password again',
    },
    labels: {
        city: 'City',
        group: 'Group',
        groupLevel: 'Group level',
        teacherLN: 'Teacher last name',
        teacherFN: 'Teacher first name',
        firstName: 'First name',
        lastName: 'Last name',
        email: 'Email',
        phone: 'Phone number',
        birthday: 'Date of birth',
        password: 'Password',
        confirmPassword: 'Confirm password',
        groupForUser: 'Group(for Student)',
        teacherLevel: 'Teacher Level',
        keyword: 'Keyword',
        role: 'Role',
        number: '№',
        numberOfStudents: 'Number of students',
        attendanceCM: 'Attendance in current month',
        groups: 'Groups',
        salaryPerHour: 'Salary Per Hour',
        workingHours: 'Working hours',
        salaryPerPeriod: 'Salary per period',
        dateFrom: 'Date From',
        dateTo: 'Date To',
        groupName: 'Group Name',
        groupCity: 'Group City',
        teacherLastName: 'Teacher last name',
        date: 'Date',
        teachers: 'Teachers',
        lessonsTime: 'Lessons time',
        lessonsType: 'Lessons type',
        present: 'Present',
        lessonsInMonth: 'Lessons in month',
        percent: 'Percent',
        repeatPassword: 'Repeat password',
        time: 'Time',
        attendance: 'Attendance',
        selectGroup: 'Select group:',
        selectTime: 'Select time:',
        selectDate: 'Select date:',
    },
    errorMessage: {
        firstName: 'You have entered characters that should not be in the first name, please correct it and try again.',
        lastName: 'You have entered characters that should not be in the last name, please correct it and try again.',
        email: 'The e-mail you entered is not valid, please correct it and try again.',
        phone: 'The phone number you entered is not valid, please correct it and try again.',
        birthday: 'Please select a date on the calendar',
        password: 'The password you entered is not valid, please correct it and try again.',
        confirmPassword: 'The password you repeated is not correct, please correct it and try again.',
        keyword: 'You have entered characters that should not be in the keyword, please correct it and try again.',
        emailAlreadyExist: 'This email is already in use, please choose other and try again',
    },
    errorMessageAuth: {
        email: 'This field is required',
        password: 'This field is required',
        wrongEmail: 'The e-mail or password you entered is not valid, please correct it and try again',
    },
    button: {
        registration: 'Sign up',
        authorization: 'Log in',
        calculate: 'Calculate',
        restore: 'Restore password',
        create: 'Create',
        addGroup: 'Add group',
        saveGroup: 'Save',
        ok: 'Ok',
        cancel: 'Cancel',
        addLesson: 'Add lesson',
        search: 'Search',
        prevWeek: 'prev week',
        nextWeek: 'next week',
        choose: 'Choose',
    },
    roleSelect: {
        student: 'S',
        admin: 'A',
        teacher: 'T',
    },
    navBar: {
        users: 'Users',
        groups: 'Groups',
        schedule: 'Schedule',
        salary: 'Salary',
        teachers: 'Teachers',
        statistics: 'Statistics',
        attendance: 'Attendance',
        studentsManager: 'Students Manager',
    },
    dialogs: {
        deleting: 'This action will result to data loss. Data recovery will not be possible. Proceed?',
    },
};
