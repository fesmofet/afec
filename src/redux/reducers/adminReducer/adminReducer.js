import actionTypes from '../../../constants/actionTypes';
import moment from 'moment';
const initialState = {
    allUsers: [],
    adminGroupsData: [],
    adminGroupsSearch: [],
    teachersSalaryData: [],
    teachersDataSearch: [],
    allTeachers: [],
    userSearchCriteria: {},
    groups: [],
    usersSearch: [],
    lessonsFilteredData: [],
    lessonsOriginData: [],
    currentDate: moment.utc().startOf('isoWeek').toString(),
};

export const adminReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_ALL_USERS:
            return {
                ...state,
                allUsers: action.payload,
                usersSearch: action.payload,
            };
        case actionTypes.SET_SCHEDULE_CURRENT_DATE:
            return {
                ...state,
                currentDate: action.payload,

            };
        case actionTypes.SET_ALL_GROUPS:
            return {
                ...state,
                adminGroupsData: action.payload,
                adminGroupsSearch: action.payload,
            };
        case actionTypes.SET_GROUPS_PAGE_SEARCH:
            return {
                ...state,
                adminGroupsSearch: state.adminGroupsData.filter(
                    item => item.group.includes(action.payload.group)
                    && item.teacherLN.includes(action.payload.teacherLN)
                    && item.teacherFN.includes(action.payload.teacherFN)
                    && item.city.includes(action.payload.city)
                    && item.level.includes(action.payload.level)
                ),
            };
        case actionTypes.SET_GROUPS: return { ...state, groups: action.payload };
        case actionTypes.SET_UPDATED_USERS: return { ...state, allUsers: action.payload };
        case actionTypes.SET_SEARCHED_USER: return { ...state, usersSearch: action.payload };
        case actionTypes.SET_USER_SEARCH_CRITERIA: return { ...state, userSearchCriteria: action.payload };
        case actionTypes.SET_TEACHERS_SALARY_DATA:
            return {
                ...state,
                teachersSalaryData: action.payload,
                teachersDataSearch: action.payload,
            };
        case actionTypes.SET_TEACHERS_SEARCH_DATA:
            return {
                ...state,
                teachersDataSearch: state.teachersSalaryData.filter(
                    item => item.last_name.includes(action.payload.lastName)
                            && item.first_name.includes(action.payload.firstName)),
            };
        case actionTypes.SET_SCHEDULE_DATA:
            return {
                ...state,
                lessonsFilteredData: action.payload,
                lessonsOriginData: action.payload,
            };
        case actionTypes.SET_FILTERED_SCHEDULE_DATA: // FIX ME NEED HELP
            return {
                ...state,
                lessonsFilteredData: state.lessonsOriginData.map(
                    lesson => lesson.groups.filter(
                        group => group.includes(action.payload.groups))),
            };
            case actionTypes.SET_ALL_TEACHERS:
            return {
                ...state,
                allTeachers: action.payload,
            };
        default:
            return state;
    }
};

