import actionTypes from '../../../constants/actionTypes';
import config from '../../../constants/config';

const mode = localStorage && localStorage.getItem('pageMode');
const modeTeacher = localStorage && localStorage.getItem('pageModeTeacher');
const adminMode = mode ? mode : config.defaultAdminMode;
const teacherMode = modeTeacher ? modeTeacher : config.defaultTeacherMode;

const initialState = {
    adminMode,
    teacherMode,
};

export const configReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_ADMIN_MODE:
            return {
                ...state,
                adminMode: action.payload,
            };
        case actionTypes.SET_TEACHER_MODE:
            return {
                ...state,
                teacherMode: action.payload,
            };
        default:
            return state;
    }
};

