import actionTypes from '../../../constants/actionTypes';
import en from '../../../managers/localManager/translates/en.js';

export const translates = { en };
const currentLocale = localStorage.getItem('language');

const initialState = {
    locale: currentLocale ? currentLocale : 'en',
    dictionary: translates[currentLocale ? currentLocale : 'en'],
};

export const localeReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_LOCALE: return { ...state, locale: action.payload, dictionary: translates[action.payload] };
        default:
            return state;
    }
};
