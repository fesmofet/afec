import { localeReducer, translates } from './localeReducer.js';
import * as actions from '../../../constants/actions.js';

describe('testing localeReducer', () => {
    it('testing localeReducer with action {type: actionTypes.SET_LOCALE}', () => {
        const expectedResult = {
            locale: 'en',
            dictionary: translates.en,
        };
        const result = localeReducer(null, actions.setLocale('en'));
        assert.deepEqual(result, expectedResult);
    });
});
