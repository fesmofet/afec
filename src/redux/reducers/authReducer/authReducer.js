import actionTypes from '../../../constants/actionTypes';
import config from '../../../constants/config';

const currentUserData = localStorage && JSON.parse(localStorage.getItem('currentUserData'));
const userData = currentUserData ? currentUserData : config.userData;

const initialState = {
    invalidFields: [],
    userData,
    authError: false,
    restorePassword: null,
};

export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_INPUT_ERROR_AUTH:
            return {
                ...state,
                invalidFields: action.payload.invalidFields,
            };
        case actionTypes.CLEAR_ERRORS: return { ...state, invalidFields: [], authError: false };
        case actionTypes.AUTH_ERROR: return { ...state, authError: true };
        case actionTypes.AUTH_SET_USER_DATA: return { ...state,
            userData: action.payload,
            authError: false,
            invalidFields: [],
        };
        case actionTypes.CLEAR_USER_DATA: return initialState;
        case actionTypes.SET_UPDATED_STUDENT: return { ...state, userData: action.payload };
        case actionTypes.SET_RESTORE_PASSWORD: return { ...state, restorePassword: action.payload };
        case actionTypes.SET_UPDATED_STUDENT: return { ...state, userData: action.payload };
        default:
            return state;
    }
};

