import { authReducer } from './authReducer.js';
import * as actions from '../../../constants/actions.js';

describe('testing authReducer', () => {
    it('testing authReducer with action {type: actionTypes.SET_INPUT_ERROR_AUTH}', () => {
        const expectedResult = {
            invalidFields: ['email', 'password'],
        };
        let invalidFields = ['email', 'password'];
        const result = authReducer(null, actions.setAuthInputData({ invalidFields }));
        assert.deepEqual(result, expectedResult);
    });

    it('testing authReducer with action {type: CLEAR_ERRORS}', () => {
        const expectedResult = {
            invalidFields: [],
            authError: false,
        };
        const result = authReducer(null, actions.clearAuthInputErrors());
        assert.deepEqual(result, expectedResult);
    });

    it('testing authReducer with action {type: AUTH_ERROR}', () => {
        const expectedResult = {
            authError: true,
        };
        const result = authReducer(null, actions.authError());
        assert.deepEqual(result, expectedResult);
    });

    it('testing authReducer with action {type: AUTH_SET_USER_DATA}', () => {
        const expectedResult = {
            invalidFields: [],
            userData: {
                id: 1,
                firstName: 'Andrey',
            },
            authError: false,
        };
        const result = authReducer(null, actions.setUserData({ id: 1, firstName: 'Andrey' }));
        assert.deepEqual(result, expectedResult);
    });

    it('testing authReducer with action {type: CLEAR_USER_DATA}', () => {
        const expectedResult = {
            invalidFields: [],
            userData: {
                id: null,
                firstName: null,
                lastName: null,
                phone: null,
                email: null,
                keyword: null,
                role: null,
                token: null,
                birthday: null,
                password: null,
            },
            authError: false,
        };
        const result = authReducer(null, actions.clearAuthUserData());
        assert.deepEqual(result, expectedResult);
    });
});
