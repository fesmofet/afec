import actionTypes from '../../../constants/actionTypes.js';

const initialState = {
    studentData: {},
    isAccount: false,
};

export const studentReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_ACCOUNT_INFO: return { ...state, studentData: action.payload };
        case actionTypes.SET_IS_ACCOUNT: return { ...state, isAccount: action.payload };
        case actionTypes.SWITCH_TO_ACCOUNT:
            return {
                ...state,
                isAccount: !state.isAccount,
            };
        default:
            return state;
    }
};
