import { regReducer } from './regReducer';
import * as actions from '../../../constants/actions.js';

describe('Testing regReducer', () => {
    const inputs = {
        email: '',
        phone: '',
        keyword: '',
        firstNme: '',
        lastName: '',
        password: '',
        birthday: '',
        passwordAgain: '',
    };
    const initialState = {
        inputs: inputs,
        isLoginExist: false,
        invalidFields: [],
    };

    it('testing regReducer with action {type: actionTypes.SET_REGISTRATION_INPUTS_DATA}', () => {
        const expectedResult = {
            isLoginExist: false,
            inputs: {
                email: 'aaa@aaa.com',
                phone: '',
                keyword: '',
                firstNme: '',
                lastName: '',
                password: '',
                birthday: '',
                passwordAgain: '',
            },
            invalidFields: [],
        };
        const result = regReducer(initialState, actions.setRegInputsData({
            inputs: {
                email: 'aaa@aaa.com',
                phone: '',
                keyword: '',
                firstNme: '',
                lastName: '',
                password: '',
                birthday: '',
                passwordAgain: '',
            },
            invalidFields: [],
        }));
        assert.deepEqual(result, expectedResult);
    });

    it('testing regReducer with action {type: actionTypes.SET_LOGIN_EXIST}', () => {
        const expectedResult = {
            isLoginExist: true,
            inputs: inputs,
            invalidFields: [],
        };
        const result = regReducer(initialState, actions.setLoginExist());
        assert.deepEqual(result, expectedResult);
    });

    it('testing regReducer with action {type: actionTypes.CLEAR_LOGIN_ERROR}', () => {
        const expectedResult = {
            isLoginExist: false,
            inputs: inputs,
            invalidFields: [],
        };
        const result = regReducer(initialState, actions.clearLoginError());
        assert.deepEqual(result, expectedResult);
    });

    it('testing regReducer with action {type: actionTypes.SET_USER_REGISTRATION_COMPLETE}', () => {
        const expectedResult = {
            isLoginExist: false,
            inputs: inputs,
            invalidFields: [],
        };
        const result = regReducer(initialState, actions.completeRegistration());
        assert.deepEqual(result, expectedResult);
    });

    it('testing regReducer with unknown actionType', () => {
        const expectedResult = {
            isLoginExist: false,
            inputs: inputs,
            invalidFields: [],
        };
        const result = regReducer(initialState, actions.testDefaultCaseInReducers());
        assert.deepEqual(result, expectedResult);
    });
});
