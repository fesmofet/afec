import actionTypes from '../../../constants/actionTypes';

const initialState = {
    inputs: {
        email: '',
        phone: '',
        keyword: '',
        firstName: '',
        lastName: '',
        password: '',
        birthday: '',
        passwordAgain: '',
    },
    isLoginExist: false,
    invalidFields: [],
};

export const regReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_REGISTRATION_INPUTS_DATA:
            return {
                ...state,
                inputs: action.payload.inputs,
                invalidFields: action.payload.invalidFields,
            };
        case actionTypes.SET_LOGIN_EXIST:
            return {
                ...state,
                isLoginExist: true,
            };
        case actionTypes.CLEAR_INPUT_ERRORS:
            return {
                ...state,
                invalidFields: [],
                isLoginExist: false,
            };
        case actionTypes.CLEAR_LOGIN_ERROR:
            return {
                ...state,
                isLoginExist: false,
            };
        case actionTypes.SET_USER_REGISTRATION_COMPLETE:
            return {
                ...state,
                inputs: {
                    email: '',
                    phone: '',
                    keyword: '',
                    firstNme: '',
                    lastName: '',
                    password: '',
                    birthday: '',
                    passwordAgain: '',
                },
                isLoginExist: false,
                invalidFields: [],
            };
        default:
            return state;
    }
};

