import actionTypes from '../../../constants/actionTypes.js';

const initialState = {
    students: [],
    searchStudents: [],
    teachersGroups: [],
    studentSearchCriteria: [],
    attendance: [],
    searchAttendance: [],
    attendanceSearchCriteria: [],
};

export const teacherReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_STUDENTS:
            return { ...state, students: action.payload };
        case actionTypes.SET_SEARCH_STUDENTS:
            return { ...state, searchStudents: action.payload };
        case actionTypes.SET_TEACHERS_GROUPS:
            return { ...state, teachersGroups: action.payload };
        case actionTypes.SET_STUDENT_SEARCH_CRITERIA:
            return { ...state, studentSearchCriteria: action.payload };
        case actionTypes.SET_ATTENDANCE:
            return { ...state, attendance: action.payload, searchAttendance: action.payload };
        case actionTypes.SET_SEARCHED_ATTENDANCES:
            return { ...state, searchAttendance: action.payload };
        case actionTypes.SET_CRITERIAS_SEARCH_ATTENDANCES:
            return { ...state, attendanceSearchCriteria: action.payload };
        default:
            return state;
    }
};
