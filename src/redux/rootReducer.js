import { combineReducers } from 'redux';
import { regReducer } from './reducers/regReducer/regReducer';
import { themeReducer } from '../managers/themeManager/reducer';
import { modalReducer } from '../managers/modalManager/reducer';
import { authReducer } from './reducers/authReducer/authReducer';
import { adminReducer } from './reducers/adminReducer/adminReducer';
import { localeReducer } from './reducers/localReducer/localeReducer';
import { configReducer } from './reducers/configReducer/configReducer';
import { teacherReducer } from './reducers/teacherReducer/teacherReducer.js';
import { studentReducer } from './reducers/studentReducer/studentReducer';

export const rootReducer = combineReducers({
    theme: themeReducer,
    student: studentReducer,
    admin: adminReducer,
    modals: modalReducer,
    locale: localeReducer,
    config: configReducer,
    registration: regReducer,
    authorization: authReducer,
    teacher: teacherReducer,
});
