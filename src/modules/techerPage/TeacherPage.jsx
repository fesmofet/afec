import React from 'react';
import {
    TeacherPageWrapper,
    TeacherPageContainer,
} from './styledComponent.js';
import { ThemeProvider } from 'styled-components';
import Attendance from './components/attendance/Attendance.jsx';
import Schedule from './components/schedule/Schedule.jsx';
import StudentsManager from './components/studentsManager/StudentsManager.jsx';
import ToolBar from '../../libs/toolBar/ToolBar.jsx';
import NavBar from '../../libs/navBar/navBar.jsx';
import navBarItems from '../../constants/navBarItemsTeacher.js';
import PropTypes from 'prop-types';
import moment from 'moment';

export default class TeacherPage extends React.Component {
    constructor(props) {
        super(props);
        let teacherId = props.authData.userData.id;
        let time = moment(`${moment().format('YYYY-MM-DD')} 09:00`, 'YYYY-MM-DD HH:mm').format();
        let data = {
            teacherId,
            groupId: 'all',
            timeStamp: parseInt(moment(time).format('X')),
        };
        props.getAttendance(data);
        props.getStudents(teacherId);
        props.getTeachersGroups(teacherId);

        const user = JSON.parse(localStorage.getItem('currentUserData'));
        const payload = {
            to: moment.utc().endOf('isoWeek').valueOf(),
            from: moment.utc().startOf('isoWeek').valueOf(),
            groups: user.groups,
            id: user.id,
        };
        props.getSchedule(payload);
    }

    renderModeSwitch(props) {
        switch (props.currentModule) {
            case 'schedule':
                return <Schedule
                    theme={props.theme}
                    filterLessons={props.filterLessons}
                    lessons={props.lessons}
                    dictionary={props.dictionary}
                    getSchedule={props.getSchedule}
                    toggleModalWindow={props.toggleModalWindow}
                    currentDate={props.currentDate}/>;
            case 'attendance':
                return <Attendance
                    dictionary={props.dictionary}
                    groups={props.groups}
                    teacherId={props.authData.userData.id}
                    getAttendance={props.getAttendance}
                    attendances={props.attendances}
                    updateAttendance={props.updateAttendance}
                    searchAttendance={props.searchAttendance}
                />;
            case 'studentsManager':
                return <StudentsManager
                    students={props.students}
                    dictionary={props.dictionary}
                    groups={props.groups}
                    updateStudent={props.updateStudent}
                    searchStudent={props.searchStudent}
                />;
            default:
                return <Schedule
                    theme={props.theme}
                    filterLessons={props.filterLessons}
                    lessons={props.lessons}
                    dictionary={dictionary}
                    getSchedule={props.getSchedule}
                    currentDate={props.currentDate}/>;
        }
    }
    render() {
        const { dictionary, theme, history, logout, switchAccount, toggleFullScreen, changeModule } = this.props;
        return (
            <ThemeProvider theme={theme}>
                <TeacherPageWrapper>
                    <TeacherPageContainer>
                        <ToolBar
                            history={history}
                            logout={logout}
                            fullScreen={toggleFullScreen}
                            switchAccount={switchAccount}
                        />
                        <NavBar
                            dictionary={dictionary}
                            changeModule={changeModule}
                            navBarItems={navBarItems}
                        />
                        {this.renderModeSwitch(this.props)}
                    </TeacherPageContainer>
                </TeacherPageWrapper>
            </ThemeProvider>
        );
    }
}

TeacherPage.propTypes = {
    history: PropTypes.object.isRequired,
    students: PropTypes.array.isRequired,
    attendances: PropTypes.array.isRequired,
    groups: PropTypes.array,
    dictionary: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    authData: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired,
    toggleFullScreen: PropTypes.func.isRequired,
    changeModule: PropTypes.func.isRequired,
    getStudents: PropTypes.func.isRequired,
    getTeachersGroups: PropTypes.func.isRequired,
    updateStudent: PropTypes.func.isRequired,
    searchStudent: PropTypes.func.isRequired,
    getAttendance: PropTypes.func.isRequired,
    updateAttendance: PropTypes.func.isRequired,
    searchAttendance: PropTypes.func.isRequired,
    switchAccount: PropTypes.func,
    getSchedule: PropTypes.func.isRequired,
};
