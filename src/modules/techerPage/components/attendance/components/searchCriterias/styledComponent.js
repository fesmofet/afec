import styled from 'styled-components';

export const AttendanceTableSearch = styled.div`
  width: 100%;
  height: 100%;
  display:flex;
  justify-content: space-around;
  align-items: center;
  margin-bottom: 10px;
`;

export const AttendanceSearchItem = styled.div`
  display:flex;
  justify-content: center;
  align-items: center;
  width: 18%;
  height: 100%;
`;

export const SearchContentContainer = styled.div`
  margin-right: 10px;
    width: 100%;
    min-width: 110px;
    height: 100%;
`;

export const SearchSelectData = styled.input`
height: 90%;
`;

export const SearchText = styled.p`
    text-align: right;
`;

export const SearchActionContainer = styled.div`
  margin-right: 10px;
    width: 100%;
    min-width: 110px;
    height: 30px;
`;

export const SearchActionContainerButton = styled.div`
  margin-right: 10px;
    width: 60%;
    min-width: 110px;
    height: 30px;
`;
