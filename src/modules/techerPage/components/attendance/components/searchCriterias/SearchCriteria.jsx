import React from 'react';
import {
    AttendanceTableSearch,
    AttendanceSearchItem,
    SearchSelectData,
    SearchText,
    SearchContentContainer,
    SearchActionContainer,
    SearchActionContainerButton,
} from './styledComponent.js';
import PropTypes from 'prop-types';
import CustomSelect from "../../../../../../libs/customSelect/CustomSelect.jsx";
import CustomButton from "../../../../../../libs/customButton/CustomButton.jsx";
import selectTimeItems from "../../../../../../constants/selectTimeItems.js";
import moment from 'moment';

const SearchCriteria = (props) => {
    const {groups, teacherId, getAttendance, dictionary} = props;
   
    groups[0] && groups[0].groupId !== 'all' ? groups.unshift({ groupId: 'all', groupName: 'All' }) : null;
    let refGroups = React.createRef();
    let refDate = React.createRef();
    let refTime = React.createRef();

    const handleGetAttendance = () => {
        let time = moment(`${refDate.current.value || moment().format('YYYY-MM-DD')} ${refTime.current.value || '09:30'}`, 'YYYY-MM-DD HH:mm').format();
        let data = {
            teacherId,
            groupId: refGroups.current.value,
            timeStamp: parseInt(moment(time).format("X")) * 1000,
        };
        getAttendance(data);
    };

    return (
        <AttendanceTableSearch>
            <AttendanceSearchItem>
                <SearchContentContainer>
                    <SearchText children={dictionary.labels.selectGroup}/>
                </SearchContentContainer>
                <SearchActionContainer>
                    <CustomSelect
                        options={
                            groups.map((group, index) => ({
                                id: group.groupId,
                                value: group.groupId,
                                innerText: group.groupName,
                            }))}
                        defaultValue={'All'}
                        referral={refGroups}
                    />
                </SearchActionContainer>
            </AttendanceSearchItem>
            <AttendanceSearchItem>
                <SearchContentContainer>
                    <SearchText children={dictionary.labels.selectDate}/>
                </SearchContentContainer>
                <SearchActionContainer>
                    <SearchSelectData
                        type='date'
                        defaultValue={moment().format('YYYY-MM-DD')}
                        ref={refDate}
                    />
                </SearchActionContainer>
            </AttendanceSearchItem>
            <AttendanceSearchItem>
                <SearchContentContainer>
                    <SearchText children={dictionary.labels.selectTime}/>
                </SearchContentContainer>
                <SearchActionContainer>
                    <CustomSelect
                        options={
                            selectTimeItems.map(time => ({
                                id: time.id,
                                value: time.value,
                                innerText: time.innerText,
                            }))}
                        defaultValue={selectTimeItems[0].value}
                        referral={refTime}
                    />
                </SearchActionContainer>
            </AttendanceSearchItem>
            <AttendanceSearchItem>
                <SearchActionContainerButton>
                    <CustomButton
                        buttonName={dictionary.button.choose}
                        onClickCallback={handleGetAttendance}
                    />
                </SearchActionContainerButton>
            </AttendanceSearchItem>
        </AttendanceTableSearch>
    );
};

SearchCriteria.propTypes = {
    groups: PropTypes.array.isRequired,
    teacherId: PropTypes.number.isRequired,
    getAttendance: PropTypes.func.isRequired,
    dictionary: PropTypes.object.isRequired,
};

export default React.memo(SearchCriteria);
