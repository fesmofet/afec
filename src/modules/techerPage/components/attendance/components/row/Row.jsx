import React from 'react';
import PropTypes from 'prop-types';
import {
    BodyRow,
    RowItem,
    ItemText,
    ItemCheckBox,
} from './styledComponent.js';

const Row = (props) => {
    const {studentId, lastName, firstName, groupName, indexOfRow, groupLevel, lessonId, attendance, updateAttendance} = props;
    const array = [indexOfRow, lastName, firstName, groupName, groupLevel];
    const checkAttendance = React.createRef();

    const handleUpdateAttendance = () => {
        let dataSend = {studentId, lessonId, attendance: checkAttendance.current.checked};
        updateAttendance(dataSend);
    };

    const showText = (item, index) => <RowItem key={index}>
        <ItemText>{item}</ItemText>
    </RowItem>;

    return (
        <BodyRow>
            {array.map((item, index) => showText(item, index))}
            <RowItem>
                <ItemCheckBox
                    type='checkbox'
                    defaultChecked={attendance}
                    onChange={handleUpdateAttendance}
                    ref={checkAttendance}
                />
            </RowItem>
        </BodyRow>
    );
};

Row.propTypes = {
    groupName: PropTypes.string.isRequired,
    groupLevel: PropTypes.string.isRequired,
    groupId: PropTypes.number.isRequired,
    lessonId: PropTypes.number.isRequired,
    studentId: PropTypes.number.isRequired,
    attendance: PropTypes.bool.isRequired,
    lastName: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    indexOfRow: PropTypes.number.isRequired,
    updateAttendance: PropTypes.func.isRequired,
};

export default React.memo(Row);
