import React from 'react';
import PropTypes from 'prop-types';
import {
    AttendanceTableBody
} from './styledComponent.js';
import Row from '../row/Row.jsx';
import {Scrollbars} from 'react-custom-scrollbars';

const Body = (props) => {
    const {attendances, updateAttendance} = props;

    const handleBody = (item, index) => {
        return <Row key={index}
                    groupName={item.groupName}
                    groupLevel={item.groupLevel}
                    groupId={item.groupId}
                    lessonId={item.lessonId}
                    studentId={item.studentId}
                    attendance={item.attendance}
                    lastName={item.lastName}
                    firstName={item.firstName}
                    indexOfRow={index + 1}
                    updateAttendance={updateAttendance}
        />;
    };

    return (
        <AttendanceTableBody>
            <Scrollbars style={{height: '45vh', minHeight: '350px'}}>
                {attendances.map((item, index) => handleBody(item, index))}
            </Scrollbars>
        </AttendanceTableBody>
    );
};

Body.propTypes = {
    attendances: PropTypes.array.isRequired,
    updateAttendance: PropTypes.func.isRequired,
    dictionary: PropTypes.object.isRequired,
};

export default React.memo(Body);

