import React from 'react';
import PropTypes from 'prop-types';
import {
    AttendanceTableHeader,
    HeaderItem,
    HeaderItemText,
} from './styledComponent.js';
import InputBlock from '../../../../../../libs/InputBlock/InputBlock.jsx';
import attendanceHeader from "../../../../../../constants/attendanceHeader.js";

const Header = (props) => {
    const { dictionary, searchAttendance} = props;
    const lastName = React.createRef();
    const firstName = React.createRef();
    const attendanceTableHeaderRefs = { lastName, firstName };

    const handleSearch = () => {
        let data = {
            lastName: attendanceTableHeaderRefs.lastName.current.value,
            firstName: attendanceTableHeaderRefs.firstName.current.value,
        };
        searchAttendance(data);
    };

    return (
        <AttendanceTableHeader>
            <HeaderItem>
                <HeaderItemText>{dictionary.labels.number}</HeaderItemText>
            </HeaderItem>
            {attendanceHeader && attendanceHeader.length ? attendanceHeader.map((item, index) =>
                <HeaderItem key={index}>
                    <InputBlock
                        id={item.id}
                        refValue={attendanceTableHeaderRefs[item.labelText]}
                        inputType={item.inputType}
                        labelText={dictionary.labels[item.labelText]}
                        isDisabled={item.isDisabled}
                        placeholder={dictionary.placeholders[item.placeholder]}
                        onChangeCallback={handleSearch}
                    />
                </HeaderItem>
            ) : null}
            <HeaderItem>
                <HeaderItemText>{dictionary.labels.group}</HeaderItemText>
            </HeaderItem>
            <HeaderItem>
                <HeaderItemText>{dictionary.labels.groupLevel}</HeaderItemText>
            </HeaderItem>
            <HeaderItem>
                <HeaderItemText>{dictionary.labels.attendance}</HeaderItemText>
            </HeaderItem>
        </AttendanceTableHeader>
    );
};

Header.propTypes = {
    dictionary: PropTypes.object.isRequired,
    searchAttendance: PropTypes.func.isRequired,
};

export default React.memo(Header);
