import styled from 'styled-components';

export const AttendanceTableWrapper = styled.div`
  width: 80%;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: flex-start;
`;

export const AttendanceTableTitle = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 35px 0 20px;
`;

export const AttendanceTableText = styled.span`
  font-size: 25px;
  font-weight: bold;
`;

export const Table = styled.div`
  box-sizing: border-box;
  width: 100%;
`;
