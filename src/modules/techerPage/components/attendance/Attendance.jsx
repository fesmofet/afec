import React from 'react';
import {
    AttendanceTableWrapper,
    AttendanceTableTitle,
    AttendanceTableText,
    Table,
} from './styledComponent.js';
import PropTypes from 'prop-types';
import Header from './components/Header/Header.jsx';
import Body from './components/body/Body.jsx';
import SearchCriteria from './components/searchCriterias/SearchCriteria.jsx';

const Attendance = (props) => {
    const {dictionary, groups, teacherId, getAttendance, attendances, updateAttendance, searchAttendance} = props;

    return (
        <AttendanceTableWrapper>
            <AttendanceTableTitle>
                <AttendanceTableText>{dictionary.resources.attendance}</AttendanceTableText>
            </AttendanceTableTitle>
            <SearchCriteria
                getAttendance={getAttendance}
                dictionary={dictionary}
                teacherId={teacherId}
                groups={groups}
            />
            <Table>
                <Header
                    dictionary={dictionary}
                    searchAttendance={searchAttendance}
                />
                <Body
                    dictionary={dictionary}
                    attendances={attendances}
                    updateAttendance={updateAttendance}
                />
            </Table>
        </AttendanceTableWrapper>
    );
};

Attendance.propTypes = {
    dictionary: PropTypes.object.isRequired,
    groups: PropTypes.array.isRequired,
    attendances: PropTypes.array.isRequired,
    teacherId: PropTypes.number.isRequired,
    getAttendance: PropTypes.func.isRequired,
    updateAttendance: PropTypes.func.isRequired,
    searchAttendance: PropTypes.func.isRequired,
};

export default React.memo(Attendance);
