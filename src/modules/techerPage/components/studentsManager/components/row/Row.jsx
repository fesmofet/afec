import React from 'react';
import {useState} from 'react';
import PropTypes from 'prop-types';
import {
    BodyRow,
    RowItem,
    ItemText,
    ItemInput,
    BtnPencil,
    BtnCancel,
    BtnCheckMark,
} from './styledComponent.js';
import CustomSelect from '../../../../../../libs/customSelect/CustomSelect.jsx';

const Row = (props) => {
    const {groupId, studentId, lastName, firstName, groupName, dictionary, indexOfRow, groups, updateStudent} = props;
    const array = [indexOfRow, lastName, firstName, groupName];
    const [isText, setIsText] = useState(true);
    const refLastName = React.createRef();
    const refFirstName = React.createRef();
    const refGroups = React.createRef();

    const handleUpdateUser = () => {
        let studentNewData = {
            studentId: studentId,
            firstName: refFirstName.current.value,
            lastName: refLastName.current.value,
            groupId: parseInt(refGroups.current.value),
        };
        updateStudent(studentNewData);
        setIsText(!isText);
    };

    const changeContent = () => setIsText(!isText);

    const showText = (item, index) => <RowItem key={index}>
        <ItemText>{item ? item : dictionary.resources.none}</ItemText>
    </RowItem>;

    const showInputs = () => <>
        <RowItem>
            <ItemText>{indexOfRow}</ItemText>
        </RowItem>
        <RowItem>
            <ItemInput defaultValue={lastName} ref={refLastName}/>
        </RowItem>
        <RowItem>
            <ItemInput defaultValue={firstName} ref={refFirstName}/>
        </RowItem>
        <RowItem>
            <CustomSelect
                options={groups.filter(item => item.groupId !== 'all').map(group => ({
                    id: group.groupId,
                    value: group.groupId,
                    innerText: group.groupName,
                }))}
                defaultValue={groupId}
                referral={refGroups}
            />
        </RowItem>
    </>;

    return (
        <BodyRow>
            {isText ? array.map((item, index) => showText(item, index)) : showInputs()}
            <RowItem>
                {isText ? <BtnPencil onClick={changeContent}/>
                    :
                    <>
                        <BtnCheckMark onClick={handleUpdateUser}/>
                        <BtnCancel onClick={changeContent}/>
                    </>
                }
            </RowItem>
        </BodyRow>
    );
};

Row.propTypes = {
    groupId: PropTypes.number.isRequired,
    indexOfRow: PropTypes.number.isRequired,
    studentId: PropTypes.number.isRequired,
    groupName: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    dictionary: PropTypes.object.isRequired,
    groups: PropTypes.array.isRequired,
    updateStudent: PropTypes.func.isRequired,
};

export default React.memo(Row);
