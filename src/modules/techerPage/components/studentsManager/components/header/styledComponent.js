import styled from 'styled-components';

let defaultStyles = {
    bgTableItem: '#236ea1',
};

export const UserTableHeader = styled.div`
  width: 100%;
  display:flex;
  justify-content: space-between;
  align-items: center;
`;

export const HeaderItem = styled.div`
  height: 100%;
  box-sizing: border-box;
  border: 1px solid black;
  min-height: 60px;
  background: ${props => props.theme.btnBgColor ? props.theme.btnBgColor : defaultStyles.bgTableItem};
  
  &:nth-child(1){
    width: 10%;
  }
  
  &:nth-child(2){
    width: calc(80%/3);
  }
  
  &:nth-child(3){
    width: calc(80%/3);
  }
  
  &:nth-child(4){
    width: calc(80%/3);
  }
  
  &:nth-child(5){
    width: 10%;
  }
`;

