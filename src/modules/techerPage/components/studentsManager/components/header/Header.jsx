import React from 'react';
import PropTypes from 'prop-types';
import {
    UserTableHeader,
    HeaderItem,
} from './styledComponent.js';
import InputBlock from '../../../../../../libs/InputBlock/InputBlock.jsx';
import headerStudentsTable from '../../../../../../constants/headerStudentsTable';

const Header = (props) => {
    const { dictionary, searchStudent } = props;

    const lastName = React.createRef();
    const firstName = React.createRef();
    const groups = React.createRef();

    const studentsTableHeaderRefs = { lastName, firstName, groups };

    const handleSearch = () => {
        let data = {
            lastName: studentsTableHeaderRefs.lastName.current.value,
            firstName: studentsTableHeaderRefs.firstName.current.value,
            group: studentsTableHeaderRefs.groups.current.value,
        };
        searchStudent(data);
    };

    return (
        <UserTableHeader>
            {headerStudentsTable && headerStudentsTable.length ? headerStudentsTable.map((item) =>
                <HeaderItem key={item.id}>
                    <InputBlock
                        id={item.id}
                        refValue={studentsTableHeaderRefs[item.labelText]}
                        inputType={item.inputType}
                        labelText={dictionary.labels[item.labelText]}
                        isDisabled={item.isDisabled}
                        placeholder={dictionary.placeholders[item.placeholder]}
                        onChangeCallback={handleSearch}
                    />
                </HeaderItem>
            ) : null}
            <HeaderItem></HeaderItem>
        </UserTableHeader>
    );
};

Header.propTypes = {
    dictionary: PropTypes.object.isRequired,
    searchStudent: PropTypes.func.isRequired,
};

export default React.memo(Header);
