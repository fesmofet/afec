import React from 'react';
import PropTypes from 'prop-types';
import {
    StudentsTableBody
} from './styledComponent.js';
import Row from '../row/Row.jsx';
import {Scrollbars} from 'react-custom-scrollbars';

const Body = (props) => {
    const {dictionary, students, groups, updateStudent} = props;
    const handleBody = (item, index) => {
        return <Row key={item.studentId}
                    indexOfRow={index + 1}
                    groupId={item.groupId}
                    groupName={item.groupName}
                    studentId={item.studentId}
                    lastName={item.lastName}
                    firstName={item.firstName}
                    dictionary={dictionary}
                    groups={groups}
                    updateStudent={updateStudent}
        />;
    };

    return (
        <StudentsTableBody>
            <Scrollbars style={{height: '55vh', minHeight: '350px'}}>
                {students.map((item, index) => handleBody(item, index))}
            </Scrollbars>
        </StudentsTableBody>
    );
};

Body.propTypes = {
    students: PropTypes.array.isRequired,
    groups: PropTypes.array.isRequired,
    updateStudent: PropTypes.func.isRequired,
    dictionary: PropTypes.object.isRequired,
};

export default React.memo(Body);

