import React from 'react';
import {
    StudentsTableWrapper,
    StudentsTableTitle,
    StudentsTableText,
    Table,
} from './styledComponent.js';
import PropTypes from 'prop-types';
import Header from './components/header/Header.jsx';
import Body from './components/body/Body.jsx';

const StudentsManager = (props) => {
    const { dictionary, students, groups, updateStudent, searchStudent } = props;
    return (
        <StudentsTableWrapper>
            <StudentsTableTitle>
                <StudentsTableText>{dictionary.resources.titleStudentsManager}</StudentsTableText>
            </StudentsTableTitle>
            <Table>
                <Header
                    dictionary={dictionary}
                    searchStudent={searchStudent}
                />
                <Body
                    dictionary={dictionary}
                    students={students}
                    groups={groups}
                    updateStudent={updateStudent}
                />
            </Table>
        </StudentsTableWrapper>
    );
};

StudentsManager.propTypes = {
    dictionary: PropTypes.object.isRequired,
    students: PropTypes.array.isRequired,
    groups: PropTypes.array.isRequired,
    updateStudent: PropTypes.func.isRequired,
    searchStudent: PropTypes.func.isRequired,
};

export default React.memo(StudentsManager);
