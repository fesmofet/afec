import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import Body from './components/body/Body.jsx';
import Header from './components/header/Header.jsx';
import CustomButton from '../../../../libs/customButton/CustomButton.jsx';
import {
    Table,
    ControlPanel,
    WeekNavigator,
    ButtonContainer,
    ScheduleTableWrapper,
    ButtonsContainer,
    ScheduleTableText,
    ScheduleTableTitle,
} from './styledComponent';

const Schedule = (props) => {
    const { dictionary, theme, filterLessons, lessons, toggleModalWindow, getSchedule, currentDate } = props;
    const user = JSON.parse(localStorage.getItem('currentUserData'));
    const handelSearchPrevWeekLessons = () => {
        const payload = {
            from: moment.utc(currentDate).subtract(7, 'days').valueOf(),
            to: moment.utc(currentDate).subtract(1, 'days').valueOf(),
            groups: user.groups,
            id: user.id,
        };
        if (!payload.from || !payload.to) {
            alert('Please choose From - To dates');
        } else {
            getSchedule(payload);
        }
    };
    const handelSearchNextWeekLessons = () => {
        const payload = {
            from: moment.utc(currentDate).add(7, 'days').valueOf(),
            to: moment.utc(currentDate).add(14, 'days').valueOf(),
            groups: user.groups,
            id: user.id,
        };
        getSchedule(payload);
    };
    const handelAddLessonClick = () => {
        toggleModalWindow({ modal: 'addLessonTeacher', isOpen: true });
    };
    const handelAddGroupClick = () => {
        // toggleModalWindow({ modal: 'addLessonAdmin', isOpen: true });
    };
    return (
        <ScheduleTableWrapper>
            <ScheduleTableTitle>
                <ScheduleTableText>{dictionary.resources.titleSchedule}</ScheduleTableText>
            </ScheduleTableTitle>
            <ControlPanel>
                <ButtonsContainer>
                    <ButtonContainer>
                        <CustomButton
                            fontSize={16}
                            boxShadow={'none'}
                            textColor={theme.textColor}
                            fontWeight={'bold'}
                            isDisabled={false}
                            buttonName={dictionary.button.addGroup}
                            borderRadius={10}
                            dataAttribute={'data-button-registration'}
                            textTransform={'uppercase'}
                            backgroundColor={theme.btnBgColor}
                            onClickCallback={handelAddGroupClick}
                            backgroundColorHover={theme.backgroundColorHover}
                        />
                    </ButtonContainer>
                    <ButtonContainer>
                        <CustomButton
                            fontSize={16}
                            boxShadow={'none'}
                            textColor={theme.textColor}
                            fontWeight={'bold'}
                            isDisabled={false}
                            buttonName={dictionary.button.addLesson}
                            borderRadius={10}
                            dataAttribute={'data-button-registration'}
                            textTransform={'uppercase'}
                            backgroundColor={theme.btnBgColor}
                            onClickCallback={handelAddLessonClick}
                            backgroundColorHover={theme.backgroundColorHover}
                        />
                    </ButtonContainer>
                </ButtonsContainer>
                <WeekNavigator>
                    <ButtonContainer>
                        <CustomButton
                            fontSize={16}
                            boxShadow={'none'}
                            textColor={theme.textColor}
                            fontWeight={'bold'}
                            isDisabled={false}
                            buttonName={dictionary.button.prevWeek}
                            borderRadius={10}
                            dataAttribute={'data-button-registration'}
                            textTransform={'uppercase'}
                            backgroundColor={theme.btnBgColor}
                            onClickCallback={handelSearchPrevWeekLessons}
                            backgroundColorHover={theme.backgroundColorHover}
                        />
                        <CustomButton
                            fontSize={16}
                            boxShadow={'none'}
                            textColor={theme.textColor}
                            fontWeight={'bold'}
                            isDisabled={false}
                            buttonName={dictionary.button.nextWeek}
                            borderRadius={10}
                            dataAttribute={'data-button-registration'}
                            textTransform={'uppercase'}
                            backgroundColor={theme.btnBgColor}
                            onClickCallback={handelSearchNextWeekLessons}
                            backgroundColorHover={theme.backgroundColorHover}
                        />
                    </ButtonContainer>
                </WeekNavigator>
            </ControlPanel>
            <Table>
                <Header dictionary={dictionary}
                        filterLessons={filterLessons}
                />
                <Body
                    dictionary={dictionary}
                    lessons={lessons}
                />
            </Table>
        </ScheduleTableWrapper>
    );
};

Schedule.propTypes = {
    dictionary: PropTypes.object.isRequired,
    filterLessons: PropTypes.func,
    lessons: PropTypes.array.isRequired,
    getSchedule: PropTypes.func.isRequired,
    toggleModalWindow: PropTypes.func.isRequired,
};

export default React.memo(Schedule);
