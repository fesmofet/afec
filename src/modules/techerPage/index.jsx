import { connect } from 'react-redux';
import TeacherPage from './TeacherPage.jsx';
import * as selectors from '../../constants/selectors';
import * as actions from '../../constants/actions';

const mapStateToProps = state => ({
    students: selectors.getStudents(state),
    lessons: selectors.getLessonsData(state),
    theme: selectors.getAdminPageTheme(state),
    dictionary: selectors.getDictionary(state),
    groups: selectors.getTeachersGroups(state),
    isAccount: selectors.checkIsAccount(state),
    attendances: selectors.getAttendances(state),
    currentDate: selectors.getCurrentDate(state),
    authData: selectors.getAuthorizationData(state),
    currentModule: selectors.getCurrentModuleTeacher(state),
});

const mapDispatchToProps = dispatch => ({
    logout: history => dispatch(actions.logout(history)),
    switchAccount: () => dispatch(actions.switchAccount()),
    toggleFullScreen: () => dispatch(actions.toggleFullScreen()),
    getSchedule: payload => dispatch(actions.getSchedule(payload)),
    getStudents: payload => dispatch(actions.getStudents(payload)),
    updateStudent: payload => dispatch(actions.updateStudent(payload)),
    searchStudent: payload => dispatch(actions.searchStudent(payload)),
    getAttendance: payload => dispatch(actions.getAttendance(payload)),
    changeModule: payload => dispatch(actions.changeModuleTeacher(payload)),
    searchAttendance: payload => dispatch(actions.searchAttendance(payload)),
    updateAttendance: payload => dispatch(actions.updateAttendance(payload)),
    getTeachersGroups: payload => dispatch(actions.getTeachersGroups(payload)),
    toggleModalWindow: payload => dispatch(actions.toggleModalWindow(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TeacherPage);
