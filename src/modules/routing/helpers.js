import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import decode from 'jwt-decode';
import PropTypes from 'prop-types';

const getCurrentUserData = () => {
    const token = localStorage.getItem('token');
    if (!token) {
        return null;
    }
    try {
        return decode(token, 'secret');
    } catch (e) {
        return null;
    }
};

export const PrivateRoute = ({ component: Component, role, ...rest }) =>
    <Route {...rest} render={props => {
        const currentUserData = getCurrentUserData();
        if (currentUserData === null || role !== currentUserData.role) {
            return <Redirect to={{ pathname: '/' }}/>;
        }
        return <Component {...props} />;
    }} />
;

PrivateRoute.propTypes = {
    component: PropTypes.object,
    role: PropTypes.string,
};

export const Role = {
    Admin: 'admin',
    Teacher: 'teacher',
    Student: 'student',
};
