import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from 'react-router-dom';
import { PrivateRoute, Role } from './helpers';
import AdminPage from '../adminPage/index.jsx';
import { GlobalStyle } from './styledComponents';
import TeacherPage from '../techerPage/index.jsx';
import StudentPage from '../studentPage/index.jsx';
import RegPage from '../authRegPage/regPage/index.jsx';
import AuthPage from '../authRegPage/authPage/index.jsx';
import ModalManager from '../../managers/modalManager/index.jsx';

export default class Layout extends React.Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path='/' component={AuthPage}/>
                    <Route exact path='/registration' component={RegPage}/>
                    <PrivateRoute exact role={Role.Admin} path='/admin' component={AdminPage}/>
                    <PrivateRoute exact role={Role.Teacher} path='/teacher' component={TeacherPage}/>
                    <PrivateRoute exact role={Role.Student} path='/student' component={StudentPage}/>
                    <Redirect to='/'/>
                </Switch>
                <ModalManager />
                <GlobalStyle direction={'ltr'}/>
            </Router>
        );
    }
}
