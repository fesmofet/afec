import React from 'react';
import PropTypes from 'prop-types';
import {
    InputBox,
    LabelBox,
    HeaderItem,
    UserTableHeader,
    InputBlockWrapper,
} from './styledComponent.js';
import headerScheduleTable from '../../../../../../constants/headerScheduleTable';

const groups = React.createRef();
const teachers = React.createRef();
const searchRefs = {
    groups,
    teachers,
};

const Header = (props) => {
    const handleOnChange = () => {
        const payload = {
            groups: searchRefs.groups.current.value,
            teachers: searchRefs.teachers.current.value,
        };
        props.filterLessons(payload);
    };
    const { dictionary,
        labelTextColor,
        labelBgColor,
        labelBorderColor,
        labelFontSize,
        labelFontWeight,
        labelBorderRadius,
        inputTextColor,
        inputBgColor,
        inputFontSize,
        dataAttribute,
        inputBlockBgColor,
    } = props;
    return (
        <UserTableHeader>
            {headerScheduleTable && headerScheduleTable.length ? headerScheduleTable.map((item) =>
                <HeaderItem key={item.id}>
                    <InputBlockWrapper
                        inputBlockBgColor={inputBlockBgColor}
                        inputBlockBrRadius={labelBorderRadius}>
                        <LabelBox
                            htmlFor={item.id}
                            labelTextColor={labelTextColor}
                            labelBgColor={labelBgColor}
                            labelBorderColor={labelBorderColor}
                            labelBorderRadius = {labelBorderRadius}
                            labelFontSize={labelFontSize}
                            labelfontWeight={labelFontWeight}
                            children={dictionary.labels[item.labelText]} />
                        {item.isSearchable ?
                        <InputBox
                            onChange={handleOnChange}
                            ref = {searchRefs[item.labelText]}
                            id={item.id}
                            type={item.inputType}
                            placeholder={dictionary.placeholders.search}
                            inputTextColor={inputTextColor}
                            inputBgColor={inputBgColor}
                            inputFontSize={inputFontSize}
                            data-at={dataAttribute}
                            inputBrRadius={labelBorderRadius}
                            disabled={item.isDisabled}
                            isFullHeight = { true }
                        />
                         : null }
                    </InputBlockWrapper>
                </HeaderItem>
            ) : null}
        </UserTableHeader>
    );
};

Header.propTypes = {
    dictionary: PropTypes.object.isRequired,
    labelTextColor: PropTypes.string,
    labelBgColor: PropTypes.string,
    labelBorderColor: PropTypes.string,
    labelFontSize: PropTypes.number,
    labelFontWeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    labelBorderRadius: PropTypes.number,
    inputTextColor: PropTypes.string,
    inputBgColor: PropTypes.string,
    inputFontSize: PropTypes.number,
    inputBlockBgColor: PropTypes.string,
    dataAttribute: PropTypes.string,
    isDisabled: PropTypes.bool,
    onChangeCallback: PropTypes.func,
    filterTeachers: PropTypes.func,
    filterLessons: PropTypes.func,
};

export default React.memo(Header);
