import styled from 'styled-components';
const inputBlockBgColorDefault = '#fff';
const labelTextColorDefault = '#fff';
const labelBgColorDefault = '#236ea1';
const labelBorderColorDefault = '#d77f45';
const labelBorderRadiusDefault = 3;
const labelFontSizeDefault = 16;
const labelFontWeightDefault = 'normal';
const inputTextColorDefault = 'black';
const inputBgColorDefault = '#fff';
const inputFontSizeDefault = 16;

export const UserTableHeader = styled.div`
  width: 100%;
  display:flex;
  justify-content: space-between;
  align-items: center;
`;

export const HeaderItem = styled.div`
  height: 100%;
  box-sizing: border-box;
  border: 1px solid black;
  
   
  &:nth-child(1){
    width: 20%;
  }
  
  &:nth-child(2){
    width: 20%;
  }
  
  &:nth-child(3){
    width: 20%;
  }
    
  &:nth-child(4){
    width: 20%;
  }
  
  &:nth-child(5){
    width: 20%;
  }
`;

export const InputBlockWrapper = styled.div`
    width: 100%;
    height: 60px;
    background-color: ${props => props.inputBlockBgColor ? props.inputBlockBgColor : inputBlockBgColorDefault};
    border-radius: ${props => props.inputBlockBrRadius ? props.inputBlockBrRadius : labelBorderRadiusDefault}px;
    display: flex;
    flex-direction: column;
    justify-content: center;
`;

export const LabelBox = styled.label`
    width: 100%;
    height: ${ props => props.isFullHeight ? '' : 100 }%;
    padding: 3px 0;
    display: flex;
    justify-content: center;
    box-sizing: border-box;
    align-items: center;
    color: ${props => props.labelTextColor ? props.labelTextColor : labelTextColorDefault};
    background-color: ${props => props.labelBgColor ? props.labelBgColor : labelBgColorDefault};
    border: 2px solid ${props => props.labelBorderColor ? props.labelBorderColor : labelBorderColorDefault};
    border-radius: ${props => props.labelBorderRadius ? props.labelBorderRadius : labelBorderRadiusDefault}px;
    font-size: ${props => props.labelFontSize ? props.labelFontSize : labelFontSizeDefault}px;
    font-weight: ${props => props.fontWeight ? props.fontWeight : labelFontWeightDefault};
`;

export const InputBox = styled.input`
    width: 100%;
    height: ${ props => !props.isFullHeight ? 50 : 100 }%;
    outline: none;
    border: none;
    box-sizing: border-box;
    border-radius: ${props => props.inputBrRadius ? props.inputBrRadius : labelBorderRadiusDefault}px;
    color: ${props => props.inputTextColor ? props.inputTextColor : inputTextColorDefault};
    background-color: ${props => props.inputBgColor ? props.inputBgColor : inputBgColorDefault};
    font-size: ${props => props.inputFontSize ? props.inputFontSize : inputFontSizeDefault}px;
    text-align: center;
`;
export const defaultStyles = {
    inputBlockBgColorDefault,
    labelTextColorDefault,
    labelBgColorDefault,
    labelBorderColorDefault,
    labelBorderRadiusDefault,
    labelFontSizeDefault,
    labelFontWeightDefault,
    inputTextColorDefault,
    inputBgColorDefault,
    inputFontSizeDefault,
};

