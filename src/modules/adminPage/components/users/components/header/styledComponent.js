import styled from 'styled-components';

let defaultStyles = {
    bgTableItem: '#236ea1',
};

export const UserTableHeader = styled.div`
  width: 100%;
  display:flex;
  justify-content: space-between;
  align-items: center;
`;

export const HeaderItem = styled.div`
  height: 100%;
  box-sizing: border-box;
  border: 1px solid black;
  min-height: 60px;
  background: ${props => props.theme.btnBgColor ? props.theme.btnBgColor : defaultStyles.bgTableItem};
  
  &:nth-child(1){
    width: 3%;
  }
  
  &:nth-child(2){
    width: 11%;
  }
  
  &:nth-child(3){
    width: 11%;
  }
  
  &:nth-child(4){
    width: 13%;
  }
  
  &:nth-child(5){
    width: 13%;
  }
  
  &:nth-child(6){
    width: 6%;
  }
  &:nth-child(7){
    width: 11%;
  }
  &:nth-child(8){
    width: 13%;
  }
  &:nth-child(9){
    width: 12%;
  }
   &:nth-child(10){
    min-width: 85px;
    width: 7%;
  }
`;

