import React from 'react';
import PropTypes from 'prop-types';
import {
    UserTableHeader,
    HeaderItem,
} from './styledComponent.js';
import InputBlock from '../../../../../../libs/InputBlock/InputBlock.jsx';
import headerUserTable from '../../../../../../constants/headerUserTable.js';

const Header = (props) => {
    const { dictionary, searchUser } = props;

    const lastName = React.createRef();
    const firstName = React.createRef();
    const email = React.createRef();
    const phone = React.createRef();
    const role = React.createRef();
    const groupForUser = React.createRef();
    const teacherLevel = React.createRef();

    const userTableHeaderRefs = { lastName, firstName, email, phone, role, groupForUser, teacherLevel };

    const handleSearch = () => {
        let data = {
            lastName: userTableHeaderRefs.lastName.current.value,
            firstName: userTableHeaderRefs.firstName.current.value,
            email: userTableHeaderRefs.email.current.value,
            phone: userTableHeaderRefs.phone.current.value,
            role: userTableHeaderRefs.role.current.value,
            groupForUser: userTableHeaderRefs.groupForUser.current.value,
            teacherLevel: userTableHeaderRefs.teacherLevel.current.value,
        };
        searchUser(data);
    };

    return (
        <UserTableHeader>
            {headerUserTable && headerUserTable.length ? headerUserTable.map((item) =>
                <HeaderItem key={item.id}>
                    <InputBlock
                        id={item.id}
                        refValue={userTableHeaderRefs[item.labelText]}
                        inputType={item.inputType}
                        labelText={dictionary.labels[item.labelText]}
                        isDisabled={item.isDisabled}
                        placeholder={dictionary.placeholders[item.placeholder]}
                        onChangeCallback={handleSearch}
                    />
                </HeaderItem>
            ) : null}
            <HeaderItem></HeaderItem>
        </UserTableHeader>
    );
};

Header.propTypes = {
    dictionary: PropTypes.object.isRequired,
    searchUser: PropTypes.func.isRequired,
};

export default React.memo(Header);
