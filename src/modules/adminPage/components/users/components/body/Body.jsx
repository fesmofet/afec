import React from 'react';
import PropTypes from 'prop-types';
import {
    UserTableBody,
} from './styledComponent.js';
import Row from '../row/Row.jsx';
import { Scrollbars } from 'react-custom-scrollbars';

const Body = (props) => {
    const { dictionary, users, authData, groups, deleteUser, updateUser, toggleModalWindow, deleteConfirm, getModalsData } = props;
    const handleDelete = (item, index) => toggleModalWindow({ modal: 'deletingModal', isOpen: true, deleteID: { userId: item.id, id: index, role: item.role } });
    if (deleteConfirm) {
        deleteUser(getModalsData.deletingModal.deleteID);
    }   
    const handleBody = (item, index) => {
        return item.email !== authData.userData.email ?
            <Row key={item.id}
                 id={index + 1}
                 userId={item.id}
                 lastName={item.lastName}
                 firstName={item.firstName}
                 email={item.email}
                 phone={item.phone}
                 role={item.role}
                 groupName={item.currentGroup}
                 teacherLevel={item.teacherLevel}
                 salaryPerHour={item.salaryPerHour}
                 dictionary={dictionary}
                 groups={groups}
                 updateUser={updateUser}
                 handleDelete={() => handleDelete(item, index)}
            /> : null;
    };

    return (
        <UserTableBody>
            <Scrollbars style={{ height: '55vh', minHeight: '350px' }}>
                {users.map((item, index) => handleBody(item, index))}
            </Scrollbars>
        </UserTableBody>
    );
};

Body.propTypes = {
    users: PropTypes.array.isRequired,
    groups: PropTypes.array.isRequired,
    authData: PropTypes.object.isRequired,
    changeRole: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired,
    updateUser: PropTypes.func.isRequired,
    toggleModalWindow: PropTypes.func.isRequired,
    dictionary: PropTypes.object.isRequired,
    getModalsData: PropTypes.object.isRequired,
    deleteConfirm: PropTypes.bool,
};

export default React.memo(Body);

