import React from 'react';
import { useState } from 'react';
import PropTypes from 'prop-types';
import {
    BodyRow,
    RowItem,
    ItemText,
    ItemInput,
    BtnPencil,
    BtnDelete,
    BtnCancel,
    BtnCheckMark,
} from './styledComponent.js';
import CustomSelect from '../../../../../../libs/customSelect/CustomSelect.jsx';

const Row = (props) => {
    const { id, lastName, firstName, email, phone, role, groupName, teacherLevel, dictionary, groups, userId, updateUser, handleDelete, salaryPerHour } = props;
    const array = [id, lastName, firstName, email, phone, role, groupName, teacherLevel, salaryPerHour];
    const [isText, setIsText] = useState(true);
    const refLastName = React.createRef();
    const refFirstName = React.createRef();
    const refEmail = React.createRef();
    const refPhone = React.createRef();
    const refRole = React.createRef();
    const refGroups = React.createRef();
    const refTeacherLevel = React.createRef();
    const refSalaryPerHour = React.createRef();

    const getGroupValueFromName = (name) => {
        for (let i = 0; i < groups.length; i++) {
            if (groups[i].name === name) {
                return groups[i].id;
            }
        }
    };

    const handleUpdateUser = () => {
        let data = {
            index: id - 1,
            userNewData: {
                id: userId,
                firstName: refFirstName.current.value,
                lastName: refLastName.current.value,
                email: refEmail.current.value,
                phone: refPhone.current.value,
                role: refRole.current.value,
                groups: refGroups.current ? parseInt(refGroups.current.value, 10) : null,
                teacherLevel: refTeacherLevel.current ? refTeacherLevel.current.value : null,
                salaryPerHour: refTeacherLevel.current ? refSalaryPerHour.current.value : null,
            },
        };
        updateUser(data);
        setIsText(!isText);
    };

    const changeContent = () => setIsText(!isText);
    const showText = (item, index) => <RowItem key={index}>
        <ItemText>{item ? item : dictionary.resources.none}</ItemText>
    </RowItem>;

    const showInputs = () => <>
        <RowItem>
            <ItemText>{id}</ItemText>
        </RowItem>
        <RowItem>
            <ItemInput defaultValue={lastName} ref={refLastName}/>
        </RowItem>
        <RowItem>
            <ItemInput defaultValue={firstName} ref={refFirstName}/>
        </RowItem>
        <RowItem>
            <ItemInput defaultValue={email} ref={refEmail}/>
        </RowItem>
        <RowItem>
            <ItemInput defaultValue={phone} ref={refPhone}/>
        </RowItem>
        <RowItem>
            <CustomSelect
                options={[
                    {
                        id: `${id}S`,
                        value: 'student',
                        innerText: dictionary.roleSelect.student,
                    },
                    {
                        id: `${id}A`,
                        value: 'admin',
                        innerText: dictionary.roleSelect.admin,
                    },
                    {
                        id: `${id}T`,
                        value: 'teacher',
                        innerText: dictionary.roleSelect.teacher,
                    },
                ]}
                defaultValue={role}
                referral={refRole}
            />
        </RowItem>
        <RowItem>
            {role === 'student' ? <CustomSelect
                options={groups.map(group => ({
                    id: group.id,
                    value: group.id,
                    innerText: group.name,
                }))}
                defaultValue={getGroupValueFromName(groupName)}
                referral={refGroups}
            /> : <ItemText>{dictionary.resources.none}</ItemText>}
        </RowItem>
        <RowItem>
            {role === 'teacher' ? <ItemInput defaultValue={teacherLevel} ref={refTeacherLevel}/> : <ItemText>{dictionary.resources.none}</ItemText>}
        </RowItem>
        <RowItem>
            {role === 'teacher' ? <ItemInput defaultValue={salaryPerHour} ref={refSalaryPerHour}/> : <ItemText>{dictionary.resources.none}</ItemText>}
        </RowItem>
    </>;

    return (
        <BodyRow>
            {isText ? array.map((item, index) => showText(item, index)) : showInputs()}
            <RowItem>
                {isText ?
                    <>
                        <BtnPencil onClick={changeContent}/>
                        <BtnDelete onClick={handleDelete}/>
                    </>
                    :
                    <>
                        <BtnCheckMark onClick={handleUpdateUser}/>
                        <BtnCancel onClick={changeContent}/>
                    </>
                }
            </RowItem>
        </BodyRow>
    );
};

Row.propTypes = {
    id: PropTypes.number.isRequired,
    userId: PropTypes.number.isRequired,
    lastName: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    groupName: PropTypes.string,
    teacherLevel: PropTypes.string,
    salaryPerHour: PropTypes.number,
    dictionary: PropTypes.object.isRequired,
    groups: PropTypes.array.isRequired,
    updateUser: PropTypes.func.isRequired,
    handleDelete: PropTypes.func.isRequired,
};

export default React.memo(Row);
