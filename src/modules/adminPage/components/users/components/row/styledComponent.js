import styled from 'styled-components';

let defaultStyles = {
    bgTableItem: '#fff',
};

export const BodyRow = styled.div`
  width: 100%;
  height: 50px;
  display:flex;
  justify-content: center;
  align-items: center;
  flex: 3 1 100px;
`;

export const RowItem = styled.div`
  width: 100%;
  height: 100%;
  display:flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  border: .5px solid black;
  background: ${props => props.theme.bgTableItem ? props.theme.bgTableItem : defaultStyles.bgTableItem};
  &:nth-child(1){
    width: 3%;
  }
  
  &:nth-child(2){
    width: 11%;
  }
  
  &:nth-child(3){
    width: 11%;
  }
  
  &:nth-child(4){
    width: 13%;
  }
  
  &:nth-child(5){
    width: 13%;
  }
  
  &:nth-child(6){
    width: 6%;
  }
  &:nth-child(7){
    width: 11%;
  }
  &:nth-child(8){
    width: 13%;
  }
  &:nth-child(9){
    width: 12%;
  }
   &:nth-child(10){
    min-width: 85px;
    width: 7%;
  }
`;

export const ItemText = styled.p`
    overflow: hidden;
    font-size: 14px;
    text-align: center;
    text-overflow: ellipsis;
    width: 90%;
`;

export const ItemInput = styled.input`
    font-size: 15px;
    width: 100%;
    height: 40px;
    text-overflow: ellipsis
`;

export const BtnPencil = styled.div`
  width: 30px;
  height: 30px;
  cursor: pointer;
  background-size: 30px;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('../../../../../../../img/pencil.svg');
  margin-right: 10px;
`;

export const BtnDelete = styled.div`
    width: 30px;
  height: 30px;
  cursor: pointer;
  background-size: 30px;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('../../../../../../../img/close.svg');
`;

export const BtnCancel = styled.div`
    width: 30px;
  height: 30px;
  cursor: pointer;
  background-size: 30px;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('../../../../../../../img/close.svg');
`;

export const BtnCheckMark = styled.div`
    width: 30px;
  height: 30px;
  cursor: pointer;
  background-size: 30px;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('../../../../../../../img/correct.svg');
  margin-right: 10px;
`;

