import styled from 'styled-components';

export const UserTableWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: flex-start;
`;

export const UserTableTitle = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 35px 0 20px;
`;

export const UserTableText = styled.span`
  font-size: 25px;
  font-weight: bold;
`;

export const Table = styled.div`
  box-sizing: border-box;
`;
