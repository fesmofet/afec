import Header from '../components/header/Header.jsx';
import en from '../../../../../managers/localManager/translates/en';

describe('UserTable components test: Header react component', () => {
    const props = {
        dictionary: en,
    };

    it('Header snapshot created, should rendered correctly with props', done => {
        const wrapper = shallow(<Header {...props}/>);
        expect(wrapper).matchSnapshot();
        done();
    });
});
