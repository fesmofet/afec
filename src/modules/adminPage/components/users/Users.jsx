import React from 'react';
import PropTypes from 'prop-types';
import {
    UserTableWrapper,
    UserTableTitle,
    UserTableText,
    Table,
} from './styledComponent.js';
import Header from './components/header/Header.jsx';
import Body from './components/body/Body.jsx';

const Users = (props) => {
    const { dictionary, changeRole, users, authData, groups, deleteUser, updateUser, searchUser, toggleModalWindow, deleteConfirm, getModalsData } = props;
    return (
        <UserTableWrapper>
            <UserTableTitle>
                <UserTableText>{dictionary.resources.titleUserTable}</UserTableText>
            </UserTableTitle>
            <Table>
                <Header
                    dictionary={dictionary}
                    searchUser={searchUser}
                />
                <Body
                    dictionary={dictionary}
                    users={users}
                    changeRole={changeRole}
                    authData={authData}
                    groups={groups}
                    deleteUser={deleteUser}
                    updateUser={updateUser}
                    toggleModalWindow={toggleModalWindow}
                    deleteConfirm={deleteConfirm}
                    getModalsData={getModalsData}
                />
            </Table>
        </UserTableWrapper>
    );
};

Users.propTypes = {
    users: PropTypes.array.isRequired,
    groups: PropTypes.array.isRequired,
    authData: PropTypes.object.isRequired,
    getModalsData: PropTypes.object.isRequired,
    changeRole: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired,
    updateUser: PropTypes.func.isRequired,
    dictionary: PropTypes.object.isRequired,
    searchUser: PropTypes.func.isRequired,
    toggleModalWindow: PropTypes.func,
    deleteConfirm: PropTypes.bool,
};

export default React.memo(Users);
