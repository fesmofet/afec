import React from 'react';
import {
    Table,
    ControlPanel,
    SalaryTableText,
    ButtonContainer,
    SalaryTableTitle,
    SalaryTableWrapper,
    InputBlockContainer,
} from '../salary/styledComponent';
import CustomButton from '../../../../libs/customButton/CustomButton.jsx';
import Header from '../salary/components/header/Header.jsx';
import Body from '../salary/components/body/Body.jsx';
import PropTypes from 'prop-types';
import InputBlock from '../../../../libs/InputBlock/InputBlock.jsx';
import config from '../../../../constants/config';
import moment from 'moment';

const dateTo = React.createRef();
const dateFrom = React.createRef();
const salaryRefs = {
    dateTo,
    dateFrom,
};

const Salary = (props) => {
    const { dictionary, changeRole, teachers, authData, getTeachersSalary, theme, filterTeachers } = props;
    const defaultValues = {
        dateTo: moment.utc().format('YYYY-MM-DD'),
        dateFrom: moment.utc().startOf('month').format('YYYY-MM-DD'),
    };
    const handelGetSalary = () => {
        const payload = {
            from: moment.utc(salaryRefs.dateFrom.current.value).startOf('day').valueOf(),
            to: moment.utc(salaryRefs.dateTo.current.value).endOf('day').valueOf(),
        };
        if (!payload.from || !payload.to) {
            alert('Please choose From and To dates');
        } else {
            getTeachersSalary(payload);
        }
    };
    return (
        <SalaryTableWrapper>
            <SalaryTableTitle>
                <SalaryTableText>{dictionary.resources.titleSalaryTable}</SalaryTableText>
            </SalaryTableTitle>
            <ControlPanel>
                <InputBlockContainer>
                    {config.salaryInputs.map((input, index) =>
                        <InputBlock
                            key={index}
                            id={input.id}
                            labelText={dictionary.labels[input.labelsKey]}
                            labelTextColor={theme.labelTextColor}
                            labelBgColor={theme.labelBgColor}
                            labelBorderColor={theme.labelBorderColor}
                            labelFontSize={input.labelFontSize}
                            labelfontWeight={input.labelFontWeight}
                            labelBorderRadius={input.labelBorderRadius}
                            inputType={input.inputType}
                            placeholder={dictionary.placeholders[input.placeholderKey]}
                            inputTextColor={theme.inputTextColor}
                            inputBgColor={theme.inputBgColor}
                            inputFontSize={input.inputFontSize}
                            dataAttribute={input.dataAttribute}
                            inputBlockBgColor={theme.inputBlockBgColor}
                            refValue={salaryRefs[input.refValue]}
                            value = {defaultValues[input.labelsKey]}
                        />)}
                </InputBlockContainer>
                <ButtonContainer>
                    <CustomButton
                        fontSize={16}
                        boxShadow={'none'}
                        textColor={theme.textColor}
                        fontWeight={'bold'}
                        isDisabled={false}
                        buttonName={dictionary.button.calculate}
                        borderRadius={10}
                        dataAttribute={'data-button-registration'}
                        textTransform={'uppercase'}
                        backgroundColor={theme.btnBgColor}
                        onClickCallback={handelGetSalary}
                        backgroundColorHover={theme.backgroundColorHover}
                    />
                </ButtonContainer>
            </ControlPanel>
            <Table>
                <Header dictionary={dictionary}
                        filterTeachers ={ filterTeachers }/>
                <Body
                    dictionary={dictionary}
                    teachers={teachers}
                    changeRole={changeRole}
                    authData={authData}/>
            </Table>
        </SalaryTableWrapper>
    );
};

Salary.propTypes = {
    teachers: PropTypes.array.isRequired,
    changeRole: PropTypes.func.isRequired,
    dictionary: PropTypes.object.isRequired,
    authData: PropTypes.object.isRequired,
    getTeachersSalary: PropTypes.func.isRequired,
    filterTeachers: PropTypes.func.isRequired,
    theme: PropTypes.object.isRequired,
    value: PropTypes.string,
};

export default React.memo(Salary);
