import styled from 'styled-components';

export const SalaryTableWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: flex-start;
`;

export const SalaryTableTitle = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 35px 0 20px;
`;

export const SalaryTableText = styled.span`
  font-size: 25px;
  font-weight: bold;
`;

export const Table = styled.div`
  width: 100%;  
  box-sizing: border-box;
`;

export const ControlPanel = styled.div `
   width: 100%;   
   height: 50px;
   display: flex;
   flex-direction: row;
   justify-content: start;
   align-items: center;
   padding-top: 10px;
   margin-bottom: 10px;
`;

export const ButtonContainer = styled.div `
   width: 150px;
   height: 50px;
`;

export const InputBlockContainer = styled.div `
  display: flex;
  flex-direction: row;
   width: 40%;
   height: 50px;
`;
