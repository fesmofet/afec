import styled from 'styled-components';

let defaultStyles = {
    bgTableItem: '#fff',
};

export const UserTableBody = styled.div`
  width: 100%;
  box-sizing: border-box;
`;

export const BodyRow = styled.div`
  width: 100%;
  height: 50px;
  display:flex;
  justify-content: center;
  align-items: center;
  flex:  3 1 100px;
`;

export const RowItem = styled.div`
  height: 100%;
  display:flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  border: .5px solid black;
  font-size: 15px;
  background: ${props => props.theme.bgTableItem ? props.theme.bgTableItem : defaultStyles.bgTableItem};
  
  &:nth-child(1){
    width: 5%;
  }
  
  &:nth-child(2){
    width: 20%;
  }
  
  &:nth-child(3){
    width: 20%;
  }
    
  &:nth-child(4){
    width: 20%;
  }
  
  &:nth-child(5){
    width: 20%;
  }
  &:nth-child(6){
    width: 15%;
  }
`;

