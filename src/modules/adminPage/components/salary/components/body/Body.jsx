import React from 'react';
import PropTypes from 'prop-types';
import {
    BodyRow,
    RowItem,
    UserTableBody,
} from './styledComponent.js';
import { Scrollbars } from 'react-custom-scrollbars';

const Body = (props) => {
    const { teachers } = props;
    return (
        <UserTableBody>
            <Scrollbars style={{ height: '50vh', minHeight: '350px' }}>
                {Object.values(teachers) && Object.values(teachers).length ? teachers.map((user, rowIndex) => {
                    return <BodyRow key={rowIndex}>
                        <RowItem> {rowIndex + 1} </RowItem>
                        <RowItem> {user.last_name} </RowItem>
                        <RowItem> {user.first_name} </RowItem>
                        <RowItem> {user.salaryperhour} </RowItem>
                        <RowItem> {user.hours} </RowItem>
                        <RowItem> {user.money} </RowItem>
                    </BodyRow>;
                }) : null}
            </Scrollbars>
        </UserTableBody>
    );
};

Body.propTypes = {
    teachers: PropTypes.array.isRequired,
    changeRole: PropTypes.func.isRequired,
    dictionary: PropTypes.object.isRequired,
    authData: PropTypes.object.isRequired,
};

export default React.memo(Body);

