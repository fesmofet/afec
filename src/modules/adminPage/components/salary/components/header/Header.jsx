import React from 'react';
import PropTypes from 'prop-types';
import {
    InputBox,
    LabelBox,
    HeaderItem,
    UserTableHeader,
    InputBlockWrapper,
} from './styledComponent.js';
import headerUserTable from '../../../../../../constants/headerSalaryTable';

const lastName = React.createRef();
const firstName = React.createRef();
const searchRefs = {
    lastName,
    firstName,
};

const Header = (props) => {
    const handleOnChange = () => {
        const payload = {
            lastName: searchRefs.lastName.current.value,
            firstName: searchRefs.firstName.current.value,
        };
        props.filterTeachers(payload);
    };
    const { dictionary,
        labelTextColor,
        labelBgColor,
        labelBorderColor,
        labelFontSize,
        labelFontWeight,
        labelBorderRadius,
        inputTextColor,
        inputBgColor,
        inputFontSize,
        dataAttribute,
        inputBlockBgColor,
    } = props;
    return (
        <UserTableHeader>
            {headerUserTable && headerUserTable.length ? headerUserTable.map((item) =>
                <HeaderItem key={item.id}>
                    <InputBlockWrapper
                        inputBlockBgColor={inputBlockBgColor}
                        inputBlockBrRadius={labelBorderRadius}>
                        <LabelBox
                            htmlFor={item.id}
                            labelTextColor={labelTextColor}
                            labelBgColor={labelBgColor}
                            labelBorderColor={labelBorderColor}
                            labelBorderRadius = {labelBorderRadius}
                            labelFontSize={labelFontSize}
                            labelfontWeight={labelFontWeight}
                            children={dictionary.labels[item.labelText]} />
                        {item.isSearchable ?
                        <InputBox
                            onChange={handleOnChange}
                            ref = {searchRefs[item.labelText]}
                            id={item.id}
                            type={item.inputType}
                            placeholder={dictionary.placeholders[item.placeholder]}
                            inputTextColor={inputTextColor}
                            inputBgColor={inputBgColor}
                            inputFontSize={inputFontSize}
                            data-at={dataAttribute}
                            inputBrRadius={labelBorderRadius}
                            disabled={item.isDisabled}
                            isFullHeight = { true }/>
                         : null }
                    </InputBlockWrapper>
                </HeaderItem>
            ) : null}
        </UserTableHeader>
    );
};

Header.propTypes = {
    dictionary: PropTypes.object.isRequired,
    labelTextColor: PropTypes.string,
    labelBgColor: PropTypes.string,
    labelBorderColor: PropTypes.string,
    labelFontSize: PropTypes.number,
    labelFontWeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    labelBorderRadius: PropTypes.number,
    inputTextColor: PropTypes.string,
    inputBgColor: PropTypes.string,
    inputFontSize: PropTypes.number,
    inputBlockBgColor: PropTypes.string,
    dataAttribute: PropTypes.string,
    isDisabled: PropTypes.bool,
    onChangeCallback: PropTypes.func,
    filterTeachers: PropTypes.func,
};

export default React.memo(Header);
