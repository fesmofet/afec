import React from 'react';
import {
    GroupWrapper,
    ControlWrapper,
    GroupTableText,
    GroupTableTitle,
    ButtonContainer,
    ButtonsContainer,
    GroupTableWrapper,
} from './styledComponent.js';
import PropTypes from 'prop-types';
import Header from './components/header/Header.jsx';
import Body from './components/body/Body.jsx';
import CustomButton from '../../../../libs/customButton/CustomButton.jsx';

const Groups = props => {
    const { dictionary, theme, getAdminGroupsData, filterGroupsPage, toggleModalWindow, deleteGroupAdmin, deleteConfirm, getModalsData } = props;
    const handleOpenModalAddGroup = () => {
        toggleModalWindow({ modal: 'addGroup', isOpen: true });
    };
    return (
        <GroupWrapper>
            <GroupTableWrapper>
                <GroupTableTitle>
                    <GroupTableText>
                        {dictionary.resources.titleGroups}
                    </GroupTableText>
                </GroupTableTitle>
                <ControlWrapper>
                    <ButtonsContainer>
                        <ButtonContainer>
                        <CustomButton
                            fontSize={16}
                            boxShadow={'none'}
                            textColor={theme.textColor}
                            fontWeight={'bold'}
                            isDisabled={false}
                            buttonName={dictionary.button.addGroup}
                            borderRadius={10}
                            dataAttribute={'data-button-registration'}
                            textTransform={'uppercase'}
                            backgroundColor={theme.btnBgColor}
                            onClickCallback={handleOpenModalAddGroup}
                            backgroundColorHover={theme.backgroundColorHover}
                        />
                        </ButtonContainer>
                    </ButtonsContainer>
                </ControlWrapper>
                <Header
                    filterGroupsPage={filterGroupsPage}
                    dictionary={dictionary}/>
                <Body
                    getModalsData={getModalsData}
                    toggleModalWindow={toggleModalWindow}
                    deleteConfirm={deleteConfirm}
                    deleteGroupAdmin={deleteGroupAdmin}
                    groups={getAdminGroupsData}/>
            </GroupTableWrapper>
        </GroupWrapper>
    );
};
Groups.propTypes = {
    dictionary: PropTypes.object.isRequired,
    getAdminGroupsData: PropTypes.array,
    filterGroupsPage: PropTypes.func,
    toggleModalWindow: PropTypes.func,
    theme: PropTypes.object.isRequired,
    deleteGroupAdmin: PropTypes.func,
    getModalsData: PropTypes.object,
    deleteConfirm: PropTypes.bool,
};
export default React.memo(Groups);
