export default [
    {
        id: 'gNumber',
        inputType: 'text',
        labelText: 'number',
        isDisabled: true,
        placeholder: 'number',
    },
    {
        id: 'groupName',
        inputType: 'text',
        labelText: 'group',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'teacherLN',
        inputType: 'text',
        labelText: 'teacherLN',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'teacherFN',
        inputType: 'text',
        labelText: 'teacherFN',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'city',
        inputType: 'text',
        labelText: 'city',
        isDisabled: false,
        placeholder: 'search',
    },
    {
        id: 'groupLevel',
        inputType: 'text',
        labelText: 'groupLevel',
        isDisabled: false,
        placeholder: 'search',
    },
];
