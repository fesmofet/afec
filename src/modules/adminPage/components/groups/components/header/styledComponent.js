import styled from 'styled-components';

const labelTextColorDefault = '#fff';
const labelBgColorDefault = '#236ea1';
const labelBorderColorDefault = '#d77f45';
const labelBorderRadiusDefault = 3;
const labelFontSizeDefault = 16;
const labelFontWeightDefault = 'normal';

export const GroupTableHeader = styled.div`
  width: 1200px;
  height: 60px;
  display:flex;
  align-items: center;
`;

export const HeaderItem = styled.div`
  height: 100%;
  box-sizing: border-box;
  border: 1px solid black;
  
  &:nth-child(1){
    width: 3%;
  }
  
  &:nth-child(2){
    width: 14%;
  }
  
  &:nth-child(3){
    width: 14%;
  }
  
  &:nth-child(4){
    width: 14%;
  }
  
  &:nth-child(5){
    width: 14%;
  }
  
  &:nth-child(6){
    width: 14%;
  }
  &:nth-child(6){
    width: 14%;
  }
  &:nth-child(7){
    width: 14%;
  }
  &:nth-child(8){
    width: 14%;
  }
`;

export const Label = styled.div`
  width: 14%;
  height: 100%;
  padding: 3px 0;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  color: ${props => props.labelTextColor ? props.labelTextColor : labelTextColorDefault};
  background-color: ${props => props.labelBgColor ? props.labelBgColor : labelBgColorDefault};
  border: 2px solid ${props => props.labelBorderColor ? props.labelBorderColor : labelBorderColorDefault};
  border-radius: ${props => props.labelBorderRadius ? props.labelBorderRadius : labelBorderRadiusDefault}px;
  font-size: ${props => props.labelFontSize ? props.labelFontSize : labelFontSizeDefault}px;
  font-weight: ${props => props.fontWeight ? props.fontWeight : labelFontWeightDefault};
`;

