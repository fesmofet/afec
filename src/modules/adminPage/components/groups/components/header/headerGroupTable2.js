export default [
    {
        id: 'numberOfStudents',
        inputType: 'text',
        labelText: 'numberOfStudents',
        isDisabled: true,
        placeholder: 'search',
    },
    {
        id: 'attendanceCM',
        inputType: 'text',
        labelText: 'attendanceCM',
        isDisabled: true,
        placeholder: 'search',
    },
];
