import React from 'react';
import PropTypes from 'prop-types';
import {
    Label,
    HeaderItem,
    GroupTableHeader,
} from './styledComponent.js';
import InputBlock from '../../../../../../libs/InputBlock/InputBlock.jsx';
import headerGroupTable from './headerGroupTable.js';
import headerGroupTable2 from './headerGroupTable2.js';

const group = React.createRef();
const teacherLN = React.createRef();
const teacherFN = React.createRef();
const city = React.createRef();
const groupLevel = React.createRef();
const searchRefs = {
    group,
    teacherLN,
    teacherFN,
    city,
    groupLevel,
};

const Header = props => {
    const { dictionary, filterGroupsPage } = props;
    const handleOnChange = () => {
        const payload = {
            city: searchRefs.city.current.value,
            group: searchRefs.group.current.value,
            level: searchRefs.groupLevel.current.value,
            teacherFN: searchRefs.teacherFN.current.value,
            teacherLN: searchRefs.teacherLN.current.value,
        };
        filterGroupsPage(payload);
    };
    return (
        <GroupTableHeader>
            {headerGroupTable.map((item) =>
                <HeaderItem key={item.id}>
                    <InputBlock
                        refValue={searchRefs[item.labelText]}
                        id={item.id}
                        inputType={item.inputType}
                        labelText={dictionary.labels[item.labelText]}
                        isDisabled={item.isDisabled}
                        placeholder={dictionary.placeholders[item.placeholder]}
                        onChangeCallback={handleOnChange}
                    />
                </HeaderItem>
            )}
            {headerGroupTable2.map((item) =>
                <Label
                    key={item.id}
                    id={item.id}>
                    {dictionary.labels[item.labelText]}
                </Label>
            )}
        </GroupTableHeader>
    );
};

Header.propTypes = {
    dictionary: PropTypes.object.isRequired,
    filterGroupsPage: PropTypes.func,
};

export default React.memo(Header);
