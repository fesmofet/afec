import styled from 'styled-components';

export const GroupTableBody = styled.div`
  width: 100%;
  box-sizing: border-box;
`;

export const RowWrapper = styled.div`
  width: 100%;
  display: flex;
`;

export const BodyRow = styled.div`
  width: 1200px;
  height: 50px;
  display:flex;
  justify-content: center;
  align-items: center;
`;

export const RowItem = styled.div`
  width: 100%;
  height: 100%;
  display:flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  border: .5px solid black;
  background: #fff;
  
  &:nth-child(1){
    width: 3%;
  }
  
  &:nth-child(2){
    width: 14%;
  }
  
  &:nth-child(3){
    width: 14%;
  }
  
  &:nth-child(4){
    width: 14%;
  }
  
  &:nth-child(5){
    width: 14%;
  }
  &:nth-child(6){
    width: 14%;
  }
  &:nth-child(7){
    width: 14%;
  }
  &:nth-child(8){
    width: 14%;
  }
`;

export const ItemText = styled.span`
    font-size: 15px;
    width: 100%;
    text-align: center;
    text-overflow: ellipsis;
`;

export const BtnContainer = styled.div`
    width: 70px;
    display:flex;
    
`;

export const BtnPencil = styled.div`
  width: 30px;
  height: 30px;
  cursor: pointer;
  background-size: 30px;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('../../../../../../../img/pencil.svg');
  margin-right: 10px;
`;

export const BtnDelete = styled.div`
    width: 30px;
  height: 30px;
  cursor: pointer;
  background-size: 30px;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('../../../../../../../img/close.svg');
`;
