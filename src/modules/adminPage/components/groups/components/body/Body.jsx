import React from 'react';
import PropTypes from 'prop-types';
import {
    BodyRow,
    RowItem,
    ItemText,
    BtnPencil,
    BtnDelete,
    RowWrapper,
    BtnContainer,
    GroupTableBody,
} from './styledComponent.js';
import { Scrollbars } from 'react-custom-scrollbars';

const Body = props => {
    const { groups, deleteGroupAdmin, toggleModalWindow, deleteConfirm, getModalsData } = props;
    const handleOpenModalEditGroup = (item) => {
        toggleModalWindow({ modal: 'editGroup', isOpen: true, data: {
            groupId: item.id,
            groupName: item.group,
            groupCity: item.city,
            groupLevel: item.level,
        } });
    };

    const deleteHandler = (e) => {
        toggleModalWindow({ modal: 'deletingModal', isOpen: true, deleteID: +e.target.id });
    };
    if (deleteConfirm) {
        deleteGroupAdmin(getModalsData.deletingModal.deleteID);
    }
    return (
        <GroupTableBody>
            <Scrollbars style={{ height: '50vh', minHeight: '350px' }}>
                {groups.map((item, index) => !item.group ? null : <RowWrapper key={item.id}>
                    <BodyRow >
                        <RowItem>
                            <ItemText>{(index + 1)}</ItemText>
                        </RowItem>
                        <RowItem>
                            <ItemText accessKey={item.group}>{item.group}</ItemText>
                        </RowItem>
                        <RowItem>
                            <ItemText>{item.teacherLN}</ItemText>
                        </RowItem>
                        <RowItem>
                            <ItemText>{item.teacherFN}</ItemText>
                        </RowItem>
                        <RowItem>
                            <ItemText accessKey={item.city}>{item.city}</ItemText>
                        </RowItem>
                        <RowItem>
                            <ItemText accessKey={item.level}>{item.level}</ItemText>
                        </RowItem>
                        <RowItem>
                            <ItemText>{item.count || '-'}</ItemText>
                        </RowItem>
                        <RowItem>
                            <ItemText>{item.attendance}</ItemText>
                        </RowItem>
                    </BodyRow>
                    <BtnContainer>
                        <BtnPencil onClick={() => handleOpenModalEditGroup(item)}/>
                        <BtnDelete onClick={deleteHandler} id={item.id}/>
                    </BtnContainer>
                </RowWrapper>)}
            </Scrollbars>
        </GroupTableBody>
    );
};

Body.propTypes = {
    groups: PropTypes.array,
    deleteGroupAdmin: PropTypes.func,
    toggleModalWindow: PropTypes.func,
    dictionary: PropTypes.object,
    getModalsData: PropTypes.object,
    deleteConfirm: PropTypes.bool,
};

export default React.memo(Body);
