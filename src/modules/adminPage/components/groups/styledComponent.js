import styled from 'styled-components';

export const GroupWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const GroupTableWrapper = styled.div`
  min-width: 1300px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

export const GroupTableTitle = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 35px 0 20px;
`;

export const GroupTableText = styled.span`
  font-size: 25px;
  font-weight: bold;
`;

export const Table = styled.div`
  border: .5px solid black;
  box-sizing: border-box;
`;

export const ButtonsContainer = styled.div `
   display: flex;
   flex-direction: row;
`;

export const ButtonContainer = styled.div `
   display: flex;
   flex-direction: row;
   width: 140px;
   height: 50px;
`;
export const ControlWrapper = styled.div`
   width: 100%;   
   height: 50px;
   display: flex;
   flex-direction: row;
   justify-content: space-between;
   align-items: center;
   padding-top: 10px;
   margin-bottom: 10px
`;
