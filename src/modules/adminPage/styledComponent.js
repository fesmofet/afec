import styled from 'styled-components';

export const defaultStyle = {
    mainBgColor: '#a0d8ef',
};

export const AdminPageWrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-between;
  background-color: ${props => props.theme.mainBgColor ? props.theme.mainBgColor : defaultStyle.mainBgColor};
`;

export const ControlPanel = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  width: 100%;
  height: 50px;
  margin-bottom: 20px;
  box-sizing: border-box;
  border: 1px solid black;
`;

export const AdminPageContainer = styled.div`
  width: 95%;
`;
