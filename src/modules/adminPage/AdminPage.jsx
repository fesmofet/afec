import React from 'react';
import { ThemeProvider } from 'styled-components';
import PropTypes from 'prop-types';
import {
    AdminPageWrapper,
    AdminPageContainer,
} from './styledComponent.js';
import NavBar from '../../libs/navBar/navBar.jsx';
import ToolBar from '../../libs/toolBar/ToolBar.jsx';
import UserTable from './components/users/Users.jsx';
import Salary from './components/salary/Salary.jsx';
import Groups from './components/groups/Groups.jsx';
import navBarItems from '../../constants/navBarItems';
import Schedule from './components/schedule/Schedule.jsx';
import moment from 'moment';

export default class AdminPage extends React.Component {
    constructor(props) {
        super(props);
        props.getAllUsers(props.authData.userData.id);
        props.getAllGroups();
        props.getGroups();
        props.getAllTeachers();
        const user = JSON.parse(localStorage.getItem('currentUserData'));
        const payload1 = {
                from: moment.utc().startOf('month').valueOf(),
                to: moment.utc().endOf('day').valueOf(),
        };
        props.getTeachersSalary(payload1);
        const payload2 = {
            to: moment.utc().endOf('isoWeek').add(1, 'hour').valueOf(),
            from: moment.utc().startOf('isoWeek').valueOf(),
            groups: user.groups,
            id: user.id,
        };
        props.getSchedule(payload2);
    }
    renderModeSwitch(props) {
        switch (props.currentModule) {
            case 'users':
                return <UserTable
                    users={props.users}
                    groups={props.groups}
                    dictionary={props.dictionary}
                    changeRole={props.changeRole}
                    authData={props.authData}
                    deleteUser={props.deleteUser}
                    updateUser={props.updateUser}
                    searchUser={props.searchUser}
                    toggleModalWindow={props.toggleModalWindow}
                    deleteConfirm={props.deleteConfirm}
                    getModalsData={props.getModalsData}/>;
            case 'groups':
                return <Groups
                    dictionary={props.dictionary}
                    theme={props.theme}
                    getAdminGroupsData={props.getAdminGroupsData}
                    filterGroupsPage={props.filterGroupsPage}
                    toggleModalWindow={props.toggleModalWindow}
                    deleteGroupAdmin={props.deleteGroupAdmin}
                    deleteConfirm={props.deleteConfirm}
                    getModalsData={props.getModalsData}/>;
            case 'schedule':
                return <Schedule
                    theme = {props.theme}
                    filterLessons ={props.filterSchedule}
                    lessons={props.lessonsData}
                    dictionary={props.dictionary}
                    getSchedule={props.getSchedule}
                    currentDate ={props.currentDate}
                    toggleModalWindow={props.toggleModalWindow}
                    />;
            case 'salary':
                return <Salary
                    theme = {props.theme}
                    getTeachersSalary ={props.getTeachersSalary}
                    filterTeachers = {props.filterTeachers}
                    teachers={props.teachersSalaryData}
                    dictionary={props.dictionary}
                    changeRole={props.changeRole}
                    authData={props.authData}/>;
            default:
                return <UserTable
                    users={props.users}
                    groups={props.groups}
                    dictionary={props.dictionary}
                    changeRole={props.changeRole}
                    authData={props.authData}
                    deleteUser={props.deleteUser}
                    updateUser={props.updateUser}
                    searchUser={props.searchUser}/>;
        }
    }
    render() {
        const { dictionary, theme, history, logout, switchAccount, toggleFullScreen, changeModule } = this.props;
        return (
            <ThemeProvider theme={theme}>
                <AdminPageWrapper>
                    <AdminPageContainer>
                        <ToolBar
                            history={history}
                            logout={logout}
                            fullScreen={toggleFullScreen}
                            switchAccount={switchAccount}
                        />
                        <NavBar
                            dictionary={dictionary}
                            changeModule={changeModule}
                            navBarItems={navBarItems}
                        />
                        {this.renderModeSwitch(this.props)}
                    </AdminPageContainer>
                </AdminPageWrapper>
            </ThemeProvider>
        );
    }
}

AdminPage.propTypes = {
    dictionary: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired,
    toggleFullScreen: PropTypes.func.isRequired,
    users: PropTypes.array.isRequired,
    teachers: PropTypes.array,
    changeModule: PropTypes.func.isRequired,
    getAllUsers: PropTypes.func.isRequired,
    getAllGroups: PropTypes.func.isRequired,
    getTeachersSalary: PropTypes.func.isRequired,
    filterTeachers: PropTypes.func.isRequired,
    authData: PropTypes.object.isRequired,
    getAllTeachers: PropTypes.func.isRequired,
    searchUser: PropTypes.func.isRequired,
    getGroups: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired,
    updateUser: PropTypes.func.isRequired,
    groups: PropTypes.array.isRequired,
    getSchedule: PropTypes.func.isRequired,
};
