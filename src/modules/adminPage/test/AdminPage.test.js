import React from 'react';
import { AdminPageWrapper, defaultStyle } from '../styledComponent';
import NavBar from '../components/navBar/navBar';
import UserTable from '../components/users/Users';
import ToolBar from '../../../libs/toolBar/ToolBar';
import en from '../../../managers/localManager/translates/en';
import oceanTheme from '../../../managers/themeManager/theme/oceanTheme';
import * as actions from '../../../constants/actions';

describe('AdminPage components test: NavBar, UserTable, ToolBar  react component', () => {
    const props = {
        dictionary: en,
        theme: oceanTheme,
        history: [],
        changeModule: actions.changeModule(),
    };

    it('ToolBar snapshot created, should rendered correctly with props', done => {
        const wrapper = shallow(<ToolBar {...props}/>);
        expect(wrapper).matchSnapshot();
        done();
    });

    it('NavBar snapshot created, should rendered correctly with props', done => {
        const wrapper = shallow(<NavBar {...props}/>);
        expect(wrapper).matchSnapshot();
        done();
    });

    it('UserTable snapshot created, should rendered correctly with props', done => {
        const wrapper = shallow(<UserTable {...props}/>);
        expect(wrapper).matchSnapshot();
        done();
    });
});

describe('AdminPageWrapper styled components:', () => {
    const props = {
        mainBgColor: '#a0d8ef',
    };

    it('AdminPageWrapper styled component must have styles from props', () => {
        const component = getRenderedSC(<AdminPageWrapper {...props}/>);
        expect(component).toHaveStyleRule('background-color', props.mainBgColor);
    });

    it('AdminPageWrapper styled component must have default styles', () => {
        const component = getRenderedSC(<AdminPageWrapper/>);
        expect(component).toHaveStyleRule('background-color', defaultStyle.mainBgColor);
    });
});
