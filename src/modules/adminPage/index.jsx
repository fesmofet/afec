import { connect } from 'react-redux';
import AdminPage from './AdminPage.jsx';
import * as selectors from '../../constants/selectors';
import * as actions from '../../constants/actions';

const mapStateToProps = state => ({
    users: selectors.getAllUsers(state),
    getAdminGroupsData: selectors.getAdminGroupsData(state),
    teachers: selectors.getTeachersSalary(state),
    teachersSalaryData: selectors.getTeachersSalary(state),
    lessonsData: selectors.getLessonsData(state),
    theme: selectors.getAdminPageTheme(state),
    dictionary: selectors.getDictionary(state),
    currentModule: selectors.getCurrentModule(state),
    authData: selectors.getAuthorizationData(state),
    groups: selectors.getGroups(state),
    deleteConfirm: selectors.deleteConfirm(state),
    getModalsData: selectors.getModalsData(state),
    currentDate: selectors.getCurrentDate(state),
});

const mapDispatchToProps = dispatch => ({
    logout: history => dispatch(actions.logout(history)),
    toggleFullScreen: () => dispatch(actions.toggleFullScreen()),
    changeRole: payload => dispatch(actions.changeRole(payload)),
    getAllUsers: payload => dispatch(actions.getAllUsers(payload)),
    getAllGroups: () => dispatch(actions.getAllGroups()),
    getTeachersSalary: (payload) => dispatch(actions.getTeachersSalary(payload)),
    changeModule: payload => dispatch(actions.changeModule(payload)),
    filterTeachers: payload => dispatch(actions.filterTeachers(payload)),
    toggleModalWindow: payload => dispatch(actions.toggleModalWindow(payload)),
    getAllTeachers: () => dispatch(actions.getAllTeachers()),
    filterGroupsPage: payload => dispatch(actions.filterGroupsPage(payload)),
    deleteUser: payload => dispatch(actions.deleteUser(payload)),
    deleteGroupAdmin: payload => dispatch(actions.deleteGroupAdmin(payload)),
    updateUser: payload => dispatch(actions.updateUser(payload)),
    searchUser: payload => dispatch(actions.searchUser(payload)),
    getGroups: () => dispatch(actions.getGroups()),
    getSchedule: payload => dispatch(actions.getSchedule(payload)),
    filterSchedule: payload => dispatch(actions.filterSchedule(payload)),
    switchAccount: () => dispatch(actions.switchAccount()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminPage);
