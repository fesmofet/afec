import styled from 'styled-components';

export const defaultStyle = {
   formBgColor: '#76a8cb',
};

export const AuthFormWrapper = styled.div`
   display: flex;
   flex-direction: column;
   align-items: center;
   width: 400px;
   background-color: ${props => props.theme.formBgColor ? props.theme.formBgColor : defaultStyle.formBgColor};
   border: 5px groove #76a8cb;
   border-radius: 10px;
`;

export const Content = styled.div `
   width: 90%;
   height: 50%;
   display: flex;
   flex-direction: column;
   align-items: center;
`;

export const FieldWrapper = styled.div `
   width: 100%;
   height: 60px;
   display: flex;
   flex-direction: column;
   justify-content: center;
   margin: 5px 0px 5px;
`;

export const InputBlockContainer = styled.div `
   width: 100%;
   height: 50px;
`;

export const ErrorBlockContainer = styled.div `
   width: 100%;
   height: 22px;
   font-size: 11px;
`;

export const ControlPanel = styled.div `
   width: 100%;   
   height: 50%;
   display: flex;
   flex-direction: column;
   justify-content: center;
   align-items: center;
   padding-top: 10px;
   margin-bottom: 10px;
`;

export const ButtonContainer = styled.div `
   width: 200px;
   height: 50px;
`;

export const LinkContainer = styled.div `
   width: 200px;
   height: 30px;
   display: flex;
   flex-direction: column;
   justify-content: center;
   align-items: center;
`;

export const LinksContainer = styled.div `
  
   height: 30px;
   display: flex;
   justify-content: space-between;
   align-items: center;
   margin-bottom: 5px;
`;

export const TitleContainer = styled.div`
      margin: 20px 0;
`;

export const TitleText = styled.span`
      font-size: 30px;
      font-weight: bold;
      color: #236ea1;
`;
