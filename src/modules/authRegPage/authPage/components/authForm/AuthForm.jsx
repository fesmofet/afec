import React from 'react';
import PropTypes from 'prop-types';
import {
    TitleText,
    TitleContainer,
    AuthFormWrapper,
    Content,
    ControlPanel,
    FieldWrapper,
    InputBlockContainer,
    ErrorBlockContainer,
    ButtonContainer,
    LinkContainer,
    LinksContainer,
} from './styledComponent.js';
import InputBlock from '../../../../../libs/InputBlock/InputBlock.jsx';
import ErrorBlock from '../../../../../libs/errorBlock/ErrorBlock.jsx';
import CustomLink from '../../../../../libs/customLink/customLink.jsx';
import CustomButton from '../../../../../libs/customButton/CustomButton.jsx';
import config from '../../../../../constants/config.js';

const email = React.createRef();
const password = React.createRef();

const authRefs = {
    email, password,
};

const AuthForm = (props) => {
    const handelAuthorization = () => {
        const payload = {
            email: authRefs.email.current.value,
            password: authRefs.password.current.value,
        };
        props.callback(payload, props.history);
    };

    const handelRegistration = () => {
        props.history.push('/registration');
    };

    const handelForgotPassword = () => {
        props.modal({ modal: 'passwordRecovery', isOpen: true });
    };
    const { dictionary, theme, data } = props;

    return (
        <AuthFormWrapper>
            <TitleContainer>
                <TitleText>{dictionary.resources.title}</TitleText>
            </TitleContainer>
            <Content>
                {config.authInputs.map(input =>
                    <FieldWrapper key={input.id}>
                        <InputBlockContainer>
                            <InputBlock
                                id={input.id}
                                labelText={dictionary.labels[input.labelsKey]}
                                labelTextColor={theme.labelTextColor}
                                labelBgColor={theme.labelBgColor}
                                labelBorderColor={theme.labelBorderColor}
                                labelFontSize={input.labelFontSize}
                                labelfontWeight={input.labelFontWeight}
                                labelBorderRadius={input.labelBorderRadius}
                                inputType={input.inputType}
                                placeholder={dictionary.placeholders[input.placeholderKey]}
                                inputTextColor={theme.inputTextColor}
                                inputBgColor={theme.inputBgColor}
                                inputFontSize={input.inputFontSize}
                                dataAttribute={input.dataAttribute}
                                inputBlockBgColor={theme.inputBlockBgColor}
                                refValue={authRefs[input.refValue]}
                            />
                        </InputBlockContainer>
                        {data.invalidFields.indexOf(input.id) !== -1 ?
                            <ErrorBlockContainer>
                                <ErrorBlock
                                    errorMessage={dictionary.errorMessageAuth[input.errorKey]}
                                    textColor={'red'}
                                />
                            </ErrorBlockContainer>
                            : ''}
                    </FieldWrapper>
                )}
                {data.authError ? 
                    <ErrorBlockContainer>
                    <ErrorBlock 
                        errorMessage={dictionary.errorMessageAuth.wrongEmail}
                        textColor={'red'}
                    />
                    </ErrorBlockContainer>
                    : null
                } 
            </Content>
            <ControlPanel>
                <ButtonContainer>
                    <CustomButton
                        fontSize={16}
                        boxShadow={'none'}
                        textColor={theme.textColor}
                        fontWeight={'bold'}
                        isDisabled={false}
                        buttonName={dictionary.button.authorization}
                        borderRadius={10}
                        dataAttribute={'data-button-registration'}
                        textTransform={'uppercase'}
                        backgroundColor={theme.btnBgColor}
                        onClickCallback={handelAuthorization}
                        backgroundColorHover={theme.backgroundColorHover}
                    />
                </ButtonContainer>
                <LinksContainer>
                    <LinkContainer>
                        <CustomLink
                            fontSize={12}
                            fontWeight={'normal'}
                            hoverColor={theme.hoverColor}
                            activeColor={theme.activeColor}
                            textTransform={'none'}
                            textDecoration={'underline'}
                            text={'Registration'}
                            onClick={handelRegistration}
                        />
                    </LinkContainer>
                    <LinkContainer>
                        <CustomLink
                            fontSize={12}
                            fontWeight={'normal'}
                            hoverColor={theme.hoverColor}
                            activeColor={theme.activeColor}
                            textTransform={'none'}
                            textDecoration={'underline'}
                            text={'Forgot password'}
                            onClick={handelForgotPassword}
                        />
                    </LinkContainer>
                </LinksContainer>
            </ControlPanel>
        </AuthFormWrapper>
    );
};

AuthForm.propTypes = {
    dictionary: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    callback: PropTypes.func,
    data: PropTypes.object.isRequired,
    modal: PropTypes.func,
};

export default React.memo(AuthForm);
