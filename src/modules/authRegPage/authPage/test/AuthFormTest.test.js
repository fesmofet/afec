import React from 'react';
import { AuthFormWrapper, defaultStyle } from '../components/authForm/StyledComponent.js';
import AuthFormMemo from '../components/authForm/AuthForm.jsx';
import en from '../../../../managers/localManager/translates/en.js';
import oceanTheme from '../../../../managers/themeManager/theme/oceanTheme.js';

describe('AuthForm react component', () => {
const props = {
    dictionary: en,
    theme: oceanTheme,
    data: {
        inputs: {
            email: '',
            password: '',
        },
        invalidFields: [],
    },
};
    it('snapshot created, should rendered correctly with props', done => {
        const wrapper = shallow(<AuthFormMemo {...props}/>);
        expect(wrapper).matchSnapshot();
        done();
    });
});

describe('AuthFormWrapper styled components:', () => {
    const props = {
        formBgColor: '#76a8cb',
    };

    it('AuthFormWrapper styled component must have styles from props', () => {
        const component = getRenderedSC(<AuthFormWrapper {...props}/>);
        expect(component).toHaveStyleRule('background-color', props.formBgColor);
    });

    it('AuthFormWrapper styled component must have default styles', () => {
        const component = getRenderedSC(<AuthFormWrapper />);
        expect(component).toHaveStyleRule('background-color', defaultStyle.formBgColor);
    });
});
