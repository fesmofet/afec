import { connect } from 'react-redux';
import AuthPage from './AuthPage.jsx';
import * as actions from '../../../constants/actions';
import * as selectors from '../../../constants/selectors';

const mapStateToProps = state => ({
    theme: selectors.getAuthPageTheme(state),
    dictionary: selectors.getDictionary(state),
    authorizationData: selectors.getAuthorizationData(state),
});

const mapDispatchToProps = dispatch => ({
    changeLocale: payload => dispatch(actions.changeLocale(payload)),
    login: (payload, history) => dispatch(actions.login(payload, history)),
    toggleModalWindow: payload => dispatch(actions.toggleModalWindow(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthPage);
