import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';
import { AuthPageWrapper } from './styledComponent.js';
import AuthForm from '../authPage/components/authForm/AuthForm.jsx';

export default class AuthPage extends React.Component {
    render() {
        return (
            <ThemeProvider theme={this.props.theme}>
                <AuthPageWrapper>
                    <AuthForm 
                        theme={this.props.theme}
                        callback={this.props.login}
                        history={this.props.history}
                        dictionary={this.props.dictionary}
                        data={this.props.authorizationData}
                        modal={this.props.toggleModalWindow}
                        />             
                </AuthPageWrapper>
            </ThemeProvider>
        );
    }
}

AuthPage.propTypes = {
    login: PropTypes.func,
    authorization: PropTypes.func,
    toggleModalWindow: PropTypes.func,
    theme: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,
    authorizationData: PropTypes.object.isRequired,
};
