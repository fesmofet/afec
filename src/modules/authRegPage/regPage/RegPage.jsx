import React from 'react';
import RegForm from '../regPage/components/regForm/RegForm.jsx';
import { RegPageWrapper } from './styledComponent.js';
import { ThemeProvider } from 'styled-components';
import PropTypes from 'prop-types';

export default class RegPage extends React.Component {
    render() {
        return (
            <ThemeProvider theme={this.props.theme}>
                <RegPageWrapper>
                    <RegForm data={this.props.registrationData}
                             theme={this.props.theme}
                             history={this.props.history}
                             callback={this.props.registration}
                             dictionary={this.props.dictionary}/>
                </RegPageWrapper>
            </ThemeProvider>
        );
    }
}

RegPage.propTypes = {
    dictionary: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    registration: PropTypes.func,
    registrationData: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired, 
};