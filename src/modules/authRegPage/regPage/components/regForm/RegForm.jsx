import React from 'react';
import PropTypes from 'prop-types';
import {
    TitlText,
    TitleContainer,
    RegFormWrapper,
    Content,
    ControllPanel,
    FieldWrapper,
    InputBlockContainer,
    ErrorBlockContainer,
    ButtonContainer,
    LinkContainer,
} from './styledComponent.js';
import InputBlock from '../../../../../libs/InputBlock/InputBlock.jsx';
import ErrorBlock from '../../../../../libs/errorBlock/ErrorBlock.jsx';
import CustomLink from '../../../../../libs/customLink/customLink.jsx';
import CustomButton from '../../../../../libs/customButton/CustomButton.jsx';
import config from '../../../../../constants/config.js';

const email = React.createRef();
const phone = React.createRef();
const keyword = React.createRef();
const firstName = React.createRef();
const lastName = React.createRef();
const password = React.createRef();
const birthday = React.createRef();
const passwordAgain = React.createRef();

const regRefs = {
    email, phone, keyword, lastName, firstName, password, birthday, passwordAgain,
};

const RegForm = (props) => {
    const handelRegistration = () => {
        const payload = {
            email: regRefs.email.current.value,
            phone: regRefs.phone.current.value,
            keyword: regRefs.keyword.current.value,
            lastName: regRefs.lastName.current.value,
            firstName: regRefs.firstName.current.value,
            password: regRefs.password.current.value,
            birthday: regRefs.birthday.current.value,
            passwordAgain: regRefs.passwordAgain.current.value,
        };
        props.callback(payload, props.history);
    };

    const handelAuthorization = () => {
        props.history.push('/');
    };

    const { dictionary, theme, data } = props;

    return (
        <RegFormWrapper>
            <TitleContainer>
                    <TitlText>Registration</TitlText>
                </TitleContainer>
            <Content>
                {config.inputs.map(input =>
                    <FieldWrapper key={input.id}>
                        <InputBlockContainer>
                            <InputBlock
                                id={input.id}
                                labelText={dictionary.labels[input.labelsKey]}
                                labelTextColor={theme.labelTextColor}
                                labelBgColor={theme.labelBgColor}
                                labelBorderColor={theme.labelBorderColor}
                                labelFontSize={input.labelFontSize}
                                labelfontWeight={input.labelfontWeight}
                                labelBorderRadius={input.labelBorderRadius}
                                inputType={input.inputType}
                                placeholder={dictionary.placeholders[input.placeholderKey]}
                                inputTextColor={theme.inputTextColor}
                                inputBgColor={theme.inputBgColor}
                                inputFontSize={input.inputFontSize}
                                dataAttribute={input.dataAttribute}
                                inputBlockBgColor={theme.inputBlockBgColor}
                                refValue={regRefs[input.refValue]}
                            />
                        </InputBlockContainer>
                        {data.invalidFields.indexOf(input.id) !== -1 ?
                            <ErrorBlockContainer>
                                <ErrorBlock
                                    errorMessage={dictionary.errorMessage[input.errorKey]}
                                    textColor={'red'}
                                />
                            </ErrorBlockContainer>
                            : ''}
                        {data.isLoginExist && input.id === 'email' ?
                            <ErrorBlockContainer>
                                <ErrorBlock
                                    errorMessage={dictionary.errorMessage.emailAlreadyExist}
                                    textColor={'red'}
                                />
                            </ErrorBlockContainer>
                            : ''}
                    </FieldWrapper>
                )}
            </Content>
            <ControllPanel>
                <ButtonContainer>
                    <CustomButton
                        fontSize={16}
                        boxShadow={'none'}
                        textColor={theme.textColor}
                        fontWeight={'bold'}
                        isDisabled={false}
                        buttonName={dictionary.button.registration}
                        borderRadius={10}
                        dataAttribute={'data-button-registration'}
                        textTransform={'uppercase'}
                        backgroundColor={theme.btnBgColor}
                        onClickCallback={handelRegistration}
                        backgroundColorHover={theme.backgroundColorHover}
                    />
                </ButtonContainer>
                <LinkContainer>
                    <CustomLink
                        fontSize={12}
                        fontWeight={'normal'}
                        hoverColor={theme.hoverColor}
                        activeColor={theme.activeColor}
                        textTransform={'none'}
                        textDecoration={'underline'}
                        text={'Authorization'}
                        onClick={handelAuthorization}
                    />
                </LinkContainer>
            </ControllPanel>
        </RegFormWrapper>
    );
};

RegForm.propTypes = {
    dictionary: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    callback: PropTypes.func,
    data: PropTypes.object.isRequired,
};

export default React.memo(RegForm);
