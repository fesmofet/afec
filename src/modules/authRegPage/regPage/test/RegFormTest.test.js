import React from 'react';
import { RegFormWrapper, defaultStyle } from '../components/regForm/StyledComponent.js';
import RegFormMemo from '../components/regForm/RegForm.jsx';
import en from '../../../../managers/localManager/translates/en.js';
import oceanTheme from '../../../../managers/themeManager/theme/oceanTheme.js';

describe('RegForm react component', () => {
const props = { 
    dictionary: en,
    theme: oceanTheme,
    data: {
        inputs: {
            email: '',
            phone: '',
            keyword: '',
            firstName: '',
            lastName: '',
            password: '',
            birthday: '',
            passwordAgain: '',
        },
        isLoginExist: false,
        invalidFields: [],
    },
};
    it('snapshot created, should rendered correctly with props', done => {
        const wrapper = shallow(<RegFormMemo {...props}/>);
        expect(wrapper).matchSnapshot();
        done();
    });
});

describe('RegFormWrapper styled components:', () => {
    const props = {
        formBgColor: '#76a8cb',
    };

    it('RegFormWrapper styled component must have styles from props', () => {
        const component = getRenderedSC(<RegFormWrapper {...props}/>);
        expect(component).toHaveStyleRule('background-color', props.formBgColor);
    });

    it('RegFormWrapper styled component must have default styles', () => {
        const component = getRenderedSC(<RegFormWrapper />);
        expect(component).toHaveStyleRule('background-color', defaultStyle.formBgColor);
    });
});
