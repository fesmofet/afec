import { connect } from 'react-redux';
import RegPage from './RegPage.jsx';
import * as selectors from '../../../constants/selectors';
import * as actions from '../../../constants/actions';

const mapStateToProps = state => ({
    theme: selectors.getRegPageTheme(state),
    dictionary: selectors.getDictionary(state),
    registrationData: selectors.getRegistrationData(state),
});

const mapDispatchToProps = dispatch => ({
    changeLocale: payload => dispatch(actions.changeLocale(payload)),
    registration: (payload, history) => dispatch(actions.registration(payload, history)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RegPage);
