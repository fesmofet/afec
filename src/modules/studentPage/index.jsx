import { connect } from 'react-redux';
import StudentPage from './StudentPage.jsx';
import * as selectors from '../../constants/selectors';
import * as actions from '../../constants/actions';

const mapStateToProps = state => ({
    theme: selectors.getAdminPageTheme(state),
    dictionary: selectors.getDictionary(state),
    lessons: selectors.getLessonsData(state),
    isAccount: selectors.checkIsAccount(state),
    currentDate: selectors.getCurrentDate(state),
    authData: selectors.getAuthorizationData(state),
    studentData: selectors.getStudentData(state),
});

const mapDispatchToProps = dispatch => ({
    toggleAccountStudent: () => dispatch(actions.toggleAccountStudent()),
    logout: history => dispatch(actions.logout(history)),
    switchAccount: () => dispatch(actions.switchAccount()),
    toggleFullScreen: () => dispatch(actions.toggleFullScreen()),
    getSchedule: payload => dispatch(actions.getSchedule(payload)),
    getAccountTableData: payload => dispatch(actions.getAccountTableData(payload)),
    requestEditAccountInfo: payload => dispatch(actions.requestEditAccountInfo(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(StudentPage);
