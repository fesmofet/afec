import React from 'react';
import PropTypes from 'prop-types';
import {
    UserTableBody,
    BodyRow,
    RowItem,
    CellItem,
    FirstCellItem,
} from './styledComponent.js';
import {Scrollbars} from 'react-custom-scrollbars';

const Body = (props) => {
    const {lessons} = props;
    return (
        <UserTableBody>
            <Scrollbars style={{height: '55vh', minHeight: '350px'}}>
                {Object.values(lessons) && Object.values(lessons).length ? lessons.map((lesson, rowIndex) => {
                    return <BodyRow key={rowIndex}>
                        {lesson.groups.length !== 0 ?
                            <>
                                <RowItem>
                                    <FirstCellItem myheight={lesson.groups.length}>
                                        {lesson.date}
                                    </FirstCellItem>
                                </RowItem>
                                <RowItem>
                                    {lesson.groups.map((group, index) => {
                                        return <CellItem key={index}>{group}</CellItem>;
                                    })}
                                </RowItem>
                                <RowItem>
                                    {lesson.teachers.map((teacher, index) => {
                                        return <CellItem key={index}>{teacher}</CellItem>;
                                    })}
                                </RowItem>
                                <RowItem>
                                    {lesson.time.map((time, index) => {
                                        return <CellItem key={index}>{time}</CellItem>;
                                    })}
                                </RowItem>
                                <RowItem>
                                    {lesson.type.map((type, index) => {
                                        return <CellItem key={index}>{type}</CellItem>;
                                    })}
                                </RowItem>
                            </>
                            : null}
                    </BodyRow>;
                }) : null}
            </Scrollbars>
        </UserTableBody>
    );
};

Body.propTypes = {
    dictionary: PropTypes.object.isRequired,
    lessons: PropTypes.array.isRequired,
};

export default React.memo(Body);

