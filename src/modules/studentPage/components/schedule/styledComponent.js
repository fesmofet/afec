import styled from 'styled-components';

export const ScheduleTableWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: flex-start;
`;

export const ScheduleTableTitle = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 35px 0 20px;
`;

export const ScheduleTableText = styled.span`
  font-size: 25px;
  font-weight: bold;
`;

export const Table = styled.div`
  width: 100%;  
  box-sizing: border-box;
`;

export const ControlPanel = styled.div `
   width: 100%;   
   height: 50px;
   display: flex;
   flex-direction: row;
   justify-content: flex-end;
   align-items: center;
   padding-top: 10px;
   margin-bottom: 10px;
`;

export const WeekNavigator = styled.div `
   display: flex;
   flex-direction: row;
`;

export const ButtonsContainer = styled.div `
   display: flex;
   flex-direction: row;
`;

export const ButtonContainer = styled.div `
   display: flex;
   flex-direction: row;
   width: 140px;
   height: 50px;
`;

export const InputBlockContainer = styled.div `
  display: flex;
  flex-direction: row;
   width: 40%;
   height: 50px;
`;
