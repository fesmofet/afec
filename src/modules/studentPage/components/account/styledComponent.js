import styled from 'styled-components';

export const defaultStyle = {
    formBgColor: '#76a8cb',
 };

export const AccountPageBody = styled.div`
    width: 100%;
    height: 680px;
    display: flex;
    justify-content: center;
`;

export const AccountPanelWrapper = styled.div`
    width: 35%;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
`;

export const AccountTableWrapper = styled.div`
    width: 60%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    margin-right: 20px;
`;

export const Content = styled.div `
   width: 90%;
   height: 50%;
   display: flex;
   flex-direction: column;
   align-items: center;
`;

export const FieldWrapper = styled.div `
   width: 100%;
   height: 60px;
   display: flex;
   flex-direction: column;
   justify-content: center;
   margin: 5px 0px 5px;
`;

export const InputBlockContainer = styled.div `
   width: 100%;
   height: 50px;
`;

export const ErrorBlockContainer = styled.div `
   width: 100%;
   height: 22px;
   font-size: 11px;
`;

export const ControlPanel = styled.div `
   width: 100%;   
   display: flex;
   flex-direction: column;
   justify-content: center;
   align-items: center;
   padding-top: 10px;
   margin-bottom: 10px;
`;

export const ButtonContainer = styled.div `
   width: 200px;
   height: 50px;
`;

export const Span = styled.span`
   text-align: center;
`;
