import styled from 'styled-components';

let defaultStyles = {
    bgTableItem: '#fff',
};

export const StudentsTableBody = styled.div`
  width: 100%;
  box-sizing: border-box;
`;

export const Row = styled.div`
  width: 100%;
  display:flex;
  justify-content: center;
  align-items: center;
  font-weight: bold;
`;

export const RowItem = styled.div`
  width: calc(100%/3);
  box-sizing: border-box;
  border: 2px groove black;
  min-height: 60px;
  display:flex;
  justify-content: center;
  align-items: center;
  background: ${props => props.theme.bgTableItem ? props.theme.bgTableItem : defaultStyles.bgTableItem};
`;

