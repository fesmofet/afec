import React from 'react';
import PropTypes from 'prop-types';
import {
    StudentsTableBody,
    Row,
    RowItem,
} from './styledComponent.js';

const Body = (props) => {
    const { studentData } = props;
    const headerItems = [studentData.student_attendance, studentData.amount_of_lessons, `${(studentData.attendence * 100).toFixed(2)}%`];
    return (
        <StudentsTableBody>
            <Row>
                {headerItems.map((item, index) => <RowItem key={index}>{item}</RowItem>)}
            </Row>
        </StudentsTableBody>
    );
};

Body.propTypes = {
    studentData: PropTypes.object.isRequired,
};

export default React.memo(Body);

