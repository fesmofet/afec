import React from 'react';
import PropTypes from 'prop-types';
import {
    UserTableHeader,
    HeaderItem,
} from './styledComponent.js';

const Header = (props) => {
    const { dictionary } = props;
    const arrayAccountHeader = ['present', 'lessonsInMonth', 'percent'];

    return (
        <UserTableHeader>
            {arrayAccountHeader.map((item, index) =>
                <HeaderItem key={index}>{dictionary.labels[item]}</HeaderItem>
            )}
        </UserTableHeader>
    );
};

Header.propTypes = {
    dictionary: PropTypes.object.isRequired,
};

export default React.memo(Header);
