import styled from 'styled-components';

export const UserTableHeader = styled.div`
  width: 100%;
  display:flex;
  justify-content: space-between;
  align-items: center;
  background-color: #3683ff;
  color: #fff;
`;

export const HeaderItem = styled.div`
  height: 100%;
  box-sizing: border-box;
  border: 2px groove black;
  min-height: 60px;
  display:flex;
  justify-content: center;
  align-items: center;
  
  &:nth-child(1){
    width: calc(100%/3);
  }
  
  &:nth-child(2){
    width: calc(100%/3);
  }
  
  &:nth-child(3){
    width: calc(100%/3);
  }
`;

