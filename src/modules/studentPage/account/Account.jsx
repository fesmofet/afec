import React from 'react';
import PropTypes from 'prop-types';
import {
    AccountPageBody,
    AccountPanelWrapper,
    AccountTableWrapper,
    Content,
    ControlPanel,
    FieldWrapper,
    ButtonContainer,
    InputBlockContainer,
} from './styledComponent.js';
import InputBlock from '../../../libs/InputBlock/InputBlock.jsx';
import CustomButton from '../../../libs/customButton/CustomButton.jsx';
import Header from './components/header/Header.jsx';
import Body from './components/body/Body.jsx';
import config from './config/config.js';
import moment from 'moment';

const lastName = React.createRef();
const firstName = React.createRef();
const password = React.createRef();
const passwordAgain = React.createRef();
const dateOfBirth = React.createRef();

const refsAccountInfo = { lastName, firstName, dateOfBirth, password, passwordAgain };

export default class Account extends React.Component {
    constructor(props) {
        super(props);
        let payload1 = props.authData.userData.id;
        props.getAccountTableData(payload1);
    }

    render() {
        const { theme, dictionary, requestEditAccountInfo, authData, studentData } = this.props;
        const handleRequest = () => {
            let id = authData.userData.id;
            const payload = {
                studentId: id,
                lastName: refsAccountInfo.lastName.current.value,
                firstName: refsAccountInfo.firstName.current.value,
                dateOfBirth: +(moment(refsAccountInfo.dateOfBirth.current.value).format('X') * 1000),
                password: refsAccountInfo.password.current.value,
                passwordAgain: refsAccountInfo.passwordAgain.current.value,
            };
           requestEditAccountInfo(payload);
        };

        let defaultValueInputs = {
            lastName: authData.userData.lastName,
            firstName: authData.userData.firstName,
            birthday: moment(parseInt(authData.userData.birthday)).format('YYYY-MM-DD'),
        };

        return (
                <AccountPageBody>
                    <AccountPanelWrapper>
                        <Content>
                            {config.map(input =>
                                <FieldWrapper key={input.id}>
                                    <InputBlockContainer>
                                        <InputBlock
                                            id={input.id}
                                            labelText={dictionary.labels[input.labelsKey]}
                                            labelTextColor={theme.labelTextColor}
                                            labelBgColor={theme.labelBgColor}
                                            labelBorderColor={theme.labelBorderColor}
                                            labelFontSize={input.labelFontSize}
                                            labelfontWeight={input.labelFontWeight}
                                            labelBorderRadius={input.labelBorderRadius}
                                            inputType={input.inputType}
                                            placeholder={dictionary.placeholders[input.placeholderKey]}
                                            inputTextColor={theme.inputTextColor}
                                            inputBgColor={theme.inputBgColor}
                                            inputFontSize={input.inputFontSize}
                                            dataAttribute={input.dataAttribute}
                                            inputBlockBgColor={theme.inputBlockBgColor}
                                            refValue={refsAccountInfo[input.refValue]}
                                            value={defaultValueInputs[input.labelsKey]}
                                        />
                                    </InputBlockContainer>
                                </FieldWrapper>
                            )}
                        </Content>
                        <ControlPanel>
                            <ButtonContainer>
                                <CustomButton
                                    fontSize={16}
                                    boxShadow={'none'}
                                    textColor={theme.textColor}
                                    fontWeight={'bold'}
                                    isDisabled={false}
                                    buttonName={dictionary.button.saveGroup}
                                    borderRadius={10}
                                    dataAttribute={'data-button-accountInfoSave'}
                                    textTransform={'uppercase'}
                                    backgroundColor={theme.btnBgColor}
                                    onClickCallback={handleRequest}
                                    backgroundColorHover={theme.backgroundColorHover}
                                />
                            </ButtonContainer>
                        </ControlPanel>
                    </AccountPanelWrapper>
                    <AccountTableWrapper>
                        <Header
                            dictionary={dictionary}
                        />
                        <Body
                            studentData={studentData}
                        />
                    </AccountTableWrapper>
                </AccountPageBody>
        );
    }
}

Account.propTypes = {
    theme: PropTypes.object.isRequired,
    authData: PropTypes.object,
    logout: PropTypes.func.isRequired,
    toggleFullScreen: PropTypes.func.isRequired,
    dictionary: PropTypes.object.isRequired,
    requestEditAccountInfo: PropTypes.func,
    studentData: PropTypes.object,
    getAccountTableData: PropTypes.func,
};
