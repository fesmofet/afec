import React from 'react';
import {StudentPageWrapper, StudentPageContainer} from './styledComponent.js';
import {ThemeProvider} from 'styled-components';
import PropTypes from 'prop-types';
import Schedule from './components/schedule/Schedule.jsx';
import moment from 'moment';
import ToolBar from '../../libs/toolBar/ToolBar.jsx';
import Account from './components/account/Account.jsx';

export default class StudentPage extends React.Component {
    constructor(props) {
        super(props);
        const user = JSON.parse(localStorage.getItem('currentUserData'));
        const payload = {
            to: moment.utc().endOf('isoWeek').valueOf(),
            from: moment.utc().startOf('isoWeek').valueOf(),
            groups: user.groups,
            id: user.id,
        };
        props.getSchedule(payload);
        let payload1 = props.authData.userData.id;
        props.getAccountTableData(payload1);
    }

    render() {
        const {
            authData,
            theme,
            logout,
            lessons,
            history,
            isAccount,
            dictionary,
            getSchedule,
            currentDate,
            switchAccount,
            filterLessons,
            toggleFullScreen,
            toggleAccountStudent,
            getAccountTableData,
            studentData,
            requestEditAccountInfo,
        } = this.props;
        return (
            <ThemeProvider theme={theme}>
                <StudentPageWrapper>
                    <StudentPageContainer>
                                    <ToolBar
                                            role={authData.userData.role}
                                            history={history}
                                            logout={logout}
                                            fullScreen={toggleFullScreen}
                                            toggleAccountStudent={toggleAccountStudent}
                                            isAccount={isAccount}
                                        />
                        {isAccount ? <Account theme={theme}
                                              switchAccount={switchAccount}
                                              history={history}
                                              authData={authData}
                                              logout={logout}
                                              dictionary={dictionary}
                                              fullScreen={toggleFullScreen}
                                              toggleFullScreen={toggleFullScreen}
                                              getAccountTableData={getAccountTableData}
                                              studentData={studentData}
                                              requestEditAccountInfo={requestEditAccountInfo}
                            />
                                    :
                                <Schedule theme={theme}
                                          filterLessons={filterLessons}
                                          lessons={lessons}
                                          dictionary={dictionary}
                                          getSchedule={getSchedule}
                                          currentDate={currentDate}/>
                           }
                    </StudentPageContainer>
                </StudentPageWrapper>
            </ThemeProvider>
        );
    }
}
StudentPage.propTypes = {
    isAccount: PropTypes.bool.isRequired,
    theme: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired,
    toggleFullScreen: PropTypes.func.isRequired,
    toggleAccountStudent: PropTypes.func.isRequired,
    authData: PropTypes.object.isRequired,
    filterLessons: PropTypes.func,
    lessons: PropTypes.array.isRequired,
    getSchedule: PropTypes.func.isRequired,
    switchAccount: PropTypes.func,
    dictionary: PropTypes.object.isRequired,
    getAccountTableData: PropTypes.func,
    studentData: PropTypes.object,
    requestEditAccountInfo: PropTypes.func,
};
