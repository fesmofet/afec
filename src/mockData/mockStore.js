export default {
    authorization: {},
    theme: {
      currentTheme: {
        regPage: {},
        authPage: {},
      },
      nameTheme: 'ocean',
    },
    locale: {
        locale: 'en',
        dictionary: {
            resources: {
                title: 'AuthPage',
            },
        },
    },
    registration: {},
    admin: {
        allUsers: [
            {
            id: '1',
            lastName: 'Vasya',
            firstName: 'Pupkin',
            email: 'vasya@gmail.com',
            phone: '+388888888888',
            role: 'admin',
        },
            {
                id: '2',
                lastName: 'Andrey',
                firstName: 'Pupkin',
                email: 'vasya@gmail.com',
                phone: '+388888888888',
                role: 'admin',
            },
            {
                id: '3',
                lastName: 'Petya',
                firstName: 'Pupkin',
                email: 'vasya@gmail.com',
                phone: '+388888888888',
                role: 'admin',
            },
            {
                id: '4',
                lastName: 'Kolya',
                firstName: 'Pupkin',
                email: 'vasya@gmail.com',
                phone: '+388888888888',
                role: 'admin',
            },
            {
                id: '5',
                lastName: 'Ivan',
                firstName: 'Pupkin',
                email: 'vasya@gmail.com',
                phone: '+388888888888',
                role: 'admin',
            },
            {
                id: '6',
                lastName: 'Sanya',
                firstName: 'Pupkin',
                email: 'vasya@gmail.com',
                phone: '+388888888888',
                role: 'admin',
            },
            {
                id: '7',
                lastName: 'Borya',
                firstName: 'Pupkin',
                email: 'vasya@gmail.com',
                phone: '+388888888888',
                role: 'admin',
            },
            {
                id: '8',
                lastName: 'Egor',
                firstName: 'Pupkin',
                email: 'vasya@gmail.com',
                phone: '+388888888888',
                role: 'admin',
            },
            ],
    },
};
