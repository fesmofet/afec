const jwt = require('jsonwebtoken');
const helpers = require('../helpers/serverHelpers');
const constants = require('../constants/constants');

class HttpServer {
    constructor(port, app, client) {
        this.port = port;
        this.app = app;
        this.client = client;
        this.start();
    }
    start() {
        this.client.connect(function (err) {
            console.log('Connection was successful');
        });
        this.app.listen(this.port, () => {
            console.log(`Server running on port:${this.port}`);
        });
    }
    getInstance() {
        return this.app;
    }
    getClient() {
        return this.client;
    }
}

class RestController {
    constructor(httpServer, pgClient) {
        this.httpServer = httpServer;
        this.pgClient = pgClient;
        this.executeRouteListeners();
    }
    executeRouteListeners() {
        this.httpServer.get('/', (req, res) => {
            res.send('route for logger testing ');
        });
        this.httpServer.post('/auth', (req, res) => {
            let sql = `SELECT * FROM users WHERE email = $1 AND password = $2;`;
            this.pgClient.query(sql, [req.body.email, req.body.password], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else if (response.rows.length) {
                    let user = response.rows[0];
                    data.user = user;
                    jwt.sign({
                        id: user.id,
                        role: user.role,
                    }, `${constants.token}`, (err, token) => {
                        data.token = token;
                        data.status = 'OK';
                        data.message = 'AUTHORIZATION SUCCESS';
                        res.status(200).json(data);
                    });
                } else {
                    data.status = 'ERROR';
                    data.message = 'AUTHORIZATION FAILED';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/reg', (req, res) => {
            let sql1 = `INSERT INTO users (first_name, last_name, email, phone, date_of_birth, password, role, keyword ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id;`;
            this.pgClient.query(sql1, [req.body.firstName, req.body.lastName, req.body.email, req.body.phone, req.body.birthday, req.body.password, req.body.role, req.body.keyword], (error, response) => {
                let data = {};
                if (error) {
                    if (error.code === '23505') {
                        data.status = 'ERROR';
                        data.message = 'USER ALREADY EXIST';
                        res.status(200).json(data);
                    } else {
                        data.status = 'ERROR';
                        data.message = 'DATABASE ERROR';
                        res.status(200).json(data);
                    }
                } else {
                    data.status = 'OK';
                    data.message = 'REGISTRATION COMPLETE';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/users', helpers.verifyToken, (req, res) => {
            let sql = `select users.*, groups.name from users left join groupmembers on users.id=groupmembers.student_id left join groups on groupmembers.group_id = groups.id where users.id != $1`;
            this.pgClient.query(sql, [req.body.userId], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    const users = response.rows;
                    data.users = users;
                    data.status = 'OK';
                    data.message = 'GET USERS SUCCESS';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.get('/groups', helpers.verifyToken, (req, res) => {
            let sql = `select * from groups`;
            this.pgClient.query(sql, [], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    data.groups = response.rows;
                    data.status = 'OK';
                    data.message = 'GET GROUPS SUCCESS';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/update-teacher', helpers.verifyToken, (req, res) => {
            let sql = `UPDATE users SET (first_name, last_name, email, phone, role, teacherlevel, salaryperhour) = ($1, $2, $3, $4, $5, $6, $7) WHERE id = $8 RETURNING *;`;
            this.pgClient.query(sql, [req.body.firstName, req.body.lastName, req.body.email, req.body.phone, req.body.role, req.body.teacherLevel, req.body.salaryPerHour, req.body.id], (error, response) => {
                let data = {};
                if (error) {
                    data.status = error;
                    data.message = 'DATABASE ERROR TEACHER';
                    res.status(200).json(data);
                } else {
                    data.updateUser = response.rows[0];
                    data.status = 'OK';
                    data.message = 'UPDATE COMPLETE';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/get-teachers-salary', helpers.verifyToken, (req, res) => {
            let sql = `select   groups.teacher_id,
                        users.last_name,
                        users.first_name,
                        users.salaryPerHour,
                        COUNT(lessons.id) as lessons_amount 
                        from lessons 
                        right join groups
                        on lessons.group_id = groups.id  
                        right join users
                        on groups.teacher_id = users.id
                        where start > $1 and  finished < $2
                        GROUP BY  groups.teacher_id, users.last_name, users.first_name, users.salaryPerHour;`;
            this.pgClient.query(sql, [req.body.from, req.body.to], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    const salaryData = response.rows;
                    data.salaryData = salaryData;
                    data.status = 'OK';
                    data.message = 'GET USERS SUCCESS';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.get('/get-teachers', helpers.verifyToken, (req, res) => {
            let sql = `SELECT * FROM users WHERE role = 'teacher';`;
            this.pgClient.query(sql, [], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    const teachersInfo = response.rows;
                    data.teachersInfo = teachersInfo;
                    data.status = 'OK';
                    data.message = 'GET TEACHERS SUCCESS';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/restore-password', (req, res) => {
            let sql = `SELECT password FROM users WHERE  email = $1 AND keyword = $2;`;
            this.pgClient.query(sql, [req.body.email, req.body.keyword], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else if (response.rows.length) {
                    let password = response.rows[0].password;
                    data.password = password;
                    data.status = 'OK';
                    data.message = 'RESTORE SUCCESS';
                    res.status(200).json(data);
                } else {
                    data.status = 'ERROR';
                    data.message = 'RESTORE FAILED';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/update-user', helpers.verifyToken, (req, res) => {
            let updateInUsers = `UPDATE users SET (first_name, last_name, email, phone, role, teacherlevel) = ($1, $2, $3, $4, $5, $6) WHERE id = $7 RETURNING *;`;
            this.pgClient.query(updateInUsers, [req.body.firstName, req.body.lastName, req.body.email, req.body.phone, req.body.role, req.body.teacherLevel, req.body.id], (error, response) => {
                let data = {};
                if (error) {
                    data.status = error;
                    data.message = 'DATABASE ERROR USER';
                    res.status(200).json(data);
                } else {
                    data.updateUser = response.rows[0];
                    let selectGroupId = `SELECT groupmembers.group_id from groupmembers WHERE student_id = $1;`;
                    this.pgClient.query(selectGroupId, [req.body.id], (error, response) => {
                        if (error) {
                            data.status = error;
                            data.message = 'DATABASE ERROR GROUP SELECT';
                            res.status(200).json(data);
                        } else {
                            if (response.rows[0]) {
                                let updateGroupId = `UPDATE groupmembers SET group_id = $1 WHERE student_id = ($2) RETURNING group_id;`;
                                this.pgClient.query(updateGroupId, [req.body.groups, req.body.id], (error, response) => {
                                    if (error) {
                                        data.status = error;
                                        data.message = 'DATABASE ERROR GROUP IF STUDENT HAVE GROUP';
                                        res.status(200).json(data);
                                    } else {
                                        let groupId = response.rows[0].group_id;
                                        let getNameOfGroup = `SELECT groups.name FROM groups WHERE id = ${groupId}`;
                                        this.pgClient.query(getNameOfGroup, [], (error, response) => {
                                            if (error) {
                                                data.status = error;
                                                data.message = 'DATABASE ERROR GET GROUP';
                                                res.status(200).json(data);
                                            } else {
                                                data.updateUser.group = response.rows[0].name;
                                                data.status = 'OK';
                                                data.message = 'UPDATE COMPLETE';
                                                res.status(200).json(data);
                                            }
                                        });
                                    }
                                });
                            } else {
                                let createStudentMember = `INSERT INTO groupmembers(group_id, student_id) VALUES ($1, $2) RETURNING group_id;`;
                                this.pgClient.query(createStudentMember, [req.body.groups, req.body.id], (error, response) => {
                                    if (error) {
                                        data.status = error;
                                        data.message = 'DATABASE ERROR GROUP IF STUDENT DONT HAVE GROUP';
                                        res.status(200).json(data);
                                    } else {
                                        let newGroupId = response.rows[0].group_id;
                                        let getGroupName = `SELECT groups.name FROM groups WHERE id = ${newGroupId}`;
                                        this.pgClient.query(getGroupName, [], (error, response) => {
                                            if (error) {
                                                data.status = error;
                                                data.message = 'DATABASE ERROR GET GROUP';
                                                res.status(200).json(data);
                                            } else {
                                                data.updateUser.group = response.rows[0].name;
                                                data.status = 'OK';
                                                data.message = 'UPDATE COMPLETE';
                                                res.status(200).json(data);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        });
        this.httpServer.post('/delete-teacher', helpers.verifyToken, (req, res) => {
            let sql = `UPDATE groups SET teacher_id = null WHERE teacher_id = $1;`;
            this.pgClient.query(sql, [req.body.userId], (error, response) => {
                let data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR GROUP DELETE TEACHER';
                    res.status(200).json(data);
                } else {
                    let sql1 = `DELETE FROM users WHERE id = $1`;
                    this.pgClient.query(sql1, [req.body.userId], (error1, response) => {
                        if (error1) {
                            data.status = 'ERROR';
                            data.message = 'DATABASE ERROR DELETE TEACHER IN USERS';
                            res.status(200).json(data);
                        } else {
                            data.status = 'OK';
                            data.message = 'DELETE COMPLETE';
                            res.status(200).json(data);
                        }
                    });
                }
            });
        });
        this.httpServer.post('/delete-user', helpers.verifyToken, (req, res) => {
            let sql = `DELETE FROM attendances WHERE student_id = $1`;
            this.pgClient.query(sql, [req.body.userId], (error, response) => {
                let data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    let sql1 = `DELETE FROM groupmembers WHERE student_id = $1`;
                    this.pgClient.query(sql1, [req.body.userId], (error1, response) => {
                        if (error1) {
                            data.status = 'ERROR';
                            data.message = 'DATABASE ERROR';
                            res.status(200).json(data);
                        } else {
                            let sql2 = `DELETE FROM users WHERE id = $1`;
                            this.pgClient.query(sql2, [req.body.userId], (error2, response) => {
                                if (error2) {
                                    data.status = 'ERROR';
                                    data.message = 'DATABASE ERROR';
                                    res.status(200).json(data);
                                } else {
                                    data.status = 'OK';
                                    data.message = 'DELETE COMPLETE';
                                    res.status(200).json(data);
                                }
                            });
                        }
                    });
                }
            });
        });
        this.httpServer.get('/groups-page', helpers.verifyToken, (req, res) => {
            let sql = `SELECT groups.id AS group_id, * FROM groups INNER JOIN users on groups.teacher_id = users.id;`;
            this.pgClient.query(sql, [], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    const groups = response.rows;
                    data.groups = groups;
                    data.status = 'OK';
                    data.message = 'GET GROUPS SUCCESS';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.get('/groups-page-count-students', helpers.verifyToken, (req, res) => {
            let sql = `
                SELECT groups.id AS group_id, groups.name, COUNT(users.id) FROM users 
                LEFT JOIN groupmembers ON users.id = groupmembers.student_id 
                LEFT JOIN  groups ON groupmembers.group_id = groups.id 
                WHERE users.role = 'student'
                GROUP BY (groups.name, groups.id);
            `;
            this.pgClient.query(sql, [], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    const groups = response.rows;
                    data.groups = groups;
                    data.status = 'OK';
                    data.message = 'GET GROUPS SUCCESS';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/add-group', helpers.verifyToken, (req, res) => {
            let sql = `INSERT into groups (name, level, city, teacher_id) VALUES ($1, $2, $3, $4)`;
            this.pgClient.query(sql, [req.body.groupName, req.body.groupLevel, req.body.groupCity, req.body.selectTeacher], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    data.status = 'OK';
                    data.message = 'GROUP WAS ADDED';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/get-schedule', helpers.verifyToken, (req, res) => {
             let sql = null;
            if (req.body.groups.length > 0) {
                let params = [];
                for (let i = 1; i <= req.body.groups.length; i++) {
                    params.push('$' + i);
                }
                sql = `SELECT lessons.id as lesson_id,lessons.type, lessons.start, lessons.finished, users.first_name, users.last_name, groups.id as group_id, groups.name 
                        FROM lessons 
                        inner join groups on lessons.group_id = groups.id 
                        inner join users on  groups.teacher_id = users.id
                         where ` + req.body.from + ` <= lessons.start and  lessons.finished <= ` + req.body.to + ` and groups.id IN (` + params.join(',') + `);`;
                this.pgClient.query(sql, req.body.groups, (error, response) => {
                    const data = {};
                    if (error) {
                        data.status = 'ERROR';
                        data.message = 'DATABASE ERROR';
                    } else {
                        data.lessons = response.rows;
                        data.status = 'OK';
                        data.message = 'GET SCHEDULE SUCCESS';
                    }
                    res.status(200).json(data);
                });
            } else {
                sql = `SELECT lessons.id as lesson_id,lessons.type, lessons.start, lessons.finished, users.first_name, users.last_name, groups.id as group_id, groups.name 
                        FROM lessons 
                        inner join groups on lessons.group_id = groups.id 
                        inner join users on  groups.teacher_id = users.id
                         where  $1 <= lessons.start and  lessons.finished <= $2;`;
                    this.pgClient.query(sql, [req.body.from, req.body.to], (error, response) => {
                    const data = {};
                    if (error) {
                        data.status = 'ERROR';
                        data.message = 'DATABASE ERROR';
                    } else {
                        data.lessons = response.rows;
                        data.status = 'OK';
                        data.message = 'GET SCHEDULE SUCCESS';
                    }
                    res.status(200).json(data);
                });
            }
        });
        this.httpServer.post('/delete-group', helpers.verifyToken, (req, res) => {
            let sql = `DELETE FROM groups WHERE id = $1`;
            this.pgClient.query(sql, [req.body.id], (error, response) => {
                let data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    data.status = 'OK';
                    data.message = 'DELETE COMPLETE';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/get-students', helpers.verifyToken, (req, res) => {
            let sql = `select groups.id as group_id, groups.name, groupmembers.student_id, users.first_name, users.last_name from groups
                        inner join groupmembers on groups.teacher_id = $1 and groups.id = groupmembers.group_id
                        inner join users on groupmembers.student_id = users.id`;
            this.pgClient.query(sql, [req.body.id], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    data.students = response.rows;
                    data.status = 'OK';
                    data.message = 'GET USERS SUCCESS';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/get-student-group', (req, res) => {
            let getTeachersGroup = `select group_id from groupmembers where student_id=$1`;
            this.pgClient.query(getTeachersGroup, [req.body.id], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    data.groups = response.rows;
                    data.status = 'OK';
                    data.message = 'GET GROUP SUCCESS';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/get-teachers-group', helpers.verifyToken, (req, res) => {
            let getTeachersGroup = `select * from groups where teacher_id=$1`;
            this.pgClient.query(getTeachersGroup, [req.body.id], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    data.groups = response.rows;
                    data.status = 'OK';
                    data.message = 'GET groups SUCCESS';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/get-teachers-group-ids', helpers.verifyToken, (req, res) => {
            let getTeachersGroup = `select id from groups where teacher_id=$1`;
            this.pgClient.query(getTeachersGroup, [req.body.id], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    data.groups = response.rows;
                    data.status = 'OK';
                    data.message = 'GET groups SUCCESS';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/update-student', helpers.verifyToken, (req, res) => {
            let updateInUsers = `UPDATE users SET (first_name, last_name) = ($1, $2) WHERE id = $3 RETURNING *;`;
            this.pgClient.query(updateInUsers, [req.body.firstName, req.body.lastName, req.body.studentId], (error, response) => {
                let data = {};
                if (error) {
                    data.status = error;
                    data.message = 'DATABASE ERROR USER';
                    res.status(200).json(data);
                } else {
                    data.updateStudent = response.rows[0];
                    let selectGroupId = `SELECT groupmembers.group_id from groupmembers WHERE student_id = $1;`;
                    this.pgClient.query(selectGroupId, [req.body.studentId], (error, response) => {
                        if (error) {
                            data.status = error;
                            data.message = 'DATABASE ERROR GROUP SELECT';
                            res.status(200).json(data);
                        } else {
                            if (response.rows[0]) {
                                let updateGroupId = `UPDATE groupmembers SET group_id = $1 WHERE student_id = $2 RETURNING group_id;`;
                                this.pgClient.query(updateGroupId, [req.body.groupId, req.body.studentId], (error, response) => {
                                    if (error) {
                                        data.status = error;
                                        data.message = 'DATABASE ERROR GROUP IF STUDENT HAVE GROUP';
                                        res.status(200).json(data);
                                    } else {
                                        let groupId = response.rows[0].group_id;
                                        let getNameOfGroup = `SELECT groups.name FROM groups WHERE id = ${groupId}`;
                                        this.pgClient.query(getNameOfGroup, [], (error, response) => {
                                            if (error) {
                                                data.status = error;
                                                data.message = 'DATABASE ERROR GET GROUP';
                                                res.status(200).json(data);
                                            } else {
                                                data.updateStudent.group = response.rows[0].name;
                                                data.updateStudent.groupId = groupId;
                                                data.status = 'OK';
                                                data.message = 'UPDATE COMPLETE';
                                                res.status(200).json(data);
                                            }
                                        });
                                    }
                                });
                            } else {
                                let createStudentMember = `INSERT INTO groupmembers(group_id, student_id) VALUES ($1, $2) RETURNING group_id;`;
                                this.pgClient.query(createStudentMember, [req.body.groupId, req.body.studentId], (error, response) => {
                                    if (error) {
                                        data.status = error;
                                        data.message = 'DATABASE ERROR GROUP IF STUDENT DONT HAVE GROUP';
                                        res.status(200).json(data);
                                    } else {
                                        let newGroupId = response.rows[0].group_id;
                                        let getGroupName = `SELECT groups.name FROM groups WHERE id = ${newGroupId}`;
                                        this.pgClient.query(getGroupName, [], (error, response) => {
                                            if (error) {
                                                data.status = error;
                                                data.message = 'DATABASE ERROR GET GROUP';
                                                res.status(200).json(data);
                                            } else {
                                                data.updateStudent.group = response.rows[0].name;
                                                data.updateStudent.groupId = newGroupId;
                                                data.status = 'OK';
                                                data.message = 'UPDATE COMPLETE';
                                                res.status(200).json(data);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        });
        this.httpServer.post('/edit-group', helpers.verifyToken, (req, res) => {
            let sql = `UPDATE groups SET (name, level, city, teacher_id) = ($1, $2, $3, $4) WHERE id = $5`;
            this.pgClient.query(sql, [req.body.groupName, req.body.groupLevel, req.body.groupCity, req.body.selectTeacher, req.body.groupId], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    data.status = 'OK';
                    data.message = 'GROUP HAS BEEN CHANGED';
                    res.status(200).json(data);
                }
            });
        });
        this.httpServer.post('/get-attendance', helpers.verifyToken, (req, res) => {
            if (req.body.groupId === 'all') {
                let sql = `select groups.name, groups.level, groups.id as group_id,
                lessons.id as lesson_id, lessons.group_id as lessons_group_id,
                attendances.student_id, attendances.attendance ,
                users.first_name, users.last_name from groups
                inner join lessons on groups.id = lessons.group_id
                inner join attendances on attendances.lesson_id = lessons.id
                inner join users on users.id = attendances.student_id
                where groups.teacher_id = $1 and lessons.start = $2;`;
                this.pgClient.query(sql, [req.body.teacherId, req.body.timeStamp], (error, response) => {
                    const data = {};
                    if (error) {
                        data.status = 'ERROR';
                        data.message = 'DATABASE ERROR';
                        res.status(200).json(data);
                    } else {
                        data.attendances = response.rows;
                        data.status = 'OK';
                        data.message = 'GROUP HAS BEEN CHANGED';
                        res.status(200).json(data);
                    }
                });
            } else {
                let sql = `select groups.name, groups.level, groups.id as group_id,
                lessons.id as lesson_id, lessons.group_id as lessons_group_id,
                attendances.student_id, attendances.attendance ,
                users.first_name, users.last_name from groups
                inner join lessons on groups.id = lessons.group_id
                inner join attendances on attendances.lesson_id = lessons.id
                inner join users on users.id = attendances.student_id
                where groups.teacher_id = $1 and lessons.start = $3
                and groups.id = $2;
                `;
                this.pgClient.query(sql, [req.body.teacherId, req.body.groupId, req.body.timeStamp], (error, response) => {
                    const data = {};
                    if (error) {
                        data.status = 'ERROR';
                        data.message = 'DATABASE ERROR';
                        res.status(200).json(data);
                    } else {
                        data.attendances = response.rows;
                        data.status = 'OK';
                        data.message = 'GROUP HAS BEEN CHANGED';
                        res.status(200).json(data);
                    }
                });
            }
            this.httpServer.post('/update-attendance', helpers.verifyToken, (req, res) => {
                let updateAttendance = `UPDATE attendances SET attendance = $1 WHERE student_id=$2 and lesson_id = $3`;
                this.pgClient.query(updateAttendance, [req.body.attendance, req.body.studentId, req.body.lessonId], (error, response) => {
                    const data = {};
                    if (error) {
                        data.status = 'ERROR';
                        data.message = 'DATABASE ERROR';
                        res.status(200).json(data);
                    } else {
                        data.status = 'OK';
                        data.message = 'ATTENDANCE HAS BEEN CHANGED';
                        res.status(200).json(data);
                    }
                });
            });
        });
        this.httpServer.post('/add-lesson-admin', helpers.verifyToken, (req, res) => {
            let sql = `INSERT into lessons (group_id, type, start, finished) VALUES ($1, $2, $3, $4) returning id`;
            this.pgClient.query(sql, [req.body.group_id, req.body.type, req.body.start, req.body.finished], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    let selectGroupId = `insert into attendances (lesson_id, student_id, attendance) 
                    select $2, users.id as student_id, false from users inner join groupmembers on users.id = groupmembers.student_id
                    where groupmembers.group_id = $1; `;
                    this.pgClient.query(selectGroupId, [req.body.group_id, response.rows[0].id], (error, response) => {
                        if (error) {
                            data.status = error;
                            data.message = 'DATABASE ERROR GROUP SELECT';
                            res.status(200).json(data);
                        } else {
                            data.status = 'OK';
                            data.message = 'LESSON WAS ADDED';
                            res.status(200).json(data);
                        }
                    });
                }
            });
        });

        this.httpServer.post('/get-account-info', helpers.verifyToken, (req, res) => {
            let sql = `
            select users.id,  users.last_name, (count (*)  filter (where attendances.attendance = true)::decimal / count(*)) as attendence, 
            COUNT (*) as amount_of_lessons,
            count (*) filter (where attendances.attendance = true)::decimal as student_attendance
            from users inner join attendances on users.id = attendances.student_id
            where users.id = $1
            group by users.id`;
            this.pgClient.query(sql, [req.body.id], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    data.info = response.rows[0];
                    data.status = 'OK';
                    data.message = 'GET USER INFO';
                    res.status(200).json(data);
                }
            });
        });

        this.httpServer.post('/update-account-info', helpers.verifyToken, (req, res) => {
            let sql = `UPDATE users SET (first_name, last_name, date_of_birth, password) = ($1, $2, $3, $4) WHERE id = $5`;
            this.pgClient.query(sql, [req.body.firstName, req.body.lastName, req.body.dateOfBirth, req.body.password, req.body.studentId], (error, response) => {
                const data = {};
                if (error) {
                    data.status = 'ERROR';
                    data.message = 'DATABASE ERROR';
                    res.status(200).json(data);
                } else {
                    data.status = 'OK';
                    data.message = 'ACCOUNT HAS BEEN UPDATED';
                    res.status(200).json(data);
                }
            });
        });
    }
}
module.exports = {
    HttpServer,
    RestController,
};
