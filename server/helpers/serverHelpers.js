const jwt = require('jsonwebtoken');

function cors(request, response, next) {
    response.header('Access-Control-Allow-Origin', '*');
    response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
}

function verifyToken(req, res, next) {
    const decode = jwt.decode(req.headers.authorization);
    if (!decode) {
        res.sendStatus(403);
    }
    const roles = {
        admin: ['/change-user-role',
            '/users',
            '/get-teachers-salary',
            '/get-teachers',
            '/add-group',
            '/groups-page',
            '/groups-page-count-students',
            '/groups',
            '/get-groups-members',
            '/delete-user',
            '/update-user',
            '/update-teacher',
            '/delete-teacher',
            '/change-user-role',
            '/delete-group',
            '/get-schedule',
            '/edit-group',
            '/add-lesson-admin',
        ],
        teacher: [
            '/get-students',
            '/groups',
            '/get-teachers-group',
            '/update-student',
            '/get-schedule',
            '/get-attendance',
            '/update-attendance',
            '/add-lesson-admin',
            '/get-teachers-group-ids',
        ],
        student: [
                '/get-schedule',
                '/get-account-info',
                '/update-account-info',
                '/get-schedule',
                '/get-student-group',
        ],
    };
    switch (decode.role) {
        case 'admin':
            roles.admin.includes(req.url) ? next() : res.sendStatus(403);
            break;
        case 'teacher':
            roles.teacher.includes(req.url) ? next() : res.sendStatus(403);
            break;
        case 'student':
            roles.student.includes(req.url) ? next() : res.sendStatus(403);
            break;
        default:
            res.sendStatus(403);
            break;
    }
}

module.exports = {
    cors,
    verifyToken,
};
