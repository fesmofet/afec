const fs = require('fs');
const path = require('path');
const { Client } = require('pg');
const morgan = require('morgan');
const express = require('express');
const bodyParser = require('body-parser');
const helpers = require('./helpers/serverHelpers.js');
const app = express();
const HttpServer = require('./restController/httpServer').HttpServer;
const RestController = require('./restController/httpServer').RestController;
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });
const { connectionString, postgresServerPort } = require('./constants/constants.js');

app.use(helpers.cors);
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('combined', { stream: accessLogStream }));

const main = () => {
    const httpServer = new HttpServer(postgresServerPort, app, new Client(connectionString));
    new RestController(httpServer.getInstance(), httpServer.getClient());
};

main();
