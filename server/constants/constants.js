const fs = require('fs');
const connectionString = fs.readFileSync('resources.txt', 'utf-8');
const token = 'secret';

module.exports = {
    connectionString,
    postgresServerPort: 3000,
    token,
};