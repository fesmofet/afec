const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

 module.exports = {
   entry: {
     app: ['@babel/polyfill', './src/index.js'],
   },
   plugins: [
     new CleanWebpackPlugin(),
     new HtmlWebpackPlugin({
        template: 'public/index.html',
     }),
   ],
   output: {
    filename: '[name].[hash:8].js',
    path: path.resolve(__dirname, './dist'),
   },
   module: {
       rules: [
        {
            test: /\.(jsx|js)$/,
            exclude: /(node_modules|bower_components)/,
            loader: 'babel-loader',
        },
        {
            test: /\.(svg|png|jpg)$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
            },
        },
    ],
   },
 };